# EXTRAN

## Overview

EXTRAN is a tool for assessing the potential effects of accidental releases of radioactive materials and toxic substances on habitability of nuclear facility control rooms. The code that estimates concentrations at nuclear facility controlroom air intakes given information about the release and the environmental conditions. EXTRAN combines procedures for estimating the amount of airborne material, a Gaussian puff dispersion model, and the most recent algorithms for estimating diffusion coefficients in building wakes. It is a modular computer code written in FORTRAN-77 that runs on personal computers. This version of the code is based on the source code published in NUREG/CR-5656, *EXTRAN: A Computer Code for Estimating Concentrations of Toxic Substances at Control Room Air Intakes*, USNRC, March 1991, prepared by J. V. Ramsdell of Pacific Northwest Laboratory.

## Project Goals

The initial goals of this project are:

* **DONE** ~~Recover the original source code as published in NUREG/CR-5656
  to a compilable runnable state on x86_64 hardware,~~
* **DONE** ~~Update the source code to well-formed Fortran 2018,~~
* **DONE** ~~Benchmark the revised code against original example scenarios (see Appendix A of NUREG/CR-5656),~~
* **DONE** Reconstitute a sustainable cross-platform build/test/packaging process based on CMake/CTest/CPack, and
* Convert documentation (NUREG/CR-5656) to Doxygen and LaTeX

## Current Status

* All FORTRAN routines and include files have been ported from NUREG/CR-5656
* API documentation is generated automatically via Doxygen
* All example cases have been successfully run
* Code compiles, runs, and packages on Linux and Windows 10
* Code is Fortran 2018 clean
* Regression tests run and pass under CTest on both Windows and Linux
* Memory tests run and pass under Windows and Linux (Dr. Memory). Note that `ctest -T memcheck` tests the output file comparisons, not the EXTRAN runs.
* All routines migrated into modules
* Needs testing on MacOS

## Development Notes

[Development notes](NOTES.md) are currently tracked via text file.

## Code of Conduct

Treat others with respect and dignity. For more information, refer to
this project's [Code of Conduct](CODE_OF_CONDUCT.md)

## License

The original code was produced by Pacific Northwest Laboratory for the
US Nuclear Regulatory Commission. No license was provided for the original
1991 code as was the practice at the time. For the purpose of furthering
education and research and to preserve the legacy of this software, this
project is released under the MIT License (see [LICENSE](LICENSE)). It is not clear
what the license should be but it is felt that the MIT License is
appropriate here.

## References

* *EXTRAN: A Computer Code for Estimating Concentrations of Toxic Substances at Control Room Air Intakes*, NUREG/CR-5656, March 1991, Nuclear Regulatory Commission https://www.osti.gov/biblio/6014413-extran-computer-code-estimating-concentrations-toxic-substances-control-room-air-intakes

## Image Credits
Uses icons from fjstudio https://www.flaticon.com/authors/fjstudio from https://www.flaticon.com - free for personal or commercial use with attribution.