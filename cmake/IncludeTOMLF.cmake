# Retrieve toml-f from external source and include into project
# See https://github.com/toml-f/toml-f
#
# The following output variables are set by the TOMLF subproject
# - TOMLF_LIB_NAME
# - TOMLF_LIBRARY_DIR
# - TOMLF_MODULE_DIR
set(TOMLF_SOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}/TOMLF-source")

# Temporarily set BUILD_SHARED_LIBS to OFF
set(_orig_BUILD_SHARED_LIBS "${BUILD_SHARED_LIBS}")
set(BUILD_SHARED_LIBS OFF)

FetchContent_Declare(
    TOMLF_external
    GIT_REPOSITORY         https://github.com/toml-f/toml-f.git
    GIT_TAG                v0.2.1
    SOURCE_DIR             "${TOMLF_SOURCE_DIR}"
)

FetchContent_MakeAvailable(TOMLF_external)
FetchContent_GetProperties(TOMLF_external)
# FetchContent_GetProperties(TOMLF_external
#     POPULATED TOMLF_external_POPULATED
# )

# Restore BUILD_SHARED_LIBS to original state
set(BUILD_SHARED_LIBS "${_orig_BUILD_SHARED_LIBS}")

set(TOMLF_FOUND "${TOMLF_external_POPULATED}")
# __END__