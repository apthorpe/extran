# Development Notes

Please record observations regarding code condition, style, and idiom here for later review. Issues and errors should be tracked as issues via GitLab.

## Recovery Phase

General condition of code appears good. Code is documented and formatted. Variables are declared. Snake-case used instead of camel-case.

Inconsistent variable case though individual variables appear to be written consistently.

OCR of source document is moderate to poor. Structure is not well-preserved, white space is often compressed or eliminated especially in header block comments. Underscore is occasionally dropped. Typos are infrequent; `'0/O'`, `'1/I/l'`, and `'5/S'` are most common. Source text is clear so typos are easily resolved when comparing OCR-interpreted text with scanned original.

`IMPLICIT NONE` not used.

Inconsistent use of continuation character; both '+' and '.' are seen. '.' is often dropped from OCR.

Nonstandard use of `INTEGER*2`, `REAL*4`. Set `I2 = KIND(integer*2_variable)` and use `INT(var, I2)` and `var = 1_I2` to avoid type warnings (`INTEGER(4) to INTEGER(2) conversion`)

Use of COMMON blocks appears straightforward as simple named global storage.

Some issues with alignment in COMMON blocks (mix of `INTEGER*2` and `REAL*4` types) specifically in `SCENARIO.INC`

**CHANGE**: COMMON block `/scenario/` has been reordered so all `INTEGER*2` variables appear at the end of the block.

**DONE**: Examine code for alternate references to `/scenario/` COMMON block. Issues may arise if variable ordering is important (*e.g.* array access for initialization or bulk manipulation)

Found `BN` format specifier; treats trailing blanks in numerical field as nulls and does not output them. See also `BZ` - treats trailing blanks in numerical field as zeros. `B` returns to default, outputting trailing blanks as spaces. The `BN` and `BZ` specifiers are modal; they toggle on null or zero interpretation and can be reset with the `B` specifier.

Attempts to open file `LPT1` when sending output to printer (DOS-era behavior)

**DONE**: Rename `LPT1` (legacy DOS printer device) to `LPT1.txt` to save printer-directed output to file

**TODO**: Rename `LPT1.txt` according to existing file naming convention, *e.g.* `EXLPhhmm.nnn` (`LP` is a mnemonic for line printer)

File naming convention is limited to the MSDOS 8.3 convention, using 24-hour hour and minute values to differentiate runs and a three digit serial number to differentiate variations in environmental conditions. For example, in a file named `EXzzhhmm.nnn`, `zz` is `MB` for mass balance, `CR` for concentration record, or `PR` for summary record, `hhmm` is the hour and minute of the local time in 24-hour-format, and `nnn` is a serial number starting with `001`. This is adequate for occasional use on a single day, but prevents runs from being run in parallel or in serial within a frequency less than a minute.

**TODO**: Revise file naming scheme to allow parallel and high-frequency serial runs.

`PAUSE` interferes with piped `STDIN` test input.

**DONE**: Comment out `PAUSE` to allow test automation

`wsig()` and `puffinit()` define identical `ZOL` (z/l) parameter arrays for stability class and repeat the calculations of friction velocity (ustar), horizontal timescale (`tsh`, `HTS`), and vertical timescale (`tsv`, `VTS`).

### extran.f

`'(10X,A,1PE11.2)'` should be defined as `FORMAT(10X, A, ES11.2)`

**TODO** Hardcoded file/printer unit numbers should be parameterized. Use `newunit=` to set file unit

Hardcoded `count` limits should be parameterized

Migrate `extran.f` output to separate routines for clarity

Backslash at end of an edit specifier is an extension to prevent an automatic newline on a `WRITE()` statement.

**DONE** Replace backslashes in edit specifiers with `advance='NO'` argument to `WRITE()`

### input.f

**TODO** Move parameters to a physical constants module and import

### inputef.f

**DONE** `nchemlin` is the maximum number of chemicals allowed in `CHEMICAL.DAT` (20); make this a global constant in `extran_param.f90`

**DONE** Arrays `phys_prop` and `gas` can be combined into a simple gas property structure/object with dimension `nchemlin`

### m_print.f90

**DONE**  Convert dimension size of 181 to a constant in `extran_param.f90`. Replace other anonymous uses of 180, 181 with dimension size parameter.

### m_puff.f90

**DONE**  Convert dimension size of 500 to a constant in `extran_param.f90`. Replace other anonymous uses of 500 with dimension size parameter.

**DONE**  Convert arrays of dimension size of 500 to a structure and replace with array of that structure

**DONE**  Convert x1/x2 variables to a structure and replace with two elements of that structure

### difcoef.f

**DONE**  Move `DIFCOEF` to `m_effluent` module. Convert `air_press` import to an argument

**TODO** Move parameters to a physical constant module and import

### ceplot.f90

**DONE** Move to routine module `m_print`; note: moved to `extran_output` instead
