# Q: Why do I need this here?
cmake_policy(SET CMP0053 NEW)
# Find output file names (naive glob)
file(GLOB testfiles
     LIST_DIRECTORIES FALSE
     EX*
)

# Define reference and test output match and substitution regexes
# here for consistency. Note use of [.] instead of \\.
set(RE_REFMATCH "EX(CR|PR|MB)_example_[0-9][.][0-9][0-9][0-9]$")
set(RE_REFSUB   "(EX(CR|PR|MB))(_example_[0-9])([.][0-9][0-9][0-9])$")

set(RE_OUTMATCH "(EX(CR|PR|MB))([0-2][0-9][0-5][0-9])([.][0-9][0-9][0-9])$")
set(RE_OUTSUB   "(EX(CR|PR|MB))([0-2][0-9][0-5][0-9])([.][0-9][0-9][0-9])")

# Pick reference and test output files from naive file list
foreach(fraw IN LISTS testfiles)
  if("${fraw}" MATCHES "${RE_REFMATCH}")
    list(APPEND reffiles "${fraw}")
  elseif("${fraw}" MATCHES "${RE_OUTMATCH}")
    list(APPEND outfiles "${fraw}")
  endif()
endforeach()

# message(STATUS "Found output files ${testfiles}")
# message(STATUS "Found reference output files ${reffiles}")
# message(STATUS "Found test output files ${outfiles}")

# # Find reference file names - fails on Windows
# file(GLOB reffiles
#      LIST_DIRECTORIES FALSE
#      EX[CMP][RB]_example_[0-9].[0-9][0-9][0-9]
# )

# Strip time and date from reference output, save to EXzz.nnn.ref
foreach(fsrc IN LISTS reffiles)
  # Rename to eliminate timestamp
  # \1 is full prefix, \2 is report type, \3 is timestamp, \4 is dotted extension
  string(REGEX REPLACE
         "${RE_REFSUB}"
         "\\1\\4.ref"
         fdest "${fsrc}")
  #  message(STATUS "Renaming output from ${fsrc} to ${fdest}")
  # file(RENAME "${fsrc}" "${fdest}")

  file(READ "${fsrc}" TMPTEXT)
  # Strip code date and version info
  string(REGEX REPLACE
         "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]Z"
         "****-**-**T**:**:**Z"
         TMPTEXT "${TMPTEXT}")
  string(REGEX REPLACE
         "VERSION [0-9][0-9.]*"
         "VERSION *********"
         TMPTEXT "${TMPTEXT}")

  # Strip time and date content
  string(REGEX REPLACE
         "../../...."
         "**/**/****"
         TMPTEXT "${TMPTEXT}")
  string(REGEX REPLACE
         "..:..:.."
         "**:**:**"
         TMPTEXT "${TMPTEXT}")
  string(REGEX REPLACE
         "${RE_OUTSUB}"
         "\\1****\\4"
         TMPTEXT "${TMPTEXT}")
  # Write to new filename
  file(WRITE "${fdest}" "${TMPTEXT}")
endforeach()

# # Find test output file names - fails on Windows
# file(GLOB outfiles
#      LIST_DIRECTORIES FALSE
#      EX[CMP][RB][0-2][0-9][0-9][0-9].[0-9][0-9][0-9]
#      )

# Strip time and date from test output, save to EXzz.nnn
foreach(fsrc IN LISTS outfiles)
  # Rename to eliminate timestamp
  # \1 is full prefix, \2 is report type, \3 is timestamp, \4 is dotted extension
  string(REGEX REPLACE
         "${RE_OUTMATCH}"
         "\\1\\4"
         fdest "${fsrc}")
  #  message(STATUS "Renaming output from ${fsrc} to ${fdest}")
  # file(RENAME "${fsrc}" "${fdest}")

  # Read file contents
  file(READ "${fsrc}" TMPTEXT)
  # Strip code date and version info
  string(REGEX REPLACE
         "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]Z"
         "****-**-**T**:**:**Z"
         TMPTEXT "${TMPTEXT}")
  string(REGEX REPLACE
         "VERSION [0-9][0-9.]*"
         "VERSION *********"
         TMPTEXT "${TMPTEXT}")

  # Strip time and date content
  string(REGEX REPLACE
         "../../...."
         "**/**/****"
         TMPTEXT "${TMPTEXT}")
  string(REGEX REPLACE
         "..:..:.."
         "**:**:**"
         TMPTEXT "${TMPTEXT}")
  string(REGEX REPLACE
         "${RE_OUTSUB}"
         "\\1****\\4"
         TMPTEXT "${TMPTEXT}")

#   message(STATUS "Writing stripped output from ${fsrc} to ${fdest}")
  # Write to new filename
  file(WRITE "${fdest}" "${TMPTEXT}")
#  message(STATUS "Renaming output from ${fsrc} to ${fsrc}.raw")
  file(RENAME "${fsrc}" "${fsrc}.raw")
endforeach()