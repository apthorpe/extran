
          Program Title:   EXTRAN VERSION 1.2.0.0

          Developed For:   U.S. Nuclear Regulatory Commission
                           Office of Nuclear Regulatory Research
                           Division of Reactor Accident Analysis

          Date:            2021-02-06T22:52:11Z

          NRC Contact(s):  C. Ferrell     Phone: (FTS) 492 3944
          Code Developer:  J. V. Ramsdell Phone: (509) 376-8626
                                                 (FTS) 444-8626

          Code Documentation: 
                           EXTRAN: A Computer Code For Estimating
                           Concentrations Of Toxic Substances At
                           Control Room Air Intakes
                           NUREG/CR-5656



          The program was prepared for an agency of the UnitedStates
          Government. Neither the United States Government nor any
          agency thereof, nor any of their employees, makes any
          warranty, expressed or implied, or assumes any legal
          liability or responsibilities for any third party's use,
          or the results of such use, of any portion of this program
          or represents that its use by such third party would not
          infringe privately owned rights. 



           Example 1  --  EXTRAN Version 1.2                                     

           RUN DATE =  2/ 6/2021  RUN TIME = 16:52:21

           SCENARIO:
           Release Type             =  Liquid Tank Burst
           Initial Mass        (kg) =      1000.
           Release Height       (m) =         0.0
           Storage Temperature  (C) =        20.0
           Maximum Pool Radius  (m) =        10.0
           Intake Distance      (m) =       250.
           Intake Height        (m) =        25.0
           Building Area     (m**2) =       100.


           ENVIRONMENTAL CONDITIONS:
           Wind Speed                (m/sec) =        3.0
           Atmospheric Stability Class       =        4
           Air Temperature               (C) =       20.0
           Atmospheric Pressure      (mm Hg) =      760.0
           Solar Radiation      (watts/m**2) =      650.0
           Cloud Cover              (tenths) =        2
           Ground Temperature            (C) =       25.0


           EFFLUENT CHARACTERISTICS:
           Material Released                 =      CHLORINE            
           Molecular Weight        (gm/mole) =       70.9
           Initial Boiling Point         (C) =      -34.1
           Heat Capacity            (j/gm-C) =        0.946
           Heat of Vapor.             (j/gm) =      288.0
           Specific Gravity                  =        1.570
           Diffusion Coef.       (cm**2/sec) =        0.079


           MODEL PARAMETERS:
           Puff Release Interval               (sec) =   10
           Time Step                           (sec) =    5
           Delay Between Release and Intake    (sec) =   50
           Threshold Concentration          (g/m**3) =    7.08E-05
           To convert g/m**3 to ppm, multiply by          3.39E+02


           RESULTS:
           Average Concentration During First Two Minutes
          After Arrival of Plume                     (g/m**3) =    1.93E-01
           Exposure Two Minutes After Arrival    (g-sec/m**3) =    2.41E+01
           Time From Plume Arrival to Max. Conc.        (sec) =   30.
           Max. Conc. in Two Minutes After Arrival (mCi/m**3) =    6.12E-01

          ADDITIONAL OUTPUT FILES: 
               EXCR1652.001
               EXMB1652.001
