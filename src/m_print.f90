!> @file m_print.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Shared plot data module

!> @brief Shared plot data module
!!
!! Contains information for plotting; instantaneous concentration and
!! exposure record, chronological history of concentration and exposure
module m_print
!***********************************************************************
!     m_print.f90                                     EXTRAN Version 1.2
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description: Contains information for plotting.
!
!     Included in modules: CEPLOT, CHIT, EXTRAN
!
!***********************************************************************
use extran_param, only: WP, IP, NTIME
use m_constant, only: ZERO
implicit none

public

!> Concentration and exposure record for a single time point at the
!! receptor
type :: dose_record
  !> Airborne concentration.
  !! Units are parts per million
  !! (ppm), mCi per \f$\si{\cubic\meter}\f$,
  !! or \f$\si{\gram\per\cubic\meter}\f$
  !! depending on the values of `ppmflg` and `ciflg` in `m_option`
  real(kind=WP) :: conc = ZERO

  !> Exposure; concentration multiplied by time interval.
  !! Units are mCi-sec per \f$\si{\cubic\meter}\f$,
  !! or \f$\si{\gram\second\per\cubic\meter}\f$
  !! depending on the value of `ciflg` in `m_option`
  real(kind=WP) :: expos = ZERO

  !> Average concentration.
  !! Units are parts per million
  !! (ppm), mCi per \f$\si{\cubic\meter}\f$,
  !! or \f$\si{\gram\per\cubic\meter}\f$
  !! depending on the values of `ppmflg` and `ciflg` in `m_option`
  real(kind=WP) :: avconc = ZERO

  !> Time, \f$\si{\second}\f$
  integer(kind=IP) :: ttime = 0_IP

  !> Cumulative number of puffs released
  integer(kind=IP) :: npuffs = 0_IP

contains

  !> Set all properties to zero
  procedure :: reset => dose_record_reset

end type dose_record

!> Time series of concentration and exposure at the receptor
type :: dose_track

  !> List of concentration and exposure records
  type(dose_record), dimension(NTIME) :: datum

  !> Instantaneous exposure in units of \f$\si{\kilo\gram\per\cubic\meter}\f$
  real(kind=WP) :: exposure = ZERO

  !> Instanteous step count
  integer(kind=IP) :: count = 0_IP

  !> Step at which material is detected at receptor
  integer(kind=IP) :: start = 0_IP

contains

  !> Set all properties to zero
  procedure :: reset => dose_track_reset

end type dose_track

!      COMMON /print/ conc, expos, avconc, ttime, count, npuffs, start,
!     +               exposure

contains

!> Set all properties to zero
subroutine dose_record_reset(this)
  use extran_param, only: WP, IP
  use m_constant, only: ZERO
  implicit none

  class(dose_record), intent(inout) :: this

  continue

  this%conc = ZERO
  this%expos = ZERO
  this%avconc = ZERO
  this%ttime = 0_IP
  this%npuffs = 0_IP

  return
end subroutine dose_record_reset

!> Set all properties to zero
subroutine dose_track_reset(this)
  use extran_param, only: WP, IP
  use m_constant, only: ZERO
  implicit none

  class(dose_track), intent(inout) :: this

  integer :: i

  continue

  do i = 1, size(this%datum)
    call this%datum(i)%reset()
  end do

  this%exposure = ZERO
  this%count = 0_IP
  this%start = 0_IP

  return
end subroutine dose_track_reset

end module m_print