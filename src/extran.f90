!> @file extran.f90
!! @author J. V. Ramsdell
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief EXTRAN main routine

!> @brief External Transport Dispersion Model For Control Room
!! Habitability Assessments
!!
!! Controlling program for estimating concentration time histories
!! following release of a gas or volatile liquid. Based on
!! NUREG/CR-5055 (@cite NUREG-CR-5656) and NUREG-0570 (@cite NUREG-0570)
program EXTRAN
!***********************************************************************
!     EXTRAN                                                 VERSION 1.2
!     External Transport Dispersion Model For Control Room
!     Habitability Assessments
!
!     J. V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington  99352
!
!     Created: 11/88
!     Updated: 10/90
!     Description:  Controlling program for estimating concentration
!                   time histories following release of a gas or
!                   volatile liquid. Based on NUREG/CR-5055 and
!                   NUREG-0570.
!
!     Relationship to other modules:
!         Makes calls to: CEPLOT, CHIT, INPUT, MODELPAR, NSIG, PUFFINIT,
!                         PUFFMASS, RINPUT
!         Called from: NONE
!***********************************************************************
  use, intrinsic :: iso_fortran_env, only: stdin => input_unit,         &
    stdout => output_unit
  use extran_file, only: outfile_config
  use extran_input, only: input, rinput, uc
  use extran_legacy, only: chit, modelpar, puffinit, puffmass
  use extran_output, only: ceplot
  use extran_param, only: WP, IP, NTIME, NMAXPUFFS
  use extran_timestamp, only: timestamp, create_timestamp
  use extran_wake, only: wake_condition, get_wake_conditions
  use m_effluent, only: effluent_properties
  use m_environ, only: environment_properties
  use m_options, only: run_count, concentration_config, output_config
  use m_pool, only: pool_condition, poolhx_condition
  use m_print, only: dose_track
  use m_puff, only: puff_template, puff_track
  use m_scenario, only: intake_properties, tank_properties, area,       &
    release_properties
  use m_constant, only: ZERO, ONE, THOUSAND, FORMFEED, LINEFEED

  implicit none

  integer, parameter :: TWO_MINUTES = 120

  ! character(len=1) :: UC

  type(timestamp) :: ts
  type(outfile_config) :: ocfg
  type(environment_properties) :: environment
  type(pool_condition) :: pool
  type(poolhx_condition) :: poolhx
  type(effluent_properties) :: effluent
  type(release_properties) :: release
  type(intake_properties) :: intake
  type(tank_properties) :: tank
  type(wake_condition) :: wake
  type(dose_track) :: dose_log
  type(puff_track) :: puff_log
  type(puff_template) :: flash_puff
  type(puff_template) :: evap_puff
  type(concentration_config) :: conc_cfg
  type(output_config) :: output_cfg

  integer(kind=IP) :: time
  integer(kind=IP) :: time_one
  integer(kind=IP) :: i
  integer(kind=IP) :: iflg
  integer(kind=IP) :: ITER

  real(kind=WP) :: maxconc
  real(kind=WP) :: maxtime
  real(kind=WP) :: cur_rel
  real(kind=WP) :: chimin

  character(len=1) :: yn
  character(len=70) :: title

  continue

  run_count = 1
  ts = create_timestamp()

  call ocfg%reset()

  write(stdout,11) ts%rdate, ts%rtime
11 format(10X,'PROGRAM RUN ', A10, '   AT ', A8)

!     NAME AND OPEN OUTPUT FILES

  call ocfg%set_filenames(ts%hhmm, run_count)

  call conc_cfg%reset()
  call output_cfg%reset()

  call environment%clear()
  call intake%reset()
  call tank%clear()
  call release%clear()
  call effluent%clear()
  call pool%clear()

  call input(title, ts%rdate, ts%rtime,  ocfg, conc_cfg, output_cfg,    &
    environment, pool, effluent, intake, release, tank)

! RE-ENTRY POINT FOR MULTIPLE MODEL RUNS

1000 continue

  iflg = 0
  time_one = 0

! COMPUTE MODEL PARAMETERS

  write(stdout,' (/10X,A)') 'COMPUTING MODEL RUN PARAMETERS '

  wake = get_wake_conditions(area, environment)

  call dose_log%reset()
  call puff_log%reset()
  call flash_puff%reset()
  call evap_puff%reset()
  call pool%reset()
  call poolhx%reset()

  call modelpar(environment, wake, intake, release, chimin)

  ! WRITE MODEL PARAMETER INFORMATION

  if (output_cfg%print_summary) then
    write(ocfg%uprint,'(//10X,A)')  ' MODEL PARAMETERS:'
    write(ocfg%uprint,'(10X,A,I4)')                                     &
      ' Puff Release Interval            (sec) = ',                     &
      release%puff_interval
    write(ocfg%uprint,'(10X,A,I4)')                                     &
      ' Time Step                        (sec) = ',                     &
      release%puff_duration
  end if
  if (output_cfg%save_summary) then
    write(ocfg%usum,'(//10X,A)') ' MODEL PARAMETERS:'
    write(ocfg%usum,'(10X,A,I4)')                                       &
      ' Puff Release Interval               (sec) = ',                  &
      release%puff_interval
      write(ocfg%usum,'(10X,A, I4)')                                    &
      ' Time Step                           (sec) = ',                  &
      release%puff_duration
  end if

  if (output_cfg%save_balance) then
    write(ocfg%ubal,12) TITLE, ts%rdate, ts%rtime
  end if
12 format(/7X, A70, /7X, 'Run on ', A10, ' at ', A8,                    &
  //7X, 'DATA FORMAT:', /7X,                                            &
  'MASS BALANCE: TANK, CURRENT RELEASE, POOL,',                         &
  ' FLASHED, EVAPORATED ' , /7X, 'POOL STATUS: VOLUME, RADIUS, ',       &
  'AREA, DEPTH, TEMPERATURE',/7X,'ENERGY BUDGET: NET SW, NET LW,',      &
  ' ATM CONV, GRND COND, NET FLUX ' /)

!    ENTERING TIME STEP LOOP

  ITER = 0
  dose_log%count    = 0
  maxconc  = ZERO
  pool%mass = ZERO
  write(stdout,' (/10X,A)') 'ENTERING DIFFUSION COMPUTATION LOOP'
  do time = 0, 1800, release%puff_duration

!     DETERMINE IF IT IS TIME TO RELEASE ANOTHER PUFF

    if ((mod(time, release%puff_interval) == 0)                         &
        .AND. ((tank%mass > ZERO)                                       &
        .OR. (pool%mass > ZERO))) then
      flash_puff%mass = ZERO
      evap_puff%mass = ZERO
      call puffmass(flash_puff, evap_puff, environment, pool, poolhx,   &
        effluent, release, tank, time)
      if (puff_log%numpuffs == 0_IP) then
        chimin = chimin * max(flash_puff%mass, evap_puff%mass)
      end if
      call puffinit(flash_puff, evap_puff, environment, pool,           &
        effluent, tank)
      call puff_log%release_puff(flash_puff)
      call puff_log%release_puff(evap_puff)

!    OUTPUT MASS AND ENERGY BALANCE

      if (release%is_liquid()) then
        cur_rel = release%mass + flash_puff%mass
      else
        cur_rel = flash_puff%mass
      end if

      if (output_cfg%save_balance) then
        ITER = ITER + 1_IP
        if (mod(ITER,11_IP) == 0_IP) then
          write(ocfg%ubal,12) TITLE, ts%rdate, ts%rtime
          ITER = 0
        end if

        write(ocfg%ubal,15)                                             &
          time, puff_log%numpuffs, tank%mass, cur_rel,                  &
          pool%mass, flash_puff%mass, evap_puff%mass, pool%volume,      &
          pool%radius, pool%area, pool%thickness, pool%temperature,     &
          poolhx%net_swrad, poolhx%net_lwrad, poolhx%air_conv,          &
          poolhx%grnd_cond, poolhx%net_flux
15      format(7x, 'TIME SINCE RELEASE = ',I4,5X,                       &
          'TOTAL NUMBER OF PUFFS RELEASED = ',                          &
          I4, /7X, 'MASS BALANCE ', 5F10.2, /7X, 'POOL STATUS  ',       &
          5F10.2, /7X, 'ENERGY BUDGET', 5F10.2)
      end if
    end if

!      MOVE AND DIFFUSE ALL PUFFS USING NRC SIGMA CURVES AND
!      BUILDING WAKE IF TV >= 0

    call puff_log%move_and_diffuse_all(environment, wake,               &
      release%puff_duration)

!     COMPUTE CONCENTRATION, EXPOSURE, AND MEAN CONCENTRATION
    call chit(puff_log, dose_log, time, time_one, chimin,               &
      environment, intake, release)
    if ((iflg == 0) .AND. (time_one > 0)) then
      iflg = 1
      if (output_cfg%print_summary) then

!     OUTPUT TO PRINTER

        write(ocfg%uprint,'(10X,A,I4)')                                 &
          'Delay Between Release and Intake    (sec) = ', time_one
        if (conc_cfg%ppm) then
          write(ocfg%uprint,'(10X,A,1PE11.2)')                          &
            ' Threshold Concentration            (ppm) = ',             &
            chimin * THOUSAND * effluent%ppmconv

          write(ocfg%uprint,'(10X,A,1PE11.2)')                          &
            ' To convert ppm to g/m**3, multiply by      ',             &
            ONE / effluent%ppmconv
        else if (conc_cfg%mci_per_m3) then
          write(ocfg%uprint,'(10X,A,1PE11.2)')                          &
          ' Threshold Concentration       (mCi/m**3) = ',               &
          chimin * THOUSAND
        else
          write(ocfg%uprint,' (10X,A,1PE11.2)')                         &
            ' Threshold Concentration         (g/m**3) = ',             &
            chimin * THOUSAND
          write(ocfg%uprint,' (10X,A,1PE11.2)')                         &
            ' To convert g/m**3 to ppm, multiply by      ',             &
            effluent%ppmconv
        end if
      end if

      if (output_cfg%save_summary) then

!         OUTPUT TO FILE

        write(ocfg%usum,'(10X,A,I4)')                                   &
          ' Delay Between Release and Intake    (sec) = ', time_one
        if (conc_cfg%ppm) then
          write(ocfg%usum,' (10X,A,1PE11.2) ')                          &
            ' Threshold Concentration             (ppm) = ',            &
          chimin * THOUSAND * effluent%ppmconv
          write(ocfg%usum,'(10X,A,1PE11.2)')                            &
            ' To convert ppm to g/m**3, multiply by       ',            &
            ONE / effluent%ppmconv
        else if (conc_cfg%mci_per_m3) then
          write(ocfg%usum,'(10X,A,1PE11.2)')                            &
            ' Threshold Concentration        (mCi/m**3) = ',            &
            chimin * THOUSAND
        else
          write(ocfg%usum,'(10X,A,1PE11.2)')                            &
            ' Threshold Concentration          (g/m**3) = ',            &
            chimin * THOUSAND
          write(ocfg%usum,'(10X,A,1PE11.2)')                            &
            ' To convert g/m**3 to ppm, multiply by       ',            &
            effluent%ppmconv
        end if
      end if

    end if

    if (dose_log%count <= 90) then
      CYCLE
    else if (dose_log%count == NTIME) then
      goto 2001
    else if ((dose_log%count == 91)                                     &
             .AND. (dose_log%datum(dose_log%count)%ttime > 270)) then
      goto 2001
    end if
  end do

2001 continue

  if (conc_cfg%ppm) then

!     CONVERT g/m**3 TO ppm

    do i = 1,dose_log%count
      dose_log%datum(i)%conc = dose_log%datum(i)%conc                   &
        * effluent%ppmconv
      dose_log%datum(i)%avconc = dose_log%datum(i)%avconc               &
        * effluent%ppmconv
    end do
  end if

  write(stdout,'(/10X,A)') 'WRITING OUT RESULTS'

  if (output_cfg%print_summary) then
    write(ocfg%uprint,'(//10X,A)') ' RESULTS:'
  end if
  if (output_cfg%save_summary) then
    write(ocfg%usum,'(//10X,A)') ' RESULTS:'
  end if

  do i=1, dose_log%count
    if ((dose_log%datum(i)%ttime <= TWO_MINUTES)                        &
        .AND. (dose_log%datum(i)%conc > maxconc)) then
      maxconc = dose_log%datum(i)%conc
      maxtime = dose_log%datum(i)%ttime
    end if
    if (((i < NTIME)                                                    &
         .AND. (dose_log%datum(i)%ttime <= TWO_MINUTES)                 &
         .AND. (dose_log%datum(i+1)%ttime > TWO_MINUTES))               &
       .OR. ((i == NTIME)                                               &
         .AND. (dose_log%datum(NTIME-1)%ttime < TWO_MINUTES))) then

      if (output_cfg%print_summary) then
        if (conc_cfg%ppm) then
          write(ocfg%uprint,40)                                         &
            ' Average Concentration During First Two Minutes',          &
            '      After Arrival of Plume                  (ppm) = ',   &
            dose_log%datum(i)%avconc
40        format(10X,A,/10x,A,1PE11.2)
        else if (conc_cfg%mci_per_m3) then
          write(ocfg%uprint,40)                                         &
          ' Average Concentration During First Two Minutes',            &
          '      After Arrival of Plume             (mCi/m**3) = ',     &
          dose_log%datum(i)%avconc
        else
          write(ocfg%uprint,40)                                         &
          ' Average Concentration During First Two Minutes',            &
          '      After Arrival of Plume               (g/m**3) = ',     &
          dose_log%datum(i)%avconc
        end if

        if (conc_cfg%mci_per_m3) then
          write(ocfg%uprint,60)                                         &
          ' Exposure Two Minutes After Arrival  (mCi-sec/m**3) = ',     &
          dose_log%datum(i)%expos
        else
          write(ocfg%uprint,60)                                         &
          ' Exposure Two Minutes After Arrival    (g-sec/m**3) = ',     &
          dose_log%datum(i)%expos

        end if
      end if

      if (output_cfg%save_summary) then
        if (conc_cfg%ppm) then
          write(ocfg%usum,40)                                           &
            ' Average Concentration During First Two Minutes',          &
            '      After Arrival of Plume                  (ppm) = ',   &
            dose_log%datum(i)%avconc
        else if (conc_cfg%mci_per_m3) then
          write(ocfg%usum,40)                                           &
            ' Average Concentration During First Two Minutes',          &
            '      After Arrival of Plume             (mCi/m**3) = ',   &
            dose_log%datum(i)%avconc
        else
          write(ocfg%usum,40)                                           &
            ' Average Concentration During First Two Minutes',          &
            'After Arrival of Plume                     (g/m**3) = ',   &
            dose_log%datum(i)%avconc
        end if

        if (conc_cfg%mci_per_m3) then
          write(ocfg%usum,60)                                           &
            ' Exposure Two Minutes After Arrival  (mCi-sec/m**3) = ',   &
            dose_log%datum(i)%expos
        else
          write(ocfg%usum,60)                                           &
            ' Exposure Two Minutes After Arrival    (g-sec/m**3) = ',   &
            dose_log%datum(i)%expos

        end if
      end if

      goto 3001

    end if
  end do

3001 continue

  if (output_cfg%print_summary) then
    write(ocfg%uprint,50)                                               &
      ' Time From Plume Arrival to Max. Conc.        (sec) = ',         &
      maxtime
50  format(10x,A,F5.0)
    if (conc_cfg%ppm) then
      write(ocfg%uprint,60)                                             &
        ' Max. Conc. in Two Minutes After Arrival      (ppm) = ',       &
        maxconc
60    format(10x,A,1PE11.2)
    else if (conc_cfg%mci_per_m3) then
      write(ocfg%uprint,60)                                             &
        ' Max. Conc. in Two Minutes After Arrival (mCi/m**3) = ',       &
        maxconc
    else
      write(ocfg%uprint,60)                                             &
        ' Max. Conc. in Two Minutes After Arrival   (g/m**3) = ',       &
        maxconc
    end if
  end if

  if (output_cfg%save_summary) then
    write(ocfg%usum,50)                                                 &
      ' Time From Plume Arrival to Max. Conc.        (sec) = ',         &
      maxtime
    if (conc_cfg%ppm) then
      write(ocfg%usum,60)                                               &
        ' Max. Conc. in Two Minutes After Arrival      (ppm) = ',       &
        maxconc
    else if (conc_cfg%mci_per_m3) then
      write(ocfg%usum,60)                                               &
        ' Max. Conc. in Two Minutes After Arrival (mCi/m**3) = ',       &
        maxconc
    else
      write(ocfg%usum,60)                                               &
        ' Max. Conc. in Two Minutes After Arrival (mCi/m**3) = ',       &
        maxconc
    end if
  end if

  if (output_cfg%print_summary                                          &
      .AND. (output_cfg%save_summary .OR. output_cfg%save_chronology    &
             .OR. output_cfg%save_balance)) then
    write(ocfg%uprint, '(/10X,A)') 'OUTPUT FILES: '
    if (output_cfg%save_summary) then
      write(ocfg%uprint,' (15X,A)') ocfg%summary_file
    end if
    if (output_cfg%save_chronology) then
      write(ocfg%uprint,'(15X,A)') ocfg%chronology_file
    end if
    if (output_cfg%save_balance) then
      write(ocfg%uprint,'(15X,A)') ocfg%balance_file
    end if
  end if

  if (output_cfg%save_summary .AND. (output_cfg%save_chronology         &
      .OR. output_cfg%save_balance)) then
    write(ocfg%usum, '(/10X,A)') 'ADDITIONAL OUTPUT FILES: '
    if (output_cfg%save_chronology) then
      write(ocfg%usum,'(15X,A)') ocfg%chronology_file
    end if
    if (output_cfg%save_balance) then
      write(ocfg%usum,'(15X,A)') ocfg%balance_file
    end if
  end if

!     PLOT DATA TO PRINTER

  if (output_cfg%print_plot) then
    call ceplot(title, ts%rdate, ts%rtime, effluent%name, dose_log,     &
      ocfg, conc_cfg)
  end if

  ! WRITE INFORMATION TO THE PRINTER
  ! IF output_cfg%print_chronology IS TRUE

  if (output_cfg%print_chronology) then
    write(ocfg%uprint,*) FORMFEED
    write(ocfg%uprint,19) TITLE, ts%rdate, ts%rtime
19  format(7X, A70, //7X, 'Run on ', A10, ' at ', A8)
    write(ocfg%uprint,*) LINEFEED

    write(ocfg%uprint,20)
20  format(7X,' TIME',3X,'CONCENTRATION',4X,'    EXPOSURE',             &
      4X,'  MEAN CONC.',3X, 'NUM OF PUFFS')
    if (conc_cfg%ppm) then
      write(ocfg%uprint,30)
30    format(7X,'(sec)',11X,'(ppm)',4X,'(g-sec/m**3)',11X,'(ppm)')
      write(ocfg%uprint,*) LINEFEED
    else if (conc_cfg%mci_per_m3) then
      write(ocfg%uprint,37)
37    format(7X,'(sec)',6X,'(mCi/m**3)',2X,'(mCi-sec/m**3)',5X,         &
        '(mCi/m**3)')
      write(ocfg%uprint,*) LINEFEED
    else
      write(ocfg%uprint,31)
31    format (7X,'(sec)',8X,'(g/m**3)',4X,'(g-sec/m**3)',8X,            &
        '(g/m**3)')
      write(ocfg%uprint,*) LINEFEED
    end if

    do i=1, 55
      write(ocfg%uprint,32) dose_log%datum(i)%ttime,                    &
        dose_log%datum(i)%conc, dose_log%datum(i)%expos,                &
        dose_log%datum(i)%avconc, dose_log%datum(i)%npuffs
32    format(8X, I4, 3(4X, 1PE12.2), 6X, I4)
      if (i >= dose_log%count) goto 6001
    end do

    write(ocfg%uprint,*) FORMFEED
    write(ocfg%uprint,19) TITLE, ts%rdate, ts%rtime
    write(ocfg%uprint,20)
    if (conc_cfg%ppm) then
      write(ocfg%uprint,30)
    else if (conc_cfg%mci_per_m3) then
      write(ocfg%uprint,37)
    else
      write(ocfg%uprint,31)
    end if

    do i =56, 111
      write(ocfg%uprint,32) dose_log%datum(i)%ttime,                    &
        dose_log%datum(i)%conc, dose_log%datum(i)%expos,                &
        dose_log%datum(i)%avconc, dose_log%datum(i)%npuffs
      if (i >= dose_log%count) goto 6001
    end do

    write(ocfg%uprint,*) FORMFEED
    write(ocfg%uprint,19) TITLE, ts%rdate, ts%rtime
    write(ocfg%uprint,20)
    if (conc_cfg%ppm) then
      write(ocfg%uprint,30)
    else if (conc_cfg%mci_per_m3) then
      write(ocfg%uprint,37)
    else
      write(ocfg%uprint,31)
    end if

    do i=112, 167
      write(ocfg%uprint,32) dose_log%datum(i)%ttime,                    &
        dose_log%datum(i)%conc, dose_log%datum(i)%expos,                &
        dose_log%datum(i)%avconc, dose_log%datum(i)%npuffs
      if (i >= dose_log%count) goto 6001
    end do

    write(ocfg%uprint,*) FORMFEED
    write(ocfg%uprint,19) TITLE, ts%rdate, ts%rtime
    write(ocfg%uprint,20)
    if (conc_cfg%ppm) then
      write(ocfg%uprint,30)
    else if (conc_cfg%mci_per_m3) then
      write(ocfg%uprint,37)
    else
      write(ocfg%uprint,31)
    end if

    do i=168, NTIME
      write(ocfg%uprint,32) dose_log%datum(i)%ttime,                    &
        dose_log%datum(i)%conc, dose_log%datum(i)%expos,                &
        dose_log%datum(i)%avconc, dose_log%datum(i)%npuffs
      if (i >= dose_log%count) goto 6001
    end do
6001 continue
    write(ocfg%uprint,*) FORMFEED
  end if

!     WRITE RESULTS TO A FILE IF output_cfg%save_chronology IS TRUE

  if (output_cfg%save_chronology) then
    write(ocfg%ucron,19) TITLE, ts%rdate, ts%rtime
    write(ocfg%ucron,20)
    if (conc_cfg%ppm) then
      write(ocfg%ucron,30)
    else if (conc_cfg%mci_per_m3) then
      write(ocfg%ucron,37)
    else
      write(ocfg%ucron,31)
    end if
    do i = 1,dose_log%count
      write(ocfg%ucron,32) dose_log%datum(i)%ttime,                     &
        dose_log%datum(i)%conc, dose_log%datum(i)%expos,                &
        dose_log%datum(i)%avconc, dose_log%datum(i)%npuffs
    end do
  end if

9000 continue

  if (output_cfg%print_summary) then
    close(ocfg%uprint)
  end if
  if (output_cfg%save_summary) then
    close(ocfg%usum)
  end if
  if (output_cfg%save_chronology) then
    close(ocfg%ucron)
  end if
  if (output_cfg%save_balance) then
    close(ocfg%ubal)
  end if

  write(unit=stdout, fmt='(//10X,A,/10X,A) ', advance='NO')             &
    'EXTRAN RUN COMPLETE. DO YOU WISH TO REVISE THE ENVIRONMENTAL',     &
    'CONDITIONS AND RUN ANOTHER CASE? Y OR N? '

  read(stdin,' (A)') yn
  if (UC(yn) == 'Y') then
    run_count = run_count + 1_IP

    ! Update output file names with new run_count
    call ocfg%set_filenames(ts%hhmm, run_count)

!...... RESET ARRAYS TO ZERO

    write(unit=stdout, fmt='(/10X,A)', advance='NO') 'RESETTING ARRAYS'

    ! call dose_log%reset()
    ! call puff_log%reset()

    call tank%reset()
    call release%reset()

    ! call pool%reset()
    ! call poolhx%reset()

!...... START DATA REVISION

    call RINPUT(title, ts%rdate, ts%rtime, ocfg, conc_cfg, output_cfg,  &
      environment, pool, effluent, intake, release, tank)

    goto 1000

  else if (UC(yn) /= 'N') then
    goto 9000
  end if

end program EXTRAN
