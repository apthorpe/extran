!> @file m_effluent.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Shared effluent data module

!> @brief Shared effluent data module
!!
!! Contains effluent information and constants used throughout the
!! EXTRAN code.
module m_effluent
!***********************************************************************
!     m_effluent.f90                                  EXTRAN Version 1.2
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description: Contains effluent information and constants used
!                  throughout the EXTRAN code.
!
!     Included in modules: DIFCOEF, EXTRAN, INPUT, INPUTEF, PUFFINIT,
!                          PUFFMASS, RINPUT, RINPUTE
!
!***********************************************************************
use extran_param, only: WP
use m_constant, only: ZERO
implicit none

public

!> Chemical property database entry
type, public :: effluent_properties
  !> Species name
  character(len=20) :: name = '__DEFAULT__         '

  !> Molecular (formula) weight, \f$\si{\gram\per\mol}\f$
  real(kind=WP) :: molec_wt = ZERO

  !> Source density, \f$\si{\gram\per\cubic\centi\meter}\f$
  real(kind=WP) :: source_density = ZERO

  !> Vapor density, \f$\si{\gram\per\cubic\centi\meter}\f$
  real(kind=WP) :: vapor_density = ZERO

  !> Reference temperature, \f$\si{\celsius}\f$
  real(kind=WP) :: ref_temperature = ZERO

  !> Boiling point at standard conditions, \f$\si{\celsius}\f$
  real(kind=WP) :: std_boiling_point = ZERO

  !> Boiling point at local conditions, \f$\si{\celsius}\f$
  real(kind=WP) :: boiling_point = ZERO

  !> Specific heat, \f$\si{\joule\per\gram\per\kelvin}\f$
  real(kind=WP) :: cp = ZERO

  !> Latent heat of vaporization, \f$\si{\joule\per\gram}\f$
  real(kind=WP) :: hv = ZERO

  !> Vapor pressure, \f$\si{\milli\meter}\f$ Hg
  real(kind=WP) :: vapor_pressure = ZERO

  !> Saturation pressure, \f$\si{\milli\meter}\f$ Hg
  real(kind=WP) :: sat_pressure = ZERO

  !> Initial (reference) molecular diffusion coefficient,
  !! \f$\si{\square\centi\meter\per\second}\f$
  real(kind=WP) :: initial_diff_coef = ZERO

  !> Condition dependent molecular diffusion coefficient,
  !! \f$\si{\square\centi\meter\per\second}\f$
  real(kind=WP) :: diff_coef = ZERO

  !> Dimensionless factor relating molecular weight, latent heat of
  !! vaporization, universal gas constant, and temperature:
  !! \f$\dfrac{\omega h_{v}}{R T}\f$
  real(kind=WP) :: pconst = ZERO

  !> Conversion factor from \f$\si{\gram\per\cubic\centi\meter}\f$
  !! to parts per million
  real(kind=WP) :: ppmconv = ZERO

  contains

  !> Set effluent properties to zero
  procedure :: clear => effluent_properties_clear
  !> Set effluent properties from chemical object
  procedure :: from_chemical => effluent_properties_from_chemical
  !> Set boiling point at given pressure
  procedure :: calculate_ref_and_boil_temp =>                           &
    effluent_properties_calculate_ref_and_boil_temp
  !> Calculate pconst coeffiecient
  procedure :: calculate_pconst =>                                      &
    effluent_properties_calculate_pconst
  !> Calculate gram-per-cubic-centimeter density to ppm based on air
  !! pressure
  procedure :: calculate_ppm_conversion =>                              &
    effluent_properties_calculate_ppm_conversion
  !> Estimate diffusion coefficient of effluent
  procedure :: estimate_diff_coeff =>                                   &
    effluent_properties_estimate_diff_coeff

end type effluent_properties

!       COMMON /effluent/ src_density, cp, hv, std_boil_pt, vapor_density,
!      .                  vapor_press, diff_coef, sat_press, molec_wt,
!      .                  pconst, tref, ppmconv, boil_point, idiff_coef

contains

!> Set effluent properties to zero
subroutine effluent_properties_clear(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(effluent_properties), intent(inout) :: this

  continue

  this%name = '__DEFAULT__         '
  this%molec_wt = ZERO
  this%source_density = ZERO
  this%vapor_density = ZERO
  this%ref_temperature = ZERO
  this%std_boiling_point = ZERO
  this%boiling_point = ZERO
  this%cp = ZERO
  this%hv = ZERO
  this%vapor_pressure = ZERO
  this%sat_pressure = ZERO
  this%initial_diff_coef = ZERO
  this%diff_coef = ZERO
  this%pconst = ZERO
  this%ppmconv = ZERO

  return
end subroutine effluent_properties_clear

!> Set effluent properties from chemical object
subroutine effluent_properties_from_chemical(this, chem)
  use extran_chemical, only: chemical
  implicit none

  !> Object reference
  class(effluent_properties), intent(inout) :: this

  !> Chemical object reference
  type(chemical), intent(in) :: chem

  continue

  this%name              = chem%name
  this%molec_wt          = chem%molec_wt
  this%std_boiling_point = chem%std_boil_pt
  this%cp                = chem%cp
  this%hv                = chem%hv
  this%source_density    = chem%density
  this%initial_diff_coef = chem%diff_coef
  this%diff_coef         = this%initial_diff_coef

  return
end subroutine effluent_properties_from_chemical

!> Set boiling point at given pressure
subroutine effluent_properties_calculate_ref_and_boil_temp(this,        &
  air_press)
  use extran_param, only: WP
  use m_constant, only: ZERO, ONE, STD_TEMP, STD_PRESSURE_MMHG,         &
    ugcnst => RGAS
  implicit none

  !> Object reference
  class(effluent_properties), intent(inout) :: this

  !> Ambient air pressure, \f$\si{\milli\meter}\f$ Hg
  real(kind=WP), intent(in) :: air_press

  continue

  this%boiling_point = (this%std_boiling_point + STD_TEMP)              &
    / (ONE + ugcnst * (this%std_boiling_point + STD_TEMP)               &
    * (log(STD_PRESSURE_MMHG) - log(air_press))                         &
    / (this%hv * this%molec_wt)) - STD_TEMP

  if (this%boiling_point < ZERO) then
    this%ref_temperature = this%boiling_point + STD_TEMP
  else
    this%ref_temperature = STD_TEMP
  end if

  return
end subroutine effluent_properties_calculate_ref_and_boil_temp

!> Calculate pconst coeffiecient
subroutine effluent_properties_calculate_pconst(this)
  use extran_param, only: WP
  use m_constant, only: STD_TEMP, ugcnst => RGAS
  implicit none

  !> Object reference
  class(effluent_properties), intent(inout) :: this

  continue

  this%pconst = (this%molec_wt * this%hv)                               &
    / (ugcnst * (this%boiling_point + STD_TEMP))

  return
end subroutine effluent_properties_calculate_pconst

!> Calculate gram-per-cubic-centimeter density to ppm based on air
!! pressure
subroutine effluent_properties_calculate_ppm_conversion(this,           &
  environment)
  use extran_param, only: WP
  use m_environ, only: environment_properties
  use m_constant, only: ZERO, ONE, STD_TEMP, STD_PRESSURE_MMHG,         &
    ugcnst => RGAS
  implicit none

  !> Object reference
  class(effluent_properties), intent(inout) :: this

  !> Environment properties object reference
  type(environment_properties), intent(in) :: environment

  real(kind=WP) :: mol_vol

  continue

  mol_vol =                                                             &
    22.414E-3_WP * (STD_PRESSURE_MMHG / environment%air_pressure)       &
    * ((environment%air_temperature + STD_TEMP) / STD_TEMP)

  this%ppmconv = mol_vol * 1.0E6_WP / this%molec_wt

  return
end subroutine effluent_properties_calculate_ppm_conversion

!> Estimate diffusion coefficient of effluent
subroutine effluent_properties_estimate_diff_coeff(this, air_press)
  use, intrinsic :: iso_fortran_env, only: stdout => OUTPUT_UNIT
  use extran_param, only: WP
  use m_constant, only: ZERO, THIRD, HALF, ONE, STD_TEMP,               &
    STD_PRESSURE_MMHG, ugcnst => RGAS, mwair => MW_AIR
  implicit none

  !> Object reference
  class(effluent_properties), intent(inout) :: this

  !> Ambient air pressure, \f$\si{\milli\meter}\f$ Hg
  real(kind=WP), intent(in) :: air_press

!  real(kind=WP), parameter :: mwair = 29.87_WP
  real(kind=WP), parameter :: ekair = 97.0_WP
  real(kind=WP), parameter :: r0air = 3.617_WP

  real(kind=WP) :: ekgas
  real(kind=WP) :: r0gas
  real(kind=WP) :: r012
  real(kind=WP) :: tref3
  real(kind=WP) :: ek12
  real(kind=WP) :: kte
  real(kind=WP) :: colint

  continue

  ! COMPUTE THE COLLISION DIAMETER
  ! BS&L Equations 1.4-15 and 16.4-15

  r0gas = 1.166_WP * (this%molec_wt / this%source_density)**THIRD
  r012 = HALF * (r0air + r0gas)

  ! COMPUTE THE epsilon/k RATIO FOR THE MATERIAL
  ! BS&L Equations 1.4-14 and 16.4-16

  ekgas = 1.15_WP * (this%boiling_point + STD_TEMP)
  ek12 = sqrt(ekair * ekgas)

  ! ESTIMATE THE COLLISION INTEGRAL FROM THE epsilon/k RATIO AND
  ! REFERENCE TEMPERATURE. THE VALUES IN B S AND L'S TABLE B-2
  ! ARE REPRESENTED BY POWER FUNCTIONS FOR THREE RANGES OF
  ! kT/epsilon.

  tref3 = this%ref_temperature**3
  kte = this%ref_temperature / ek12
  if (kte <= 1.6_WP) then
    colint = 1.471_WP * kte**(-0.4926_WP)
  else if (kte <= 5.0_WP) then
    colint = 1.3351_WP * kte**(-0.2863_WP)
  else
    colint = 1.0946_WP * kte**(-0.1629_WP)
  end if

  ! COMPUTE DIFFUSION COEFFICIENT
  ! BS &L Equation 16.4-13

  this%diff_coef = 0.0018583_WP * sqrt(tref3                            &
    * (ONE / mwair + ONE / this%molec_wt))                              &
    / ((air_press / STD_PRESSURE_MMHG) * r012**2 * colint)

  return
end subroutine effluent_properties_estimate_diff_coeff

end module m_effluent