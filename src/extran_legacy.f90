!> @file extran_legacy.f90
!! @author J. V. Ramsdell
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains EXTRAN legacy routines

!> @brief Contains EXTRAN legacy routines
module extran_legacy
  implicit none

  private

  public :: chit
  public :: modelpar
  public :: puffinit
  public :: puffmass

contains

!> @brief Computes Puff Concentration
!!
!! Computes concentration within the puffs.
subroutine CHIT(puff_log, dose_log, time, time_one, chimin,             &
  environment, intake, release)
!***********************************************************************
!
!     CHIT                                            EXTRAN Version 1.2
!     Computes Puff Concentration
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 10/88
!     Updated: 10/90
!
!     Description: Computes concentration within the puffs.
!
!     Relationship to other modules:
!
!         Makes calls to: NONE
!
!         Called from:    EXTRAN
!
!***********************************************************************
  use extran_param, only: WP, IP, NTIME
  use m_environ, only: environment_properties
  use m_print, only: dose_track
  use m_puff, only: puff_track
  use m_scenario, only: intake_properties, release_properties
  use m_constant, only: ZERO, HALF, ONE, TWO, PI, THOUSAND

  implicit none

  real(kind=WP), parameter :: twopi = TWO * PI

  !> List of released puffs
  type(puff_track), intent(in) :: puff_log

  !> Concentration and exposure profile
  type(dose_track), intent(inout) :: dose_log

  !> Scenario time, \f$\si{\second}\f$
  integer(kind=IP), intent(in) :: time

  !> Delay between puff release and intake, \f$\si{\second}\f$
  integer(kind=IP), intent(inout) :: time_one

  !> Minimum concentration, UNIT
  real(kind=WP), intent(in) :: chimin

  !> Environment property object
  type(environment_properties), intent(in) :: environment

  !> Intake property object
  type(intake_properties), intent(in) :: intake

  !> Release property object
  type(release_properties), intent(in) :: release

  real(kind=WP) :: chi_cc
  real(kind=WP) :: f_of_y
  real(kind=WP) :: g_of_h
  real(kind=WP) :: radius
  real(kind=WP) :: meanchi
  real(kind=WP) :: chi
  real(kind=WP) :: leadedge
  real(kind=WP) :: chiprint
  real(kind=WP) :: meanchiprint
  real(kind=WP) :: exposprint
  real(kind=WP) :: center
  real(kind=WP) :: trailedge

  integer(kind=IP) :: i
  integer(kind=IP) :: pstart

  continue

! COMPUTE CONCENTRATION, EXPOSURE, AND MEAN CONCENTRATION

  chi = ZERO
  pstart = max(1_IP, dose_log%start)
  do i = pstart, puff_log%numpuffs
    center = puff_log%puff(i)%age * environment%ubar
    radius = 4.24_WP * puff_log%puff(i)%tsigy
    leadedge = center + radius
    trailedge = center - radius
    if (leadedge >= intake%distance) then
      chi_cc = puff_log%puff(i)%mass                                    &
             / (twopi**1.5 * puff_log%puff(i)%tsigy**2                  &
             * puff_log%puff(i)%tsigz                                   &
             + release%puff_interval * release%vent_flow)
      f_of_y = exp(-HALF * ((intake%distance                            &
             - (puff_log%puff(i)%age * environment%ubar))                           &
             / puff_log%puff(i)%tsigy)**2)

      g_of_h = exp(-HALF * ((release%height - intake%height)            &
             / puff_log%puff(i)%tsigz)**2)                              &
             + exp(-HALF * ((release%height + intake%height)            &
             / puff_log%puff(i)%tsigz)**2)
      chi = chi + (chi_cc * f_of_y * g_of_h)
      if (time_one == 0) then
        if (chi >= chimin) then
          time_one = time
        else
          return
        end if
      end if
    end if
    if (trailedge > intake%distance) then
      dose_log%start = dose_log%start + 1_IP
    end if
  end do
  dose_log%exposure = dose_log%exposure + (chi * release%puff_duration)
  meanchi = dose_log%exposure                                           &
          / (time - time_one + release%puff_duration)

!....... CONVERT FROM KG/M**3 TO GM/M**3

  chiprint = chi * THOUSAND
  exposprint = dose_log%exposure * THOUSAND
  meanchiprint = meanchi * THOUSAND
  if ((time_one > 0_IP) .AND. (dose_log%count <= NTIME)) then
    dose_log%count = dose_log%count + 1_IP
    dose_log%datum(dose_log%count)%npuffs = puff_log%numpuffs
    dose_log%datum(dose_log%count)%ttime  = time - time_one
    dose_log%datum(dose_log%count)%conc   = chiprint
    dose_log%datum(dose_log%count)%expos  = exposprint
    dose_log%datum(dose_log%count)%avconc = meanchiprint
  end if

  return
end subroutine CHIT

!> @brief Computes Model Parameters
!!
!! Computes model parameters such as puff release interval, delta time,
!! time one
subroutine MODELPAR(environment, wake, intake, release, chimin)
!***********************************************************************
!     MODELPAR                                        EXTRAN Version 1.2
!     Computes Model Parameters
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description: Computes model parameters such as puff release interval,
!                  delta time, time one
!
!     Relationship to other modules:
!
!         Makes calls to:  NSIG,
!                          extran_wake::wake_condition%get_sigma_y,
!                          extran_wake::wake_condition%get_sigma_z
!
!         Called from:     EXTRAN
!
!***********************************************************************
  use extran_param, only: WP, IP
  use extran_wake, only: wake_condition
  use m_environ, only: environment_properties
  use m_puff, only: nsig
  use m_scenario, only: area, intake_properties, release_properties
  use m_constant, only: ZERO, ONE, TWO, THOUSAND, PI

  implicit none

  real(kind=WP), parameter :: LONG_DISTANCE = THOUSAND

  !> Environment property object
  type(environment_properties), intent(in) :: environment

  !> Wake condition object
  type(wake_condition), intent(in) :: wake

  !> Control room air intake object
  type(intake_properties), intent(in) :: intake

  !> Chemical release object
  type(release_properties), intent(inout) :: release

  !> Minimum concentration
  real(kind=WP), intent(out) :: chimin

  real(kind=WP) :: sigmay
  real(kind=WP) :: sigmaz
  real(kind=WP) :: tty
  real(kind=WP) :: wsigy
  real(kind=WP) :: wsigz
  real(kind=WP) :: tsigy
  real(kind=WP) :: tsigz

  continue

! COMPUTE PUFF RELEASE INTERVAL

  sigmay = ZERO
  sigmaz = ZERO
  call nsig(environment%stability_class, intake%distance,               &
    sigmay, sigmaz)
  tty = intake%distance / environment%ubar
  if (area > ZERO) then
    wsigy = wake%get_sigma_y(tty)
    wsigz = wake%get_sigma_z(tty)
  else
    wsigy = ZERO
    wsigz = ZERO
  end if
  tsigy = sqrt(sigmay**2 + wsigy**2)
  tsigz = sqrt(sigmaz**2 + wsigz**2)

  chimin = 1.0E-4_WP * ONE                                              &
    / ((TWO * PI)** (1.5_WP) * tsigy**2 * tsigz)
  release%puff_interval = int(TWO * tsigy / environment%ubar, IP)
  if (release%puff_interval <= 1_IP) then
    release%puff_interval = 1_IP
  else if (release%puff_interval > 10_IP) then
    if (intake%distance >= LONG_DISTANCE) then
      release%puff_interval = min(30_IP,release%puff_interval)
    else
      release%puff_interval = 10_IP
    end if
  else
    release%puff_interval = release%puff_interval                       &
      - mod(release%puff_interval,2_IP)
  end if

! COMPUTE DELTA TIME

  release%puff_duration = max(release%puff_interval / 2_IP, 1_IP)
  ! if (release%puff_duration == 0_IP) release%puff_duration = 1_IP

  return
end subroutine MODELPAR

!> @brief Computes initial puff dimensions and virtual travel time
subroutine PUFFINIT(flash_puff, evap_puff, environment, pool, effluent, &
  tank)
!***********************************************************************
!     PUFFINIT                                        EXTRAN Version 1.2
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description: Computes initial puff dimensions and virtual times.
!
!     Relationship to other modules:
!
!         Makes calls to:  NONE
!
!         Called from:     EXTRAN
!
!***********************************************************************
  use extran_param, only: WP, IP, NSTAB
  use extran_wake, only: wake_condition, get_wake_conditions
  use m_effluent, only: effluent_properties
  use m_environ, only: environment_properties
  use m_pool, only: pool_condition
  use m_puff, only: puff_template
  use m_scenario, only: tank_properties, area
  use m_constant, only: ZERO, TENTH, THIRD, HALF, ONE, TWO, PI, FOUR,   &
    TEN, THOUSAND, STD_TEMP, STD_PRESSURE_MMHG

  implicit none

  ! Constant in Wake Diffusion Equation
  real(kind=WP), parameter :: constant = HALF

  real(kind=WP), parameter :: twopi = TWO * PI

  ! Gas constant in (m^3 atm)/(kg-mole K) = 8.3144 / 101.325
  real(kind=WP), parameter :: ugcnst = 8.2057E-2_WP

  !> Initial conditions of puff released as gas or due to liquid flashing
  type(puff_template), intent(inout) :: flash_puff

  !> Initial conditions of puff released due to evaporation
  type(puff_template), intent(inout) :: evap_puff

  !> Environment property object
  type(environment_properties), intent(in) :: environment

  !> Pool object reference
  type(pool_condition), intent(in) :: pool

  !> Chemical effluent object reference
  type(effluent_properties), intent(inout) :: effluent

  !> Chemical storage tank object reference
  type(tank_properties), intent(in) :: tank

  type(wake_condition) :: wake

  real(kind=WP) :: ftv
  real(kind=WP) :: ftvprime
  real(kind=WP) :: differ
  real(kind=WP) :: c1
  real(kind=WP) :: c2
  real(kind=WP) :: puff_vol

  integer(kind=IP) :: kount

  continue

  !     COMPUTE TIME SCALES FOR WAKE DIFFUSION MODEL

  wake = get_wake_conditions(area, environment)

!     COMPUTE INITIAL SIGMA YAND Z FOR PUFF FROM FLASHING

  flash_puff%isigy = ZERO
  flash_puff%isigz = ZERO
  if (flash_puff%mass > ZERO) then
    puff_vol = ugcnst * (flash_puff%mass / effluent%molec_wt)           &
      * (tank%temperature + STD_TEMP)                                   &
      / (environment%air_pressure * THOUSAND / STD_PRESSURE_MMHG)
    effluent%vapor_density = flash_puff%mass / puff_vol
    if (effluent%vapor_density < TWO) then
      flash_puff%isigy = puff_vol**THIRD / sqrt(TWOPI)
      flash_puff%isigz = flash_puff%isigy
    else
      flash_puff%isigz = ONE
      flash_puff%isigy = sqrt(puff_vol / (TWOPI * flash_puff%isigz))
    end if
  end if

!     COMPUTE INITIAL SIGMA Y AND SIGMA Z FOR EVAPORATION OF
!     SPILLED LIQUID

  evap_puff%isigy = ZERO
  evap_puff%isigz = ZERO
  if (evap_puff%mass > ZERO) then
    evap_puff%isigy = sqrt(pool%area / (FOUR * pi))
    evap_puff%isigz = TENTH
  end if

!     COMPUTE VIRTUAL TIMES FOR WAKE MODEL FOR PUFF FROM FLASHING

  if ((flash_puff%isigy**2) >= (constant*wake%tsh**2)                   &
      .OR. (flash_puff%isigz**2) >= (constant*wake%tsv**2)) then
      flash_puff%tvy = -ONE
      flash_puff%tvz = -ONE
  else

!     USE NEWTON-RAPHSON METHOD TO ESTIMATE VIRTUAL TIMES FOR
!     WAKE MODEL

!     INITIAL GUESSES
    flash_puff%tvy = ONE
    flash_puff%tvz = ONE

!     CONSTANT TERMS

    c1 = wake%tsh                                                       &
       * log(ONE - flash_puff%isigy**2 / (constant * wake%tsh**2))
    c2 = wake%tsv                                                       &
       * log(ONE - flash_puff%isigz**2 / (constant * wake%tsv**2))

!     ESTIMATE VIRTUAL TIME FOR HORIZONTAL DIFFUSION
    kount = 0_IP
    differ = ONE
    do while (abs(differ) >= 0.01_WP)
      if (kount > 20_IP) stop 'NO CONVERGENCE IN NEWTON'
      kount = kount + 1_IP
      ftv = flash_puff%tvy                                              &
          - wake%tsh * log(ONE + flash_puff%tvy / wake%tsh) + c1
      ftvprime = flash_puff%tvy / (wake%tsh + flash_puff%tvy)
      differ = - ftv / ftvprime
      flash_puff%tvy = flash_puff%tvy + differ
    end do

!     ESTIMATE VIRTUAL TIME FOR VERTICAL DIFFUSION

    kount = 0_IP
    differ = ONE
    do while (abs(differ) >= 0.01_WP)
      if (kount > 20_IP) stop 'NO CONVERGENCE IN NEWTON '
      kount = kount + 1_IP
      ftv = flash_puff%tvz                                              &
          - wake%tsv * log(ONE + flash_puff%tvz / wake%tsv) + c2
      ftvprime = flash_puff%tvz / (wake%tsv + flash_puff%tvz)
      differ = - ftv / ftvprime
      flash_puff%tvz = flash_puff%tvz + differ
    end do

!     INITIAL PUFF SIZE INCLUDED IN WAKE MODEL DELETE FROM NORMAL
!     DIFFUSION

    flash_puff%isigy = ZERO
    flash_puff%isigz = ZERO
  end if

!     COMPUTE VIRTUAL TIMES FOR WAKE MODEL FOR PUFF FROM EVAPORATION

  if ((evap_puff%isigy**2) >= (constant*wake%tsh**2) .OR.               &
    (evap_puff%isigz**2) >= (constant*wake%tsv**2)) then
    evap_puff%tvy = -ONE
    evap_puff%tvz = -ONE
  else

!     USE NEWTON-RAPHSON METHOD TO ESTIMATE VIRTUAL TIMES FOR
!     WAKE MODEL

!     INITIAL GUESSES
    evap_puff%tvy = ONE
    evap_puff%tvz = ONE

!     CONSTANT TERMS

    c1 = wake%tsh                                                       &
       * log(ONE - evap_puff%isigy**2 / (constant * wake%tsh**2))
    c2 = wake%tsv                                                       &
       * log(ONE - evap_puff%isigz**2 / (constant * wake%tsv**2))

!     ESTIMATE VIRTUAL TIME FOR HORIZONTAL DIFFUSION

    kount = 0_IP
    differ = ONE
    do while (abs(differ) >= 0.01_WP)
      if (kount > 20_IP) stop 'NO CONVERGENCE IN NEWTON '
      kount = kount + 1_IP
      ftv = evap_puff%tvy                                               &
          - wake%tsh * log(ONE + evap_puff%tvy / wake%tsh) + c1
      ftvprime = evap_puff%tvy / (wake%tsh + evap_puff%tvy)
      differ = - ftv / ftvprime
      evap_puff%tvy = evap_puff%tvy + differ
    end do

!     ESTIMATE VIRTUAL TIME FOR VERTICAL DIFFUSION

    kount = 0
    differ = ONE
    do while (abs(differ) >= 0.01_WP)
      if (kount > 20_IP) stop 'NO CONVERGENCE IN NEWTON'
      kount = kount + 1_IP
      ftv = evap_puff%tvz                                               &
          - wake%tsv * log(ONE + evap_puff%tvz / wake%tsv) + c2
      ftvprime = evap_puff%tvz / (wake%tsv + evap_puff%tvz)
      differ = - ftv / ftvprime
      evap_puff%tvz = evap_puff%tvz + differ
    end do

!     INITIAL PUFF SIZE INCLUDED IN WAKE MODEL DELETE FROM NORMAL
!     DIFFUSION

    evap_puff%isigy = ZERO
    evap_puff%isigz = ZERO

  end if

  return
end subroutine PUFFINIT

!> @brief Computes initial mass of material in a puff
subroutine PUFFMASS(flash_puff, evap_puff, environment, pool, poolhx,   &
  effluent, release, tank, time)
!***********************************************************************
!     PUFFMASS                                        EXTRAN Version 1.2
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 10/88
!     Updated: 10/90
!
!     Description: Computes initial mass of material in a puff.
!
!     Relationship to other modules:
!
!         Makes calls to:  NONE
!
!         Called from:     EXTRAN
!
!***********************************************************************
  use, intrinsic :: iso_fortran_env, only: stdout => output_unit
  use extran_param, only: WP, IP
  use m_effluent, only: effluent_properties
  use m_environ, only: environment_properties
  use m_pool, only: pool_condition, poolhx_condition
  use m_puff, only: puff_template
  use m_scenario, only: tank_properties, release_properties
  use m_constant, only: ZERO, TENTH, THIRD, HALF, ONE, TWO, PI,         &
    g => GRAV, sb_const => SIGMA_SB, STD_TEMP, THOUSAND

  implicit none

!     INITIALIZE CONSTANTS -- ALL VALUES ARE IN MKS

  real(kind=WP), parameter :: gas_const = 0.0624_WP
  real(kind=WP), parameter :: visc_air = 2.0E-5_WP
  real(kind=WP), parameter :: pool_depth_1cm = 0.01_WP

  !> Initial conditions of puff released as gas or due to liquid flashing
  type(puff_template), intent(inout) :: flash_puff

  !> Initial conditions of puff released due to evaporation
  type(puff_template), intent(inout) :: evap_puff

  !> Environment property object
  type(environment_properties), intent(in) :: environment

  !> Pool object reference
  type(pool_condition), intent(inout) :: pool

  !> Pool heat transfer object reference
  type(poolhx_condition), intent(inout) :: poolhx

  !> Chemical effluent object reference
  type(effluent_properties), intent(inout) :: effluent

  !> Chemical release object reference
  type(release_properties), intent(inout) :: release

  !> Chemical storage tank object reference
  type(tank_properties), intent(inout) :: tank

  !> Scenario time, \f$\si{\second}\f$
  integer(kind=IP), intent(in) :: time

  real(kind=WP) :: hc
  real(kind=WP) :: hd
  real(kind=WP) :: re_num
  real(kind=WP) :: sc_num
  real(kind=WP) :: init_radius
  real(kind=WP) :: char_len
  real(kind=WP) :: albedo
  real(kind=WP) :: log_sat_press
  real(kind=WP) :: temp_chg
  real(kind=WP) :: est_p_temp
  real(kind=WP) :: xs_energy
  real(kind=WP) :: add_evap
  real(kind=WP) :: dif_temp
  real(kind=WP) :: adj_diff_coef
  real(kind=WP) :: pool_area_1
  real(kind=WP) :: pool_area_2

  continue

  if (release%from_burst()) then
    release%mass = tank%mass
    tank%mass = ZERO
    release%rate = ZERO
  else if (release%from_leak()) then
    release%mass = min(release%rate * release%puff_interval, tank%mass)
    tank%mass = tank%mass - release%mass
    if ((tank%mass <= ZERO) .AND. (release%rate > ZERO)) then
      write(stdout,'(A)') ' Last Puff, Tank Empty '
      tank%mass = ZERO
      release%rate = ZERO
    end if
  end if

  if ((release%mass <= ZERO) .AND. (pool%mass <= ZERO)) then
    flash_puff%mass = ZERO
    evap_puff%mass = ZERO
    return
  end if

  if (release%is_gas()) then
    flash_puff%mass = release%mass
    evap_puff%mass = ZERO
    return
  end if

  if (effluent%boiling_point <= environment%air_temperature) then

!     COMPUTE PORTION OF RELEASE THAT FLASHES, ADD REMAINDER TO
!     POOL AND COMPUTE POOL TEMPERATURE

    if (release%mass > ZERO) then
      flash_puff%mass = effluent%cp * release%mass                      &
        * (environment%air_temperature - effluent%boiling_point)        &
        / effluent%hv
      release%mass = release%mass - flash_puff%mass
    end if

  end if

!     EVAPORATION FORCED BY VAPOR PRESSURE
!     POOL TEMPERATURE CHANGES CONTROLLED BY ENERGY BALANCE

!     COMPUTE POOL DIMENSIONS AND TEMPERATURE ASSUMING
!     UNIFORM MIXING IN POOL

  if (effluent%boiling_point <= environment%air_temperature) then
    pool%temperature = pool%temperature * pool%mass                     &
      + release%mass * effluent%boiling_point
  else
    pool%temperature = pool%temperature * pool%mass                     &
      + release%mass * tank%temperature
  end if
  pool%mass = pool%mass + release%mass

  if (pool%mass > ZERO) then
    pool%temperature = pool%temperature / pool%mass
    pool%volume = pool%mass / effluent%source_density

!     ESTIMATE POOL AREA BY 2 METHODS AND COMPARE WITH MAXIMUM
!     AREA SUPPLIED BY USER -- CHOOSE SMALLEST VALUE

    init_radius = (pool%volume / PI)**THIRD
    pool_area_1 = PI * (init_radius**2                                  &
      + (TWO * (time + release%puff_interval))                          &
      * sqrt((g * pool%volume / PI)))
    pool_area_2 = pool%volume / pool_depth_1cm
    pool%area = min(pool%max_area, pool_area_1, pool_area_2)
    pool%radius = sqrt(pool%area / PI)
    pool%thickness = pool%volume / pool%area

!     COMPUTE SATURATION VAPOR PRESSURE AT POOL TEMPERATURE

    log_sat_press = effluent%pconst                                     &
      * (ONE - (effluent%boiling_point + STD_TEMP)                      &
      / (pool%temperature + STD_TEMP))
    effluent%sat_pressure =                                             &
      environment%air_pressure * exp(log_sat_press)

!     ADJUST DIFFUSION COEFFICIENT FOR TEMPERATURE

    dif_temp = HALF * (environment%air_temperature + pool%temperature)  &
      + STD_TEMP
    adj_diff_coef = effluent%diff_coef                                  &
      * (dif_temp / effluent%ref_temperature)**1.5_WP

!     COMPUTE REYNOLDS AND SCHMIDT NUMBER

    char_len = TWO * pool%radius
    re_num = (char_len * environment%ubar * environment%air_density)    &
      / visc_air
    sc_num = visc_air / (adj_diff_coef * environment%air_density)

!     COMPUTE hd BASED FOR TURBULENT FLOW

    hd = 0.037_WP * (adj_diff_coef / char_len)                          &
      * re_num**.8_WP * sc_num**THIRD

!     COMPUTE MASS EVAPORATED DURING PERIOD

    evap_puff%mass = hd * effluent%molec_wt * pool%area                 &
      * release%puff_interval * effluent%sat_pressure                   &
      / (gas_const * (pool%temperature + STD_TEMP))
    if (evap_puff%mass >= pool%mass) then
      evap_puff%mass = pool%mass
      pool%mass = ZERO
      write(stdout,'(A)') ' POOL DRIED UP '
    else
      pool%mass = pool%mass - evap_puff%mass

!     NET SHORT-WAVE RADIATION (watts/m**2)

      albedo = TENTH
      poolhx%net_swrad =                                                &
        (ONE - albedo) * environment%solar_radiation_flux

!     NET LONG-WAVE RADIATION (watts/m**2)

      poolhx%lw_in =                                                    &
        5.31E-13_WP * (environment%air_temperature + STD_TEMP)**6       &
        + 60.0_WP * environment%cloud_cover_frac
      if (environment%solar_radiation_flux > 100.0_WP) then
        poolhx%lw_in = poolhx%lw_in - 30.0_WP
      end if
      poolhx%lw_out = sb_const * (pool%temperature + STD_TEMP)**4
      poolhx%net_lwrad = poolhx%lw_in - poolhx%lw_out

!     FORCED CONVECTION OF AIR OVER THE SPILL

      hc = 6.69_WP * environment%ubar**0.6_WP
      poolhx%air_conv =                                                 &
        hc * (environment%air_temperature - pool%temperature)

!     CONDUCTION FROM GROUND

      poolhx%grnd_cond = (THOUSAND                                      &
        * (environment%earth_temperature - pool%temperature))           &
        / sqrt(real(time + release%puff_interval, WP))

!         NET ENERGY FLUX TO SPILL

      poolhx%net_flux = poolhx%net_swrad + poolhx%net_lwrad             &
                      + poolhx%air_conv + poolhx%grnd_cond

!         CORRECT POOL TEMPERATURE

      temp_chg = pool%area * poolhx%net_flux * release%puff_interval    &
        / (effluent%cp * pool%mass)   &
        - (evap_puff%mass / pool%mass) * (effluent%hv / effluent%cp)

      if (temp_chg <= ZERO) then

!           POOL COOLS FROM EVAPORATION

        pool%temperature = pool%temperature + temp_chg

      else

!           POOL WARMS FROM ENERGY INPUT

        est_p_temp = pool%temperature + temp_chg

!           CHECK TO SEE IF BOILING POINT EXCEEDED AND CORRECT
!           EVAPORATION IF IT IS

        if (est_p_temp <= effluent%boiling_point) then
          pool%temperature = est_p_temp
        else
          pool%temperature = effluent%boiling_point
          xs_energy = pool%mass * (est_p_temp - effluent%boiling_point) &
            * effluent%cp

          add_evap = xs_energy / effluent%hv

          if (pool%mass > add_evap) then
            evap_puff%mass = evap_puff%mass + add_evap
            pool%mass = pool%mass - add_evap
          else
            evap_puff%mass = evap_puff%mass + pool%mass
            pool%mass = ZERO
          end if

        end if

      end if
    end if
  end if

  return
end subroutine PUFFMASS

end module extran_legacy
