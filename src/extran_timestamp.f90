!> @file extran_timestamp.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains timestamp structures and generation
!! and manipulation routines

!> @brief Contains timestamp structures and generation
!! and manipulation routines
module extran_timestamp
  implicit none

  private

  public :: create_timestamp

  !> @brief timestamp data structure.
  !!
  !! Contents are static and mutable but ideally will not be
  !! changed after creation. Call `create_timestamp()` to generate
  !! a new properly formatted timestampe; it returns a value of type
  !! `timestamp` as a generator or factory function.
  type, public :: timestamp
    !> Space-padded datestamp in mm/dd/yyyy format
    character(len=10) :: rdate = '  /  /    '

    !> 24-hour timestamp in HH:MM:SS format
    character(len=8) :: rtime = '  :  :  '

    !> Zero-padded hour-minute timestamp
    character(len=4) :: hhmm = '    '

    !> Raw time elements returned from intrinsic `get_date_and_time()`
    !! function (year, month, day, minutes offset from UTC, hour,
    !! minute, second, millisecond)
    integer, dimension(8) :: datepart = 0

  end type timestamp

contains

!> Create timestamp structure for current time and date.
!!
!! See `timestamp` type for details on use.
type(timestamp) function create_timestamp() result(ts)
  implicit none

1 format(I2, '/', I2, '/', I4)
2 format(I2.2, ':', I2.2, ':', I2.2)
3 format(I2.2, I2.2)

  continue

  call date_and_time(values=ts%datepart)

  write(ts%rdate, fmt=1) ts%datepart(3), ts%datepart(2), ts%datepart(1)
  write(ts%rtime, fmt=2) ts%datepart(5), ts%datepart(6), ts%datepart(7)
  write(ts%hhmm, fmt=3) ts%datepart(5), ts%datepart(6)

  return
end function create_timestamp

end module extran_timestamp
