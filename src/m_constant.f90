!> @file m_constant.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains numerical and physical constants used by EXTRAN

!> @brief Contains numerical and physical constants used by EXTRAN
!!
!! @note All members are considered public
module m_constant
    use extran_param, only: WP
    implicit none

    ! *** Numerical constants ***

    !> Numerical constant - zero
    real(kind=WP), parameter :: ZERO = 0.0_WP

    !> Numerical constant - one
    real(kind=WP), parameter :: ONE = 1.0_WP

    !> Numerical constant - two
    real(kind=WP), parameter :: TWO = 2.0_WP

    !> Numerical constant - three
    real(kind=WP), parameter :: THREE = 3.0_WP

    !> Numerical constant - four
    real(kind=WP), parameter :: FOUR = 4.0_WP

    !> Numerical constant - five
    real(kind=WP), parameter :: FIVE = 5.0_WP

    !> Numerical constant - ten
    real(kind=WP), parameter :: TEN = 10.0_WP

    !> Numerical constant - hundred \f$\num{100}\f$
    real(kind=WP), parameter :: HUNDRED = 100.0_WP

    !> Numerical constant - thousand \f$\num{1000}\f$
    real(kind=WP), parameter :: THOUSAND = 1000.0_WP

    !> Numerical constant - tenth \f$\left(\frac{1}{10}\right)\f$
    real(kind=WP), parameter :: TENTH = ONE / TEN

    !> Numerical constant - fifth \f$\left(\frac{1}{5}\right)\f$
    real(kind=WP), parameter :: FIFTH = ONE / FIVE

    !> Numerical constant - quarter \f$\left(\frac{1}{4}\right)\f$
    real(kind=WP), parameter :: QUARTER = ONE / FOUR

    !> Numerical constant - third \f$\left(\frac{1}{3}\right)\f$
    real(kind=WP), parameter :: THIRD = ONE / THREE

    !> Numerical constant - half \f$\left(\frac{1}{2}\right)\f$
    real(kind=WP), parameter :: HALF = ONE / TWO

    !> Numerical constant - \f$\num{1.0E9}\f$
    real(kind=WP), parameter :: BIG1P9 = 1.0E9_WP

    !> Numerical constant - \f$\num{1.0E-6}\f$
    real(kind=WP), parameter :: TINY1M6 = 1.0E-6_WP

    !> Numerical constant - \f$\num{1.0E-24}\f$
    real(kind=WP), parameter :: TINY1M24 = 1.0E-24_WP

    ! *** Unit conversion factors ***

    !> Zero Celsius, \f$\SI{273.15}{\kelvin}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: T0K = 273.15_WP

    !> Seconds per minute, \f$\SI{60}{\second\per\minute}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: SEC_MIN = 60.0_WP

    !> Minutes per hour, \f$\SI{60}{\minute\per\hour}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: MIN_HR = 60.0_WP

    !> Seconds per hour, \f$\SI{3600}{\second\per\hour}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: SEC_HR = SEC_MIN * MIN_HR

    ! *** Physical constants - SI ***

    !> Molecular (formula) weight of air at sea level, \f$\SI{29.87}{\gram\per\mol}\f$.
    !! Original unreferenced EXTRAN value.
    real(kind=WP), parameter :: MW_AIR = 29.87_WP

!     !> Molecular (formula) weight of air at sea level, \f$\SI{28.964425278794}{\gram\per\mol}\f$
!     !! or \f$\SI{28.964425278794}{\lbm\per\lbmol}\f$
!     !! from @cite CRC_Handbook_66 Source: "U.S. Standard Atmosphere, 1976", NOAA/NASA/USAF
!     real(kind=WP), parameter :: MW_AIR = 28.964425278794_WP
! !    real(kind=WP), parameter :: MW_AIR = 28.9647_WP

    !> Standard gravitational acceleration, \f$\SI{9.8}{\meter\per\square\second}\f$
    !! Original unreferenced EXTRAN value.
    real(kind=WP), parameter :: GRAV = 9.8_WP

    ! !> Standard gravitational acceleration, \f$\SI{9.80665}{\meter\per\square\second}\f$
    ! !! CODATA 2018 data taken from https://physics.nist.gov/cuu/Constants/Table/allascii.txt
    ! real(kind=WP), parameter :: GRAV = 9.80665_WP

    !> Molar (universal) gas constant, \f$R = k N_{A} = \SI{8.3144}{\joule\per\mol\per\kelvin}\f$.
    !! Original unreferenced EXTRAN value.
    real(kind=WP), parameter :: RGAS = 8.3144_WP
!    !> Molar (universal) gas constant, \f$R = k N_{A} = \SI{8.31446261815324}{\joule\per\mol\per\kelvin}\f$
!    !! Approximation of CODATA 2018 data taken from
!    !! https://physics.nist.gov/cuu/Constants/Table/allascii.txt
!    real(kind=WP), parameter :: RGAS = 8.31446261815324_WP

    ! !> Stefan-Boltzmann constant, \f$\SI{5.670374419E-8}{\watt\per\square\meter\per\kelvin^{4}}\f$
    ! !! Approximation of CODATA 2018 data taken from
    ! !! https://physics.nist.gov/cuu/Constants/Table/allascii.txt
    ! real(kind=WP), parameter :: SIGMA_SB = 5.670374419E-8_WP

    !> Stefan-Boltzmann constant, \f$\SI{5.67E-8}{\watt\per\square\meter\per\kelvin^{4}}\f$
    !! Original unreferenced EXTRAN value.
    real(kind=WP), parameter :: SIGMA_SB = 5.67E-8_WP

    ! *** Other constants ***

    !> Pi, \f$\pi\f$.
    !! Original unreferenced EXTRAN value.
    real(kind=WP), parameter :: PI = 3.14159_WP
!    !> Pi, \f$\pi\f$
!    real(kind=WP), parameter :: PI = 3.141592653589793_WP

    !> Standard temperature, \f$\SI{273.16}{\kelvin}\f$.
    !! Original unreferenced EXTRAN value.
    real(kind=WP), parameter :: STD_TEMP = 273.16_WP

    !> Standard pressure, \f$\SI{760}{\milli\meter}\f$ Hg.
    !! Original unreferenced EXTRAN value.
    real(kind=WP), parameter :: STD_PRESSURE_MMHG = 760.0_WP

    !> Air density at standard conditions,
    !! \f$\si{\gram\per\cubic\meter}\f$.
    !! Original unreferenced EXTRAN value.
    real (kind=WP), parameter :: STD_AIR_DENSITY = 1.29_WP

    ! ASCII printer control codes

    !> Line feed
    character(len=1), parameter :: LINEFEED = char(10)

    !> Form feed
    character(len=1), parameter :: FORMFEED = char(12)

    !> Escape
    character(len=1), parameter :: ESC = char(27)

    !> Condensed print (after ESC)
    character(len=1), parameter :: PRCOND = char(15)

    !> Normal print (after ESC)
    character(len=1), parameter :: PRNORM = char(18)

! contains
end module m_constant
