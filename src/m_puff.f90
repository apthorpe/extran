!> @file m_puff.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Shared effluent puff data module

!> @brief Shared effluent puff data module
!!
!! Contains initial puff condition template, record of puff parameters,
!! and chronology of released puffs
module m_puff
!***********************************************************************
!     m_puff.f90                                      EXTRAN Version 1.2
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description:  Contains puff parameters
!
!     Included in modules: CHIT, EXTRAN, PUFFINIT, RELPUFF
!
!***********************************************************************
use extran_param, only: WP, IP, NMAXPUFFS
use m_constant, only: ZERO
implicit none

private

public :: nsig

!> Initial puff properties
type, public :: puff_template
  !> TBD
  real(kind=WP) :: mass = ZERO
  !> Initial lateral dispersion coefficient, \f$\si{\meter}\f$
  real(kind=WP) :: isigy = ZERO
  !> Initial vertical dispersion coefficient, \f$\si{\meter}\f$
  real(kind=WP) :: isigz = ZERO
  !> Initial lateral dispersion timescale, \f$\si{\second}\f$
  real(kind=WP) :: tvy = ZERO
  !> Initial vertical dispersion timescale, \f$\si{\second}\f$
  real(kind=WP) :: tvz = ZERO
contains
  !> Set all properties to zero
  procedure :: reset => puff_template_reset
end type puff_template

!> Characteristics of a single released puff
type, public :: puff_record
  !> Puff mass, \f$\si{\kilo\gram}\f$
  real(kind=WP) :: mass = ZERO
  !> Puff age, \f$\si{\second}\f$
  real(kind=WP) :: age = ZERO
  !> Puff TBD lateral timescale, \f$\si{\second}\f$
  real(kind=WP) :: tvy = ZERO
  !> Puff TBD vertical timescale, \f$\si{\second}\f$
  real(kind=WP) :: tvz = ZERO
  !> Puff TBD lateral dispersion coefficient, \f$\si{\meter}\f$
  real(kind=WP) :: nsigy = ZERO
  !> Puff TBD vertical dispersion coefficient, \f$\si{\meter}\f$
  real(kind=WP) :: nsigz = ZERO
  !> Puff TBD lateral dispersion coefficient, \f$\si{\meter}\f$
  real(kind=WP) :: tsigy = ZERO
  !> Puff TBD vertical dispersion coefficient, \f$\si{\meter}\f$
  real(kind=WP) :: tsigz = ZERO
contains
  !> Set all properties to zero
  procedure :: reset => puff_record_reset
  !> Move and diffuse the puff
  procedure :: move_and_diffuse => puff_record_move_and_diffuse
end type puff_record

!> Puff recordset class
type, public :: puff_track
  !> Instantaneous number of released puffs
  integer(kind=IP), public :: numpuffs = 0_IP

  !> List of released puffs
  type(puff_record), dimension(NMAXPUFFS), public :: puff
contains
  !> Set all properties to zero
  procedure :: reset => puff_track_reset
  !> Add (environmentally) released puff to puff track
  procedure :: release_puff => puff_track_release_puff
  !> Move and diffuse all puffs
  procedure :: move_and_diffuse_all => puff_track_move_and_diffuse_all
end type puff_track

!       COMMON /puff/ puffm, puffage, pufftvy, pufftvz, puffnsigy,
!      .              puffnsigz, pufftsigy, pufftsigz, pmass1, pmass2,
!      .              isigy1, isigz1, isigy2, isigz2, tvy1, tvz1, tvy2,
!      .              tvz2, numpuffs

contains

!> @brief Diffusion curves as used in XOQDOQ, PAVAN, MESOI, and MESORAD
!!
!! Computes new diffusion coefficients given the last values,
!! atmospheric stability, mixing layer thickness and distance moved.
subroutine NSIG(stab, DSMTRI, SIGMAY, SIGMAZ)
!***********************************************************************
!     NSIG                                            EXTRAN Version 1.2
!     Diffusion Curves As Used In XOQDOQ, PAVAN, MESOI, and MESORAD
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 6/83
!     Updated: 10/90
!
!     Description: Computes new diffusion coefficients given the
!                  last values, atmospheric stability, mixing layer
!                  thickness and distance moved.
!
!     Relationship to other modules:
!
!         Makes calls to:  NONE
!
!         Called from:     EXTRAN, MODELPAR
!
!***********************************************************************
  use extran_param, only: WP, IP, NSTAB, NDISPRANGE
  use m_environ, only: mix_depth
  use m_constant, only: ZERO, ONE, HUNDRED, THOUSAND

  implicit none

  ! Tadmor & Gur (1968) exponent on power-law correlation for sigma-y
  real(kind=WP), parameter :: BEXP = 0.9031_WP

  !> Atmospheric stability class, 1-7
  integer(kind=IP), intent(in) :: stab

  !> Longitudinal (horizontal) distance from source to receptor,
  !! \f$\si{\meter}\f$
  real(kind=WP), intent(in) :: DSMTRI

  !> Diffusion coefficient in the lateral direction, \f$\si{\meter}\f$
  real(kind=WP), intent(out) :: SIGMAY

  !> Diffusion coefficient in the vertical direction, \f$\si{\meter}\f$
  real(kind=WP), intent(inout) :: SIGMAZ

  real(kind=WP), dimension(NSTAB), parameter :: AY = (/                 &
    0.3658_WP, 0.2751_WP, 0.2089_WP, 0.1471_WP, 0.1046_WP,              &
    0.0722_WP, 0.0481_WP /)
  real(kind=WP), dimension(NSTAB, NDISPRANGE), parameter :: AZ =        &
    reshape(                                                            &
    (/  0.192_WP,    0.156_WP,  0.116_WP, 0.079_WP, 0.063_WP,           &
        0.053_WP,    0.032_WP,                                          &
        0.00066_WP,  0.0382_WP, 0.113_WP, 0.222_WP, 0.211_WP,           &
        0.086_WP,    0.052_WP,                                          &
        0.00024_WP,  0.055_WP,  0.113_WP, 1.26_WP,  6.73_WP,            &
        18.05_WP,    10.83_WP /), (/ NSTAB, NDISPRANGE /) )
  real(kind=WP), dimension(NSTAB, NDISPRANGE), parameter :: BZ =        &
    reshape(                                                            &
    (/  0.936_WP,  0.922_WP, 0.905_WP, 0.881_WP, 0.871_WP,              &
        0.814_WP,  0.814_WP,                                            &
        1.941_WP,  1.149_WP, 0.911_WP, 0.725_WP, 0.678_WP,              &
        0.74_WP,   0.74_WP,                                             &
        2.094_WP,  1.098_WP, 0.911_WP, 0.516_WP, 0.305_WP,              &
        0.18_WP,   0.18_WP /), (/ NSTAB, NDISPRANGE /) )
  real(kind=WP), dimension(NSTAB, NDISPRANGE), parameter :: CZ =        &
    reshape(                                                            &
    (/  ZERO,      ZERO,     ZERO,     ZERO,     ZERO,                  &
        ZERO,      ZERO,                                                &
        9.27_WP,   3.3_WP,   0.0_WP,  -1.7_WP,  -1.3_WP,                &
        -0.35_WP,  -0.21_WP,                                             &
        -9.6_WP,    2.0_WP,   0.0_WP, -13._WP,  -34.0_WP,                &
        -48.6_WP,  -29.2_WP /), (/ NSTAB, NDISPRANGE /) )

  real(kind=WP) :: XVY
  real(kind=WP) :: XEY
  real(kind=WP) :: SZLIM
  real(kind=WP) :: XVZ
  real(kind=WP) :: XEZ

  continue

  XVY = (SIGMAY / AY(STAB))**(ONE / BEXP)
  XEY = XVY + DSMTRI
  SIGMAY = AY(STAB) * XEY**BEXP

! ** SIGMA Z COMPUTATIONS
! ** CHECK INITIAL SIGMA Z SIZE AGAINST MAXIMUM

  SZLIM = 0.8_WP * mix_depth
  if (SIGMAZ >= SZLIM) return

! ** COMPUTE VIRTUAL DISTANCE

  XVZ = (SIGMAZ / AZ(STAB,1))**(ONE / BZ(STAB,1))
  if (SIGMAZ > ONE) then
    if ((XVZ + DSMTRI) > HUNDRED .AND. SIGMAZ > CZ(STAB,2)) then
      XVZ= ((SIGMAZ - CZ(STAB,2)) / AZ(STAB,2))                         &
        **(ONE / BZ(STAB,2))
      if ((XVZ + DSMTRI) >= THOUSAND                                  &
          .AND. SIGMAZ > CZ(STAB,3)) then
        XVZ = ((SIGMAZ - CZ(STAB,3)) / AZ(STAB,3))                      &
          **(ONE / BZ(STAB,3))
      end if
    end if
  end if

  XEZ = XVZ + DSMTRI

  if (XEZ <= HUNDRED) then
    SIGMAZ = AZ(STAB,1) * XEZ ** BZ(STAB,1)
  else if (XEZ <= THOUSAND) then
    SIGMAZ = AZ(STAB,2) * XEZ ** BZ(STAB,2) + CZ(STAB,2)
  else if (XEZ > THOUSAND) then
    SIGMAZ = AZ(STAB,3) * XEZ ** BZ(STAB,3) + CZ(STAB,3)
  end if

!  if (SIGMAZ > SZLIM) SIGMAZ = SZLIM
  SIGMAZ = min(SIGMAZ, SZLIM)

  return
end subroutine NSIG

!> Set all properties to zero
subroutine puff_template_reset(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(puff_template), intent(inout) :: this

  this%mass  = ZERO
  this%isigy = ZERO
  this%isigz = ZERO
  this%tvy   = ZERO
  this%tvz   = ZERO

  continue
  return
end subroutine puff_template_reset

!> Set all properties to zero
subroutine puff_record_reset(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(puff_record), intent(inout) :: this

  this%mass  = ZERO
  this%age   = ZERO
  this%tvy   = ZERO
  this%tvz   = ZERO
  this%nsigy = ZERO
  this%nsigz = ZERO
  this%tsigy = ZERO
  this%tsigz = ZERO

  continue
  return
end subroutine puff_record_reset

!> Move and diffuse the puff
subroutine puff_record_move_and_diffuse(this, environment, wake,        &
  delta_dist, rpuff_duration)
  use extran_param, only: WP, IP
  use extran_wake, only: wake_condition
  use m_environ, only: environment_properties
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(puff_record), intent(inout) :: this

  !> Environment properties object
  type(environment_properties), intent(in) :: environment

  !> Wake object reference
  type(wake_condition), intent(in) :: wake

  !> Transit distance, \f$\si{\second}\f$
  real(kind=WP), intent(in) :: delta_dist

  !> Transit time, \f$\si{\second}\f$
  real(kind=WP), intent(in) :: rpuff_duration

  real(kind=WP) :: tty
  real(kind=WP) :: ttz
  real(kind=WP) :: wsigy
  real(kind=WP) :: wsigz

  continue

  this%age = this%age + rpuff_duration
  call nsig(environment%stability_class, delta_dist, this%nsigy,        &
    this%nsigz)
  if ((this%tvy >= ZERO) .AND. (this%tvz >= ZERO)) then
    tty = this%age + this%tvy
    ttz = this%age + this%tvz
    wsigy = wake%get_sigma_y(tty)
    wsigz = wake%get_sigma_z(ttz)
  else
    wsigy = ZERO
    wsigz = ZERO
  end if
  this%tsigy = sqrt(this%nsigy**2 + wsigy**2)
  this%tsigz = sqrt(this%nsigz**2 + wsigz**2)

  continue
  return
end subroutine puff_record_move_and_diffuse

!> Set all properties to zero
subroutine puff_track_reset(this)
  use extran_param, only: IP, NMAXPUFFS
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(puff_track), intent(inout) :: this

  integer :: i

  continue

  this%numpuffs = 0_IP

  do i = 1, NMAXPUFFS
    call this%puff(i)%reset()
  end do

  return
end subroutine puff_track_reset

!> If new puff mass is positive, increment puff counter and populate
!! puff record with initial conditions
subroutine puff_track_release_puff(this, new_puff)
  use extran_param, only: IP, NMAXPUFFS
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(puff_track), intent(inout) :: this

  !> Puff initial conditions
  type(puff_template), intent(in) :: new_puff

  continue

  if (new_puff%mass > ZERO) then
    this%numpuffs = this%numpuffs + 1_IP
    this%puff(this%numpuffs)%mass = new_puff%mass
    this%puff(this%numpuffs)%age = ZERO
    this%puff(this%numpuffs)%tvy = new_puff%tvy
    this%puff(this%numpuffs)%tvz = new_puff%tvz
    this%puff(this%numpuffs)%nsigy = new_puff%isigy
    this%puff(this%numpuffs)%nsigz = new_puff%isigz
    this%puff(this%numpuffs)%tsigy = ZERO
    this%puff(this%numpuffs)%tsigz = ZERO
  end if

  return
end subroutine puff_track_release_puff

!> MOVE AND DIFFUSE ALL PUFFS USING NRC SIGMA CURVES AND
!! BUILDING WAKE IF TV >= 0
subroutine puff_track_move_and_diffuse_all(this, environment, wake,     &
  puff_duration)
  use extran_param, only: WP, IP
  use extran_wake, only: wake_condition
  use m_environ, only: environment_properties
  implicit none

  !> Object reference
  class(puff_track), intent(inout) :: this

  !> Environment property object reference
  type(environment_properties), intent(in) :: environment

  !> Wake object reference
  type(wake_condition), intent(in) :: wake

  !> Puff duration, \f$\si{\second}\f$
  integer(kind=IP), intent(in) :: puff_duration

  real(kind=WP) :: rpuff_duration
  real(kind=WP) :: delta_dist
  integer(kind=IP) :: puffid

  continue

  rpuff_duration = real(puff_duration, WP)
  delta_dist = rpuff_duration * environment%ubar

  do puffid = 1, this%numpuffs
    call this%puff(puffid)%move_and_diffuse(environment, wake,          &
      delta_dist, rpuff_duration)
  end do

  return
end subroutine puff_track_move_and_diffuse_all

end module m_puff