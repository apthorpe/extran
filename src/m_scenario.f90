!> @file m_scenario.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Shared release scenario data module

!> @brief Shared release scenario data module
!!
!! Contains scenario information and constants used throughout the
!! WAKE model.
module m_scenario
!***********************************************************************
!     m_scenario.f90                                  EXTRAN Version 1.2
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description:  Contains scenario information and constants used
!                   throughout the WAKE model.
!
!     Included in modules: CHIT, EXTRAN, INPUT, INPUTS,
!                          MODELPAR, PUFFINIT, PUFFMASS, RINPUT,
!                          extran_wake
!
!***********************************************************************
use extran_param, only: WP, IP
use m_constant, only: ZERO
implicit none

public

!> Control room air intake properties
type, public :: intake_properties
  !> Horizontal distance from base of source to base of intake,
  !! \f$\si{\meter}\f$
  real(kind=WP) :: distance = ZERO

  !> Intake height, \f$\si{\meter}\f$
  real(kind=WP) :: height = ZERO
contains
  !> Set properties to default
  procedure :: reset => intake_properties_reset
end type intake_properties

!> Building cross-sectional area if release occurs in building wake,
!! \f$\si{\square\meter}\f$
real(kind=WP) :: area

!> Chemical storage tank properties
type, public :: tank_properties
  !> Initial amount of material in tank, either in Ci or
  !! \f$\si{\kilo\gram}\f$
  real(kind=WP) :: initial_mass

  !> Instantaneous amount of material in tank, either in Ci or
  !! \f$\si{\kilo\gram}\f$
  real(kind=WP) :: mass

  !> Temperature of tank contents, \f$\si{\celsius}\f$
  real(kind=WP) :: temperature
contains
  !> Zero out properties
  procedure :: clear => tank_properties_clear
  !> Set properties to default
  procedure :: reset => tank_properties_reset
end type tank_properties

!> Chemical release properties
type, public :: release_properties
  !> Initial release rate, either in Ci/s or
  !! \f$\si{\kilo\gram\per\second}\f$
  real(kind=WP) :: initial_rate = ZERO

  !> Instantaneous release rate, either in Ci/s or
  !! \f$\si{\kilo\gram\per\second}\f$
  real(kind=WP) :: rate = ZERO

  !> Cumulative released material, in Ci or \f$\si{\kilo\gram}\f$
  real(kind=WP) :: mass = ZERO

  !> Release height, \f$\si{\meter}\f$
  real(kind=WP) :: height = ZERO

  !> Volumetric release rate for release via a short stack or vent,
  !! \f$\si{\cubic\meter\per\second}\f$
  real(kind=WP) :: vent_flow = ZERO

  !> Release scenario ID (was `rel_type`):
  !!   1. Liquid Tank Burst
  !!   2. Liquid Tank Leak
  !!   3. Gas Tank Burst
  !!   4. Gas Tank Leak
  integer(kind=IP) :: scenario = 0_IP

  !> Puff release interval, \f$\si{second}\f$. Was `pri`
  integer(kind=IP) :: puff_interval = 0_IP

  !> Puff release duration, \f$\si{second}\f$. Was `delta_time`
  integer(kind=IP) :: puff_duration = 0_IP

contains
  !> Zero out properties
  procedure :: clear => release_properties_clear
  !> Set properties to default
  procedure :: reset => release_properties_reset
  !> True if release is from gas tank, false if from liquid tank
  procedure :: is_gas => release_properties_is_gas
  !> True if release is from liquid tank, false if from gas tank
  procedure :: is_liquid => release_properties_is_liquid
  !> True if release is from tank leak, false if from tank burst
  procedure :: from_leak => release_properties_from_leak
  !> True if release is from tank burst, false if from tank leak
  procedure :: from_burst => release_properties_from_burst
end type release_properties

!       COMMON /scenario/ intake_dist, intake_height, area, rel_height,
!      .                  tank_mass, tank_temp, rel_rate, max_pool_rad,
!      .                  max_pool_area, init_tank_mass, init_rel_rate,
!      .                  vent_flow, pri, delta_time

contains

!> Set properties to default
subroutine intake_properties_reset(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(intake_properties), intent(inout) :: this

  continue

  this%distance = ZERO
  this%height = ZERO

  return
end subroutine intake_properties_reset

!> Zero out properties
subroutine tank_properties_clear(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(tank_properties), intent(inout) :: this

  continue

  this%initial_mass = ZERO
  this%mass = ZERO
  this%temperature = ZERO

  return
end subroutine tank_properties_clear

!> Set properties to default
subroutine tank_properties_reset(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(tank_properties), intent(inout) :: this

  continue

  this%mass = this%initial_mass

  return
end subroutine tank_properties_reset

!> Zero out properties
subroutine release_properties_clear(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(release_properties), intent(inout) :: this

  continue

  this%initial_rate = ZERO
  this%rate = ZERO
  this%mass = ZERO
  this%height = ZERO
  this%vent_flow = ZERO
  this%scenario = 0_IP
  this%puff_interval = 0_IP
  this%puff_duration = 0_IP

  return
end subroutine release_properties_clear

!> Set properties to default
subroutine release_properties_reset(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(release_properties), intent(inout) :: this

  continue

  this%rate = this%initial_rate
  this%mass = ZERO

  return
end subroutine release_properties_reset

!> True if release is from gas tank, false if from liquid tank
logical function release_properties_is_gas(this) result(is_gas)
  use extran_param, only: IP
  implicit none
  !> Object reference
  class(release_properties), intent(in) :: this
  continue
  select case(this%scenario)
  case(1_IP, 2_IP)
    is_gas = .false.
  case(3_IP, 4_IP)
    is_gas = .true.
  case default
    stop 'Invalid release type; should range from 1..4'
  end select
  return
end function release_properties_is_gas

!> True if release is from liquid tank, false if from gas tank
logical function release_properties_is_liquid(this) result(is_liquid)
  use extran_param, only: IP
  implicit none
  !> Object reference
  class(release_properties), intent(in) :: this
  continue
  select case(this%scenario)
  case(1_IP, 2_IP)
    is_liquid = .true.
  case(3_IP, 4_IP)
    is_liquid = .false.
  case default
    stop 'Invalid release type; should range from 1..4'
  end select
  return
end function release_properties_is_liquid

!> True if release is from tank leak, false if from tank burst
logical function release_properties_from_leak(this) result(from_leak)
  use extran_param, only: IP
  implicit none
  !> Object reference
  class(release_properties), intent(in) :: this
  continue
  select case(this%scenario)
  case(1_IP, 3_IP)
    from_leak = .false.
  case(2_IP, 4_IP)
    from_leak = .true.
  case default
    stop 'Invalid release type; should range from 1..4'
  end select
  return
end function release_properties_from_leak

!> True if release is from tank burst, false if from tank leak
logical function release_properties_from_burst(this) result(from_burst)
  use extran_param, only: IP
  implicit none
  !> Object reference
  class(release_properties), intent(in) :: this
  continue
  select case(this%scenario)
  case(1_IP, 3_IP)
    from_burst = .true.
  case(2_IP, 4_IP)
    from_burst = .false.
  case default
    stop 'Invalid release type; should range from 1..4'
  end select
  return
end function release_properties_from_burst

end module m_scenario