!> @file m_options.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Shared option and output file name data module

!> @brief Shared option and output file name data module
!!
!! Contains file names and flags that control model output.
module m_options
!***********************************************************************
!     m_options.f90                                   EXTRAN Version 1.2
!
!     Created: 7/90
!     Updated: 10/90
!
!     Description:  Contains flags that control model output.
!
!     Included in modules: CEPLOT, EXTRAN, INPUT, INPUTO, INPUTS,
!                          MODELPAR, RINPUT
!
!***********************************************************************
use extran_param, only: IP
implicit none

public

!> Serial number of case (1 - 999)
integer(kind=IP) :: run_count

!> Direct output to file or printer
type, public :: concentration_config
  !> Display concentration in ppm. Was `ppmflg`
  logical :: ppm = .false.

  !> Display concentration in mCi/m**3. Was `ciflg`
  logical :: mci_per_m3 = .false.

  contains

  !> Reset properties
  procedure :: reset => concentration_config_reset

end type concentration_config

!> Direct output to file or printer
type, public :: output_config
  !> Send summary output to a printer. Was `prtflg1`
  logical :: print_summary = .false.

  !> Send concentration chronology to a printer. Was `histflg1`
  logical :: print_chronology = .false.

  !> Send plot of transient results to a printer. Was `pltflg`
  logical :: print_plot = .false.

  !> Write summary output to file. Was `prtflg2`
  logical :: save_summary = .false.

  !> Write concentration chronology to a file. Was `histflg2`
  logical :: save_chronology = .false.

  !> Write pool status and mass and energy balances to a file. Was `statflg`
  logical :: save_balance = .false.

contains

  !> Reset properties
  procedure :: reset => output_config_reset

end type output_config

!       COMMON /options/ ppmflg, prtflg1, prtflg2, pltflg, histflg1,
!      .                 histflg2, statflg, ciflg, run_count

!       COMMON /filenames/ PRTFILE, CRONFILE, MBFILE

contains

!> Reset properties
subroutine concentration_config_reset(this)
  implicit none

  !> Object reference
  class(concentration_config), intent(inout) :: this

  continue

  !> Display concentration in ppm. Was `ppmflg`
  this%ppm = .false.

  !> Display concentration in mCi/m**3. Was `ciflg`
  this%mci_per_m3 = .false.

  return
end subroutine concentration_config_reset

!> Reset properties
subroutine output_config_reset(this)
  implicit none

  !> Object reference
  class(output_config), intent(inout) :: this

  continue

  this%print_summary = .false.
  this%print_chronology = .false.
  this%print_plot = .false.
  this%save_summary = .false.
  this%save_chronology = .false.
  this%save_balance = .false.

  return
end subroutine output_config_reset

end module m_options