!> @file extran_param.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains numerical precision, array sizes, and similar
!! modeling constants used by EXTRAN

!> @brief Contains numerical precision, array sizes, and similar
!! modeling constants used by EXTRAN
!!
!! @note All members are considered public
module extran_param
! Set WP (REAL 'working' precision) and IP (integer precision)
! from iso_fortran_env constants
use, intrinsic :: iso_fortran_env, only: WP => REAL32,                &
    IP => INT16
implicit none

!> Time divisions
integer, parameter :: NTIME = 181

!> Maximum number of puffs to model
integer, parameter :: NMAXPUFFS = 500

!> Maximum number of chemicals in property database
integer, parameter :: NCHEMLIM = 30

!> Number of physical properties tracked for each chemical
!! in property database
integer, parameter :: NCHEMPROPS = 6

!> Number of atmospheric stability classes
integer, parameter :: NSTAB = 7

!> Number of atmospheric dispersion range intervals
!! (\f$<\SI{100}{\meter}\f$, \f$\SIrange{100}{1000}{\meter}\f$,
!! \f$>\SI{1000}{\meter}\f$)
integer, parameter :: NDISPRANGE = 3

! contains
end module extran_param