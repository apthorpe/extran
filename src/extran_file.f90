!> @file extran_file.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains file names, name templates, and shared unit numbers
!! for files used by EXTRAN

!> @brief Contains file names, name templates, and shared unit numbers
!! for files used by EXTRAN
module extran_file
  implicit none

  private

  !> Chemical datafile name
  character(len=*), parameter, public :: chemical_datafile = 'CHEMICAL.DAT'

  !> Output file name and unit number structure
  type, public :: outfile_config
    !> File name of primary output
    character(len=12) :: summary_file = '            '

    !> Primary output unit number
    integer :: usum = 10

    !> File name of concentration chronology
    character(len=12) :: chronology_file = '            '

    !> Primary output unit number
    integer :: ucron = 11

    !> File name of mass and energy balance
    character(len=12) :: balance_file = '            '

    !> Primary output unit number
    integer :: ubal = 12

    !> File name of output directed to printer
    character(len=12) :: printer_file = '            '

    !> Primary output unit number
    integer :: uprint = 3
  contains
    !> Set properties to default values (blank and zero)
    procedure :: reset => outfile_config_reset

    !> Generate output file names from timestamp and run count
    procedure :: set_filenames => outfile_config_set_filenames

    !> Dump internal contents to stdout
    procedure :: dump => outfile_config_dump
  end type outfile_config

contains

!> Set properties to default values (blank and zero)
subroutine outfile_config_reset(this)
  implicit none

  !> Object reference
  class(outfile_config), intent(inout) :: this

  continue

  this%summary_file = '            '
  this%usum = 10
  this%chronology_file = '            '
  this%ucron = 11
  this%balance_file = '            '
  this%ubal = 12
  this%printer_file = '            '
  this%uprint = 3

  return
end subroutine outfile_config_reset

!> Generate output file names from timestamp and run count
subroutine outfile_config_set_filenames(this, hhmm, run_count)
  use extran_param, only: IP
  implicit none

  !> Object reference
  class(outfile_config), intent(inout) :: this

  !> Hour-minute timestamp in zero-padded 24-hour format
  character(len=4), intent(in) :: hhmm

  !> Run serial number
  integer(kind=IP), intent(in) :: run_count

1 format(A4, A4, ".", I3.3)

  continue

  this%uprint = 3
  this%usum = 10
  this%ucron = 11
  this%ubal = 12

  write(this%summary_file, 1)    'EXPR', hhmm, run_count
  write(this%chronology_file, 1) 'EXCR', hhmm, run_count
  write(this%balance_file, 1)    'EXMB', hhmm, run_count
  write(this%printer_file, 1)    'EXLP', hhmm, run_count

  return
end subroutine outfile_config_set_filenames

!> Dump internal contents to stdout
subroutine outfile_config_dump(this)
  use, intrinsic :: iso_fortran_env, only: stdout => output_unit
  use extran_param, only: IP
  implicit none

  !> Object reference
  class(outfile_config), intent(inout) :: this

1 format(A, ' file (', I2, '): ', A12)

  continue

  write(unit=stdout, fmt=1) 'Summary', this%usum, this%summary_file
  write(unit=stdout, fmt=1) 'Chronology', this%ucron,                   &
    this%chronology_file
  write(unit=stdout, fmt=1) 'Balance', this%ubal, this%balance_file
  write(unit=stdout, fmt=1) 'Printer', this%uprint, this%printer_file

  return
end subroutine outfile_config_dump

end module extran_file
