!> @file m_pool.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Shared pool information data module

!> @brief Shared pool information data module
!!
!! Contains information on the pool of material
module m_pool
!***********************************************************************
!     m_pool.f90                                      EXTRAN Version 1.2
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description:  Contains information on the pool of material
!
!     Included in modules: EXTRAN, PUFFINIT, PUFFMASS
!
!***********************************************************************
use extran_param, only: WP
use m_constant, only: ZERO
implicit none

public

!> Pool properties
type, public :: pool_condition
  !> Instaneous mass of material in pool, \f$\si{\kilo\gram}\f$
  real(kind=WP) :: mass = ZERO

  !> Pool temperature, \f$\si{celsius}\f$
  real(kind=WP) :: temperature = ZERO

  !> Pool volume, \f$\si{\cubic\meter}\f$
  real(kind=WP) :: volume = ZERO

  !> Maximum allowable pool radius, \f$\si{\meter}\f$
  real(kind=WP) :: max_radius = ZERO

  !> Maximum allowable pool area, \f$\si{\square\meter}\f$
  real(kind=WP) :: max_area = ZERO

  !> Pool radius, \f$\si{\meter}\f$
  real(kind=WP) :: radius = ZERO

  !> Pool area, \f$\si{\square\meter}\f$
  real(kind=WP) :: area = ZERO

  !> Pool depth, \f$\si{\meter}\f$
  real(kind=WP) :: thickness = ZERO
contains
  !> Set pool properties to defaults (zero)
  procedure :: clear => pool_condition_clear
  !> Set pool properties to scenario initial conditions
  procedure :: reset => pool_condition_reset
end type pool_condition

!> Pool heat transfer properties
type, public :: poolhx_condition
  !> Net absorbed? short wave radiation, W?
  real(kind=WP) :: net_swrad = ZERO

  !> Incident long wave radiation, W?
  real(kind=WP) :: lw_in = ZERO

  !> Emitted long wave radiation, W?
  real(kind=WP) :: lw_out = ZERO

  !> Net absorbed? long wave radiation, W?
  real(kind=WP) :: net_lwrad = ZERO

  !> Heat lost by convection to the air, W?
  real(kind=WP) :: air_conv = ZERO

  !> Heat lost by conduction to the ground, W?
  real(kind=WP) :: grnd_cond = ZERO

  !> Net heat flux to? pool, W/m**2?
  real(kind=WP) :: net_flux = ZERO
contains
  !> Set pool heat transfer properties to defaults (zero)
  procedure :: reset => poolhx_condition_reset
end type poolhx_condition

!       COMMON /pool/ rel_mass, pool_mass, pool_temp, pool_vol,
!      .       pool_radius, pool_area, pool_thick, net_swrad, lw_in,
!      .       lw_out, net_lwrad, air_conv, grnd_cond, net_flux
contains

!> Set pool properties to defaults (zero)
subroutine pool_condition_clear(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(pool_condition), intent(inout) :: this

  continue

  this%mass = ZERO
  this%temperature = ZERO
  this%volume = ZERO
  this%max_radius = ZERO
  this%max_area = ZERO
  this%radius = ZERO
  this%area = ZERO
  this%thickness = ZERO

  return
end subroutine pool_condition_clear

!> Set pool properties to scenario initial conditions
subroutine pool_condition_reset(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(pool_condition), intent(inout) :: this

  continue

  this%mass = ZERO
  this%temperature = ZERO
  this%volume = ZERO
  this%radius = ZERO
  this%area = ZERO
  this%thickness = ZERO

  return
end subroutine pool_condition_reset

!> Set pool heat transfer properties to defaults (zero)
subroutine poolhx_condition_reset(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(poolhx_condition), intent(inout) :: this

  continue

  this%net_swrad = ZERO
  this%lw_in     = ZERO
  this%lw_out    = ZERO
  this%net_lwrad = ZERO
  this%air_conv  = ZERO
  this%grnd_cond = ZERO
  this%net_flux  = ZERO

  return
end subroutine poolhx_condition_reset

end module m_pool