!> @file m_environ.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Shared environmental data module

!> @brief Shared environmental data module
!!
!! Contains environmental information and constants used throughout the
!! EXTRAN code such as air properties, solar radiation, cloud cover,
!! atmospheric stability, mixing layer altitude, and ground temperature.
module m_environ
!***********************************************************************
!     m_environ.f90                                   EXTRAN Version 1.2
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description:  Contains environmental information and constants
!                   used throughout the EXTRAN code.
!
!     Included in modules: CHIT, DIFCOEF, EXTRAN, INPUT, INPUTE,
!                          INPUTEF, MODELPAR, NSIG, PUFFINIT,
!                          PUFFMASS, RINPUT, RINPUTE, extran_wake
!
!***********************************************************************
use extran_param, only: WP, IP
use m_constant, only: ZERO, THOUSAND
implicit none

private

!> Mixing layer height, \f$\si{\meter}\f$
real(kind=WP), parameter, public :: mix_depth = THOUSAND

!> Earth and sky properties
type, public :: environment_properties
  !> Mean wind speed, \f$\si{\meter\per\second}\f$
  real(kind=WP), public :: ubar = ZERO

  !> Ambient air temperature, \f$\si{\celsius}\f$
  real(kind=WP), public :: air_temperature = ZERO

  !> Ambient air pressure, \f$\si{\milli\meter}\f$ Hg
  real(kind=WP), public :: air_pressure = ZERO

  !> Surface earth temperature, \f$\si{\celsius}\f$
  real(kind=WP), public :: earth_temperature = ZERO

  !> Solar radiation, \f$\si{watt\per\square\meter}\f$
  real(kind=WP), public :: solar_radiation_flux = ZERO

  !> Cloud cover, fraction
  real(kind=WP), public :: cloud_cover_frac = ZERO

  !> Air density, \f$\si{\kilo\gram\per\cubic\meter}\f$
  real(kind=WP), public :: air_density = ZERO

  !> Atmospheric stability class:
  !!  - A = 1
  !!  - B = 2
  !!  - C = 3
  !!  - D = 4
  !!  - E = 5
  !!  - F = 6
  !!  - G = 7
  integer(kind=IP), public :: stability_class = 0_IP
contains
  !> Zero out properties
  procedure :: clear => environment_properties_clear
  !> Calculate air density from temperature and pressure
  procedure :: calculate_air_density =>                                 &
    environment_properties_calculate_air_density
end type environment_properties

!       COMMON /environ/ ubar, z, znot, air_temp, air_press, mix_depth,
!      .                 earth_temp, sol_rad, ccover, air_density, stab
contains

!> Set environment properties to zero
subroutine environment_properties_clear(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(environment_properties), intent(inout) :: this

  continue

  this%ubar = ZERO
  this%air_temperature = ZERO
  this%air_pressure = ZERO
  this%earth_temperature = ZERO
  this%solar_radiation_flux = ZERO
  this%cloud_cover_frac = ZERO
  this%air_density = ZERO
  this%stability_class = 0_IP

  return
end subroutine environment_properties_clear

!> Calculate air density from temperature and pressure
subroutine environment_properties_calculate_air_density(this)
  use m_constant, only: STD_AIR_DENSITY, STD_TEMP, STD_PRESSURE_MMHG
  implicit none

  !> Object reference
  class(environment_properties), intent(inout) :: this

  continue

  this%air_density = STD_AIR_DENSITY                                    &
    * (STD_TEMP / (this%air_temperature + STD_TEMP))                    &
    * (this%air_pressure / STD_PRESSURE_MMHG)

  return
end subroutine environment_properties_calculate_air_density

end module m_environ