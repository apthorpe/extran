!> @file extran_input.f90
!! @author J. V. Ramsdell
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains EXTRAN input routines

!> @brief Contains EXTRAN input routines
module extran_input
  implicit none

  private

  public :: uc
  public :: input
  public :: rinput

contains

!> @brief Return an upper case letter if input is lower case letter
!!
!! The input character is examined. If has an ASCII character in the
!! range from 97 through 122, it is a lower case letter. If it is a
!! lower case letter NN is set equal to the value minus 32. This is the
!! upper case equivalent to the letter. UC is set equal to char(NN)
FUNCTION UC(A)
!***********************************************************************
!     UC                                              EXTRAN Version 1.2
!     Return an upper case letter if input is lower case letter
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 7/10
!     Updated: 10/90
!
!     Description: The input character is examined. If has an ASCII
!                  character in the range from 97 through 122, it is a
!                  lower case letter. If it is a lower case letter
!                  NN is set equal to the value minus 32. This is the
!                  upper case equivalent to the letter. UC is set equal
!                  to char(NN)
!
!*******************************************************************
  use extran_param, only: IP
  implicit none

  character(len=1) :: UC

  !> Input character
  character(len=1), intent(in) :: A

  integer(kind=IP) :: NN

  continue

  NN = int(ichar(A), IP)

  if ((NN >= 97_IP) .AND. (NN <= 122_IP)) then
    NN = NN - 32_IP
    UC = char(NN)
  else
    UC = A
  end if

  return
end function UC

!> @brief Prompts User For Model Output Options
!!
!! Allows user control of model output options
subroutine INPUTO(conc_cfg, output_cfg)
!***********************************************************************
!     INPUTO                                          EXTRAN Version 1.2
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 7/90
!     Updated: 10/90
!
!     Description: Allows user control of model output options
!
!     Relationship to other modules:
!
!         Makes calls to:  NONE
!
!         Called from:     INPUT
!
!***********************************************************************
  use, intrinsic :: iso_fortran_env, only: stdin => input_unit,         &
    stdout => output_unit
  use extran_param, only: WP, IP
  use m_options, only: concentration_config, output_config

  implicit none

  ! character(len=1) :: UC

  !> Concentration unit configuration object
  type(concentration_config), intent(inout) :: conc_cfg

  !> Output (file and printing) configuration object
  type(output_config), intent(inout) :: output_cfg

  character(len=1) :: yn
  character(len=1) :: yn1

  logical :: second_pass

  continue

  second_pass = .false.

  write(stdout,'(//A)') ' OUTPUT OPTION SELECTION '

100 continue
105 continue
  write(stdout,'(/A,/A)',advance='NO')                                  &
    'Output concentrations may be expressed as g/m**3, ppm, or ',       &
    ' mCi/m**3. Do you want concentrations in ppm? Y or N '

  read(stdin,'(A)',ERR=105) yn

  if (UC(yn) == 'Y') then
    conc_cfg%ppm = .true.
  else if (UC(yn) == 'N') then
    conc_cfg%ppm = .false.
106 continue
    write(stdout, '(/A)',advance='NO')                                  &
      ' Do you want concentrations in mCi/m**3? Y or N '
    read(stdin, '(A)') yn1
    if (UC(yn1) == 'Y') then
      conc_cfg%mci_per_m3 = .true.
    else if (UC(yn1) == 'N ') then
      conc_cfg%mci_per_m3 = .false.
    else
      goto 106
    end if
  else
    goto 105
  end if
  if (second_pass) goto 900

200 continue
205 continue
  write(stdout,'(/A)',advance='NO')                                     &
    ' Do you want primary output to be sent to a printer? Y or N '
  read(stdin,'(A)' ,ERR=205) yn
  if (UC(yn) == 'Y') then
    output_cfg%print_summary = .true.
255 continue
    write(stdout,'(/A)',advance='NO')                                   &
      ' Do you want a copy of the output sent to a file? Y or N '
    read(stdin,'(A)',ERR=255) yn1
    if (UC (yn1) == 'Y') then
      output_cfg%save_summary = .true.
    else if (UC(yn1) == 'N') then
      output_cfg%save_summary = .false.
    else
      goto 255
    end if
  else if (UC(yn) == 'N') then
    output_cfg%print_summary = .false.
    output_cfg%save_summary = .true.
  else
    goto 205
  end if
  if (second_pass) goto 900

300 continue
305 continue
    write(stdout,'(/A,/A)',advance='NO')                                &
    ' Concentrations may be plotted. Do you want a plot of',            &
    ' the output? Y or N '
  read(stdin,'(A)',ERR=305) yn
  if (UC(yn) == 'Y') then
    output_cfg%print_plot = .true.
  else if (UC(yn) == 'N') then
    output_cfg%print_plot = .false.
  else
    goto 305
  end if
  if (second_pass) goto 900

400 continue
405 write(stdout,' (/A/A)',advance='NO')                                &
    ' A concentration chronology may be printed. Do you want a',        &
    ' printer output of the concentration chronology? Y or N '
  read(stdin,' (A)' ,ERR=405) yn
  if (UC(yn) == 'Y') then
    output_cfg%print_chronology = .true.
  else if (UC(yn) == 'N') then
    output_cfg%print_chronology = .false.
  else
    goto 405
  end if
  if (second_pass) goto 900

500 continue
505 write(stdout,'(/A,A)',advance='NO')                                 &
    ' Do you want a file with the concentration chronology?',           &
    ' Y or N '
  read(stdin,'(A)',ERR=505) yn
  if (UC(yn) == 'Y') then
    output_cfg%save_chronology = .true.
  else if (UC(yn) == 'N') then
    output_cfg%save_chronology = .false.
  else
    goto 505
  end if
  if (second_pass) goto 900

600 continue
605 write(stdout,' (/A/A)',advance='NO')                                &
    ' Do you want a file with details of the release and pool',         &
    ' status and the energy balance of the pool? Y or N '
  read(stdin,'(A)' ,ERR=605) yn
  if (UC(yn) == 'Y') then
    output_cfg%save_balance = .true.
  else if (UC(yn) == 'N') then
    output_cfg%save_balance = .false.
  else
    goto 605
  end if
! REVIEW INPUT DATA AND REVISE AS NECESSARY
  second_pass = .true.

900 continue
  write(stdout,'(//A)') ' OUTPUT OPTION REVIEW:'
  if (conc_cfg%ppm) then
    write(stdout,'(1X,A)') ' 1   Concentration output in ppm '
  else if (conc_cfg%mci_per_m3) then
    write(stdout,'(1X,A)') ' 1   Concentration output in mCi/m**3'
  else
    write(stdout,'(1X,A)') ' 1   Concentration output in g/m**3 '
  end if
  if (output_cfg%print_summary) then
    write(stdout,'(1X,A)') ' 2   Print primary results'
    if (output_cfg%save_summary) then
      write(stdout,'(6X,A)') 'Create file with primary results'
    else
      write(stdout,'(6X,A)') 'Don''t create file with primary results'
    end if
  else
    write(stdout,'(1X,A)') ' 2   Don''t print primary results'
    write(stdout,'(6X,A)')  'Create file with primary results'
  end if
  if (output_cfg%print_plot) then
    write(stdout,'(1X,A)') ' 3   Plot results '
  else
    write(stdout,'(1X,A)') ' 3   Don''t plot results'
  end if
  if (output_cfg%print_chronology) then
    write(stdout,'(1X,A)')                                              &
      ' 4   Write concentration chronology to printer '
  else
    write(stdout,'(1X,A)')                                              &
      ' 4   Don''t write concentration chronology to printer '
  end if
  if (output_cfg%save_chronology) then
    write(stdout,'(1X,A)')                                              &
      ' 5   Create file with concentration chronology'
  else
    write(stdout,'(1X,A)')                                              &
      ' 5   Don''t create file with concentration chronology'
  end if
  if (output_cfg%save_balance) then
    write(stdout,'(1X,A)')                                              &
      ' 6   Create file with release, pool and energy balance data'
  else
    write(stdout, '(1X,A,A)')                                           &
      ' 6   Don''t create file with release, pool and energy balance'   &
      ,' data'
  end if
  write(stdout,'(1X,A)') ' C   CONTINUE WITH DATA ENTRY'
  write(stdout,'(1X,A)') ' X   EXIT PROGRAM '
  write(stdout,906,advance='NO')
906 format(/'  ENTER  C  TO CONTINUE IF DATA ARE CORRECT,',             &
    /'  IF DATA ARE NOT CORRECT ENTER NUMBER OF ITEM TO BE CHANGED'     &
    /'  OR, ENTER  X  TO STOP PROGRAM : ')
  read(stdin, '(A) ',ERR=900) yn
  if (UC(yn) == 'X')                                                    &
    stop '  PROGRAM STOPPED AT OUTPUT OPTION ENTRY '
  if (UC(yn) == 'C') return
  if (yn == '1') then
    goto 100
  else if (yn == '2') then
    goto 200
  else if (yn == '3') then
    goto 300
  else if (yn == '4') then
    goto 400
  else if (yn == '5') then
    goto 500
  else if (yn == '6') then
    goto 600
  else
    write(stdout, '(/A)')                                               &
      ' ENTER A NUMBER FROM 1 THRU 6, A C, OR AN X. '
    goto 900
  end if

  return
end subroutine INPUTO

!> @brief Prompts User For Source Description
!!
!! Prompts user for release and receptor geometry
subroutine INPUTS(conc_cfg, pool, intake, release, tank)
!***********************************************************************
!     INPUTS                                          EXTRAN Version 1.2
!     Prompts User For Source Description
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 10/89 from INPUT
!     Updated: 10/90
!
!     Description: Prompts user for release and receptor geometry
!
!     Relationship to other modules:
!
!         Makes calls to:  NONE
!
!         Called from:     INPUT
!
!***********************************************************************
  use, intrinsic :: iso_fortran_env, only: stdin => input_unit,         &
    stdout => output_unit
  use extran_param, only: WP, IP
  use m_options, only: concentration_config
  use m_pool, only: pool_condition
  use m_scenario, only: intake_properties, tank_properties, area,       &
    release_properties
  use m_constant, only: ZERO, HALF, ONE

  implicit none

  ! character(len=1) :: UC

  real(kind=WP), parameter :: MAX_TANK_MASS = 1.0E6_WP

  !> Concentration unit configuration
  type(concentration_config), intent(in) :: conc_cfg

  !> Pool object
  type(pool_condition), intent(inout) :: pool

  !> Control room intake properties
  type(intake_properties), intent(out) :: intake

  !> Chemical release object
  type(release_properties), intent(inout) :: release

  !> Chemical storage tank properties
  type(tank_properties), intent(out) :: tank

  integer(kind=IP) :: NN

  character(len=1) :: yn

  logical :: second_pass

  continue

  second_pass = .false.

  write(stdout,'(//A)') 'RELEASE AND RECEPTOR GEOMETRY: '
  NN = 1
100 continue

105 continue
  if (conc_cfg%mci_per_m3) then
    write(stdout,'(/A,A)',advance='NO')                                 &
      ' Enter Total Activity (Ci) in Tank',                             &
      '(e.g., 1000.0) : '
    read(stdin,'(F15.0)' ,ERR=105) tank%initial_mass
    tank%mass = tank%initial_mass
    if ((tank%mass <= ZERO)                                             &
      .OR. (tank%mass > MAX_TANK_MASS)) then
      write(stdout,'(/A)')                                              &
        ' Out of Range; Range is 0.0 to 1,000,000 Ci '
      goto 105
    else
      write(stdout,'(G15.3)') tank%mass
    end if
  else
    write(stdout,'(/A,A)',advance='NO')                                 &
      ' Enter Total Mass (kg) in Tank ',                                &
      ' (e. g., 1000. 0) : '
    read(stdin,'(F15.0)',ERR=105) tank%initial_mass
    tank%mass = tank%initial_mass
    if ((tank%mass <= ONE)                                              &
      .OR. (tank%mass > MAX_TANK_MASS)) then
      write(stdout,'(/A)') 'Out of Range; Range is 1.0 to 1,000,000 kg '
      goto 105
    else
      write(stdout,'(F15.1)') tank%mass
    end if
  end if

  if (second_pass) goto 900

200 continue

  if (.NOT. second_pass) NN = NN + 1_IP

  if (release%from_leak()) then
205 continue
    if (conc_cfg%mci_per_m3) then
      write(stdout,'(/A,A)',advance='NO')                               &
        ' Enter Release Rate in Ci/sec ',                               &
        '(e.g., 10.0) : '
      read(stdin,' (F15.0)' ,ERR=205) release%initial_rate
      release%rate = release%initial_rate

      if (release%rate >= HALF * tank%mass) then
        write(stdout,'(/A,A)') ' Release Rate Too Large ',              &
          'Catastrophic Failure '
        goto 205
      else if (release%rate <= ZERO) then
        write(stdout,'(/A)',advance='NO')                               &
          ' Release Rate must be greater than 0.0'
        goto 205
      else
        write(stdout, '(G15.3)') release%rate
      end if
    else
      write(stdout,'(/A,A)',advance='NO')                               &
        ' Enter Release Rate in kg/sec',                                &
        ' (e.g., 100.0) : '
      read(stdin,'(F15.0)',ERR=205) release%initial_rate
      release%rate = release%initial_rate
      if (release%rate >= HALF * tank%mass) then
        write(stdout,'(/A,A)') ' Release Rate Too Large',               &
          'Catastrophic Failure '
        goto 205
      else if (release%rate <= ZERO) then
        write(stdout,'(/A)',advance='NO')                               &
          ' Release Rate must be greater than 0.0'
        goto 205
      else
        write(stdout,'(F15.3)') release%rate
      end if
    end if
  end if

  if (second_pass) goto 900

300 continue

  if (.NOT. second_pass) NN = NN+1_IP

305 continue
  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Height (meters) of Release ',                               &
    '(e.g., 0.0) : '
  read(stdin,'(F15.0)',ERR=305) release%height
  if ((release%height < ZERO)                                           &
      .OR. (release%height > 100.0_WP)) then
    write(stdout,'(/A)') 'Out of Range; Range is (0.0 - 100.0)'
    goto 305
  else
    write(stdout,' (F15.1)') release%height
  end if

  if (second_pass) goto 900

400 continue

  if (.NOT. second_pass) NN = NN + 1_IP

405 continue
  write(stdout,' (/A,A)',advance='NO')                                  &
    ' Enter Storage Temperature in Tank in Degrees ',                   &
    'C (e.g., 15.0) : '
  read(stdin,' (F15.0)' ,ERR=405) tank%temperature
  if ((tank%temperature < -40.0_WP)                                     &
    .OR. (tank%temperature > 50.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is -40.0 to 50.0 C'
    goto 405
  else
    write(stdout,' (F15.1)') tank%temperature
  end if

  if (second_pass) goto 900

500 continue

  if (release%is_liquid()) then
    if (.NOT. second_pass) NN = NN + 1_IP
505 continue
    write(stdout,' (/A,A)',advance='NO')                                &
      'Enter Maximum Radius of Pool',                                   &
      ' in Meters, if appropriate (e.g., 15.0) : '
    read(stdin,'(F15.0)',ERR=505) pool%max_radius
    if ((pool%max_radius < ZERO)                                        &
      .OR. (pool%max_radius > 50.0_WP)) then
      write(stdout,'(/A)') 'Out of Range; Range is 0 to 50 m'
      goto 505
    else
      write(stdout, '(F15.1)') pool%max_radius
    end if
    if (second_pass) goto 900
  end if

600 continue

  if (.NOT. second_pass) NN = NN + 1_IP

605 continue
  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Distance (meters) to Intake',                               &
    '(e. g., 100.0) : '
  read(stdin, '(F15.0)' ,ERR=605) intake%distance
  if ((intake%distance < 5.0_WP)                                        &
    .OR. (intake%distance > 1999.0_WP)) then
    write(stdout,'(/A)') 'Out of Range; Range is (5.0 - 1999.)'
    goto 605
  else
    write(stdout,' (F15.0)') intake%distance
  end if

  if (second_pass) goto 900

700 continue

  if (.NOT. second_pass) NN = NN + 1_IP

705 continue
  write(stdout,' (/A,A)',advance='NO')                                  &
    ' Enter Height (meters) of Intake ',                                &
    '(e.g., 25.0) : '
  read(stdin,' (F15.0)' ,ERR=705) intake%height
  if ((intake%height < ZERO)                                            &
    .OR. (intake%height > 100.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is (0.0 - 100.0)'
    goto 705
  else
    write(stdout,'(F15.1)') intake%height
  end if

  if (second_pass) goto 900

800 continue

  if (.NOT. second_pass) NN = NN + 1_IP

805 continue
  write(stdout,' (/A,A)',advance='NO')                                  &
    ' Enter Area (m**2) of Building, ',                                 &
    'if release is in wake (e.g., 100.0) : '
  read(stdin,'(F15.0)' ,ERR=805) area
  if ((area < ZERO) .OR. (area > 4000.0_WP)) then
    write(stdout,'(/A)') 'Out of Range; Range is (0.0 - 4000.0)'
    goto 805
  else
    write(stdout,' (F15.0)') area
  end if
  if (second_pass) goto 900
850 continue
  release%vent_flow = ZERO
  if (release%scenario == 4) then
    if (.NOT. second_pass) NN = NN + 1_IP
855 continue
    write(stdout, '(/A,A,/A)',advance='NO')                             &
      ' If Release is thru a short stack or vent enter flow ',          &
      '(m**3/sec).  ',' Enter zero if unknown or zero. (e.g. 10.0) '
    read(stdin,' (F15.0) ',ERR=855) release%vent_flow
    if ((release%vent_flow < ZERO)                                      &
      .OR. (release%vent_flow > 50.0_WP)) then
      write(stdout,'(/A)') ' Out of Range; Range is (0.0 - 50.0) '
      goto 855
    else
      write(stdout,' (F15.2)') release%vent_flow
    end if
  end if

!       REVIEW INPUT DATA AND REVISE AS NECESSARY

  second_pass = .true.

900 continue

  write(stdout,'(//A)') ' SCENARIO DATA REVIEW: '
  if (release%scenario == 1_IP) then
    write(stdout,'(/A,A)')     '      Release Type              = ',    &
      '  Liquid Tank Burst'
    if (conc_cfg%mci_per_m3) then
      write(stdout,'(A,G16.2)')'  1   Initial Activity     (Ci) = ',    &
        tank%mass
    else
      write(stdout,'(A,F11.1)')'  1   Initial Mass         (kg) = ',    &
        tank%mass
    end if
    NN = 2_IP

  else if (release%scenario == 2_IP) then
    write(stdout,'(/A,A)')     '      Release Type              = ',    &
      '     Liquid Leak '
    if (conc_cfg%mci_per_m3) then
      write(stdout,'(A,G16.2)')'  1   Initial Activity     (Ci) = ',    &
        tank%mass
      write(stdout,'(A,G15.2)')'  2   Release Rate     (Ci/sec) = ',    &
        release%rate
    else
      write(stdout,'(A,F11.1)')'  1   Initial Mass         (kg) = ',    &
        tank%mass
      write(stdout,'(A,F12.2)')'  2   Release Rate     (kg/sec) = ',    &
        release%rate
    end if
    NN = 3_IP

  else if (release%scenario == 3_IP) then
    write(stdout,'(/A,A)')     '      Release Type              = ',    &
      '  Gas Tank Burst'
    if (conc_cfg%mci_per_m3) then
      write(stdout,'(A,G16.2)')'  1   Initial Activity     (Ci) = ',    &
        tank%mass
    else
      write(stdout,'(A,F11.1)')'  1   Initial Mass         (kg) = ',    &
        tank%mass
    end if
    NN = 2_IP

  else if (release%scenario == 4_IP) then
    write(stdout,'(/A,A)')     '      Release Type              = ',    &
      '     Gas Leak '
    if (conc_cfg%mci_per_m3) then
      write(stdout,'(A,G16.2)')'  1   Initial Activity     (Ci) = ',    &
        tank%mass
      write(stdout,'(A,G15.2)')'  2   Release Rate     (Ci/sec) = ',    &
        release%rate
    else
      write(stdout,'(A,F11.1)')'  1   Initial Mass         (kg) = ',    &
        tank%mass
      write(stdout,'(A,F11.1)')'  2   Release Rate    ?(kg/sec) = ',    &
        release%rate
    end if
    NN = 3_IP
  end if

  write(stdout,'(1X,I2,A,F10.0)') NN,'   Release Height        (m) = ', &
    release%height
  NN = NN + 1_IP
  write(stdout,'(1X,I2,A,F10.0)') NN,'   Storage Temperature   (C) = ', &
    tank%temperature
  NN = NN + 1_IP
  if (release%is_liquid()) then
    write(stdout,'(1X,I2,A,F10.0)') NN,                                 &
      '   Maximum Pool Radius   (m) = ', pool%max_radius
    NN = NN + 1_IP
  end if

  write(stdout,'(1X,I2,A,F10.0)') NN,'   Intake Distance       (m) = ', &
    intake%distance
  NN = NN + 1_IP
  write(stdout,'(1X,I2,A,F10.0)') NN,'   Intake Height         (m) = ', &
    intake%height
  NN = NN + 1_IP
  write(stdout,'(1X,I2,A,F10.0)') NN,'   Building Area      (m**2) = ', &
    area

  if (release%scenario == 4_IP) then
    NN = NN + 1_IP
    write(stdout,'(1X,I2,A,F12.2)')NN,'   Vent Flow       (m**3/s) = ', &
      release%vent_flow
  end if
  write(stdout,'(1X,A)') ' C   CONTINUE WITH DATA ENTRY '
  write(stdout,'(1X,A)') ' X   EXIT PROGRAM '
  write(stdout,906,advance='NO')
906 format(/'  ENTER  C  TO CONTINUE IF DATA ARE CORRECT,',             &
    /'  IF DATA ARE NOT CORRECT ENTER NUMBER OF ITEM TO BE CHANGED' ,   &
    /'  OR, ENTER  X  TO STOP PROGRAM : ')
  read(stdin,'(A)',ERR=900) yn
  if (UC (yn) == 'X')                                                   &
    stop '  PROGRAM STOPPED AT RELEASE AND RECEPTOR DATA ENTRY '
  if (UC(yn) == 'C') return
  if (release%scenario == 1_IP) then
    if (yn == '1') then
      goto 100
    else if (yn == '2') then
      goto 300
    else if (yn == '3') then
      goto 400
    else if (yn == '4') then
      goto 500
    else if (yn == '5') then
      goto 600
    else if (yn == '6') then
      goto 700
    else if (yn == '7') then
      goto 800
    end if
  else if (release%scenario == 2_IP) then
    if (yn == '1') then
      goto 100
    else if (yn == '2') then
      goto 200
    else if (yn == '3') then
      goto 300
    else if (yn == '4') then
      goto 400
    else if (yn == '5') then
      goto 500
    else if (yn == '6') then
      goto 600
    else if (yn == '7') then
      goto 700
    else if (yn == '8') then
      goto 800
    end if
  else if (release%scenario == 3_IP) then
    if (yn == '1') then
      goto 100
    else if (yn == '2') then
      goto 300
    else if (yn == '3') then
      goto 400
    else if (yn == '4') then
      goto 600
    else if (yn == '5') then
      goto 700
    else if (yn == '6') then
      goto 800
    end if
  else if (release%scenario == 4_IP) then
    if (yn == '1') then
      goto 100
    else if (yn == '2') then
      goto 200
    else if (yn == '3') then
      goto 300
    else if (yn == '4') then
      goto 400
    else if (yn == '5') then
      goto 600
    else if (yn == '6') then
      goto 700
    else if (yn == '7') then
      goto 800
    else if (yn == '8') then
      goto 850
    end if
  end if

  write(stdout,'(A,I2,A)')                                              &
    ' ENTER A NUMBER FROM 1 THRU ', NN, ' A C, OR AN X.'
  goto 900

  return
end subroutine INPUTS

!> @brief Prompts User For Input Data
!!
!! Prompts user for environmental data input depending on release type.
subroutine INPUTE(environment, release)
!***********************************************************************
!     INPUTE                                          EXTRAN Version 1.2
!     Prompts User For Environmental Data
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 10/89 from INPUT
!     Updated: 10/90
!
!     Description: Prompts user for environmental data input depending
!                  on release type.
!
!     Relationship to other modules:
!
!         Makes calls to:  NONE
!
!         Called from:     INPUT
!
!***********************************************************************
  use, intrinsic :: iso_fortran_env, only: stdin => input_unit,         &
    stdout => output_unit
  use extran_param, only: WP, IP
  use m_scenario, only: release_properties
  use m_environ, only: environment_properties
  use m_constant, only: ZERO, HALF, TENTH

  implicit none

  !> Environment property object
  type(environment_properties), intent(inout) :: environment

  !> Chemical release object
  type(release_properties), intent(in) :: release

  integer (kind=IP) :: NN
  integer (kind=IP) :: iccover

  character(len=1) :: yn

  logical :: second_pass

  continue

  second_pass = .false.

  NN = 4_IP
  write(stdout,'(///A)') ' ENVIRONMENTAL CONDITIONS: '

100 continue
105 continue

  write(stdout, '(/A)', advance='NO')                                   &
    'Enter Wind Speed (m/s) (e.g., 5.0) : '
  read(stdin,'(F15.0)',ERR=105) environment%ubar
  if ((environment%ubar < HALF) .OR. (environment%ubar > 20.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is (0.5 - 20.0)'
    goto 105
  else
    write(stdout,'(F15.1)') environment%ubar
  end if

  if (second_pass) goto 800

200 continue
205 continue

  write(stdout,' (/A)') ' Atmospheric Stability Classes '
  write(stdout,'(A)') ' 1 = A, 2 = S, 3 = C, 4 = D, 5 = E, 6 = F, 7 = G'
  write(stdout,'(A)',advance='NO') ' Enter Stability Class Number : '
  read(stdin,' (I1)' ,ERR=205) environment%stability_class
  if ((environment%stability_class < 1)                                 &
    .OR. (environment%stability_class > 7)) then
    write(stdout, '(/A)') ' Out of Range; Range is (l - 7)'
    goto 205
  else
    write(stdout,'(11x,i4)') environment%stability_class
  end if

  if (second_pass) goto 800

300 continue
305 continue

  write(stdout, '(/A,A)', advance='NO')                                 &
    ' Enter Ambient Air Temperature (C)',                               &
    ' (e.g., 10.0) : '
  read(stdin,'(F15.0)',ERR=305) environment%air_temperature
  if ((environment%air_temperature < -40.0_WP)                          &
    .OR. (environment%air_temperature > 50.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is (-40.0 TO +50.0)'
    goto 305
  else
    write(stdout,'(F15.0)') environment%air_temperature
  end if

  if (second_pass) goto 800

400 continue
405 continue

  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Atmospheric Pressure (mm Hg)',                              &
    ' (e.g., 760.0) : '
  read(stdin,'(F15.0)',ERR=405) environment%air_pressure
  if ((environment%air_pressure < 600.0_WP)                             &
    .OR. (environment%air_pressure > 800.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is (600.0 - 800.0)'
    goto 405
  else
    write(stdout,'(F15.1)') environment%air_pressure
  end if

  if (second_pass) goto 800

  if (release%is_gas()) goto 800

  NN = 7_IP

500 continue
505 continue

  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Value for Solar Radiation ',                                &
    '(watts/m**2) (e.g. 500.0) : '
  read(stdin,'(F15.0)' ,ERR=505) environment%solar_radiation_flux
  if ((environment%solar_radiation_flux <= ZERO)                        &
    .OR. (environment%solar_radiation_flux > 1200.0_WP)) then
    write(stdout,'(A,A)') ' Solar Radiation Out of Range, Range is ',   &
      '0 to 1200. '
    goto 505
  else
    write(stdout, '(F15.0)') environment%solar_radiation_flux
  end if

  if (second_pass) goto 800

600 continue
605 continue

  write(stdout,'(/A)',advance='NO')                                     &
    ' Enter Cloud Cover in tenths (e.g. 4 ) '
  read(stdin,'(I4)',ERR=605) iccover
  if ((iccover < 0_IP) .OR. (iccover > 10_IP)) then
    write(stdout,' (A,A)') ' Cloud Cover Out of Range, Range is ',      &
      '0 to 10 '
    goto 605
  else
    write(stdout, '(10X,I4)') iccover
  end if
  environment%cloud_cover_frac = real(iccover, WP) * TENTH

  if (second_pass) goto 800

700 continue
705 continue

  write(stdout, '(/A,A)', advance='NO')                                 &
    ' Enter Temperature (C) of the Ground',                             &
    ' (e.g., Air Temp. + 10.0) : '
  read(stdin, '(F15.0)' ,ERR=705) environment%earth_temperature
  if ((environment%earth_temperature < -40.0)                           &
    .OR. (environment%earth_temperature > 60.0)) then
    write(stdout,'(/A)') ' Out of Range; Range is (-40.0 to +60.0)'
    goto 705
  else
    write(stdout,'(F15.0)') environment%earth_temperature
  end if

  second_pass = .true.

! REVIEW INPUT DATA AND REVISE AS NECESSARY

800 continue

  write(stdout,'(//A)') ' ENVIRONMENTAL CONDITIONS REVIEW:'
  write(stdout,'(A,F10.1)')                                             &
    '  1   Wind Speed               (m/sec) = ',                        &
    environment%ubar
  write(stdout,'(A,I8)')                                                &
    '  2   Atmosperic Stability Class       = ',                        &
    environment%stability_class
  write(stdout,'(A,F10.1)')                                             &
    '  3   Air Temperature              (C) = ',                        &
    environment%air_temperature
  write(stdout,'(A,F10.1)')                                             &
    '  4   Atmospheric Pressure     (mm Hg) = ',                        &
    environment%air_pressure
  if (release%is_liquid()) then
    write(stdout,'(A,F10.1)')                                           &
    '  5   Solar Radiation     (watts/m**2) = ',                        &
    environment%solar_radiation_flux
    write(stdout,'(A,I8)')                                              &
    '  6   Cloud Cover             (tenths) = ', iccover
    write(stdout,'(A,F10.1)')                                           &
    '  7   Ground Temperature           (C) = ',                        &
    environment%earth_temperature
  end if
  write(stdout, '(1X,A)') ' C   CONTINUE WITH DATA ENTRY '
  write(stdout,' (1X,A)') ' X   EXIT PROGRAM '

!900   continue

  write(stdout ,901,advance='NO')
901 format(/'  ENTER  C  TO CONTINUE IF DATA ARE CORRECT,',             &
    /'  IF DATA ARE NOT CORRECT ENTER NUMBER OF ITEM TO BE CHANGED',    &
    /'  OR, ENTER  X  TO STOP PROGRAM: ')
  read(stdin,'(A)',ERR=800) yn

  if (UC (yn) == 'X')                                                   &
    stop '  PROGRAM STOPPED AT ENVIRONMENTAL DATA ENTRY '

  if (UC(yn) == 'C') return

  if (yn == '1') then
    goto 100
  else if (yn == '2') then
    goto 200
  else if (yn == '3') then
    goto 300
  else if (yn == '4') then
    goto 400
  end if

  if (release%is_liquid()) then
    if (yn == '5') then
      goto 500
    else if (yn == '6') then
      goto 600
    else if (yn == '7') then
      goto 700
    end if
  end if

  write(stdout,'(A,I2,A)')                                              &
    '  ENTER A NUMBER FROM 1 THRU ', NN, ', A C, OR AN X. '
  goto 800

  return
end subroutine INPUTE

!> @brief Prompts User For Effluent Data
!!
!! Prompts user for effluent characteristics.
subroutine INPUTEF(environment, release, effluent)
!***********************************************************************
!     INPUTEF                                         EXTRAN Version 1.2
!     Prompts User For Effluent Data
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 10/89 from INPUT
!     Updated: 10/90
!
!     Description: Prompts user for effluent characteristics.
!
!     Relationship to other modules:
!
!         Makes calls to:  DIFCOEF
!
!         Called from:     INPUT
!
!***********************************************************************
  use, intrinsic :: iso_fortran_env, only: stdin => input_unit,         &
    stdout => output_unit
  use extran_param, only: WP, IP, NCHEMLIM, NCHEMPROPS
  use extran_chemical, only: chemical_db
  use extran_file, only: chemical_datafile
  use m_effluent, only: effluent_properties
  use m_environ, only: environment_properties
  use m_scenario, only: release_properties
  use m_constant, only: ZERO, ONE, PI, ugcnst => RGAS, std_temp,        &
    STD_PRESSURE_MMHG

  implicit none

  !> Environment properties object
  type(environment_properties), intent(in) :: environment

  !> Chemical release object
  type(release_properties), intent(in) :: release

  !> Chemical effluent object
  type(effluent_properties), intent(inout) :: effluent

  integer(kind=IP) :: chem_id
  logical :: second_pass
  integer(kind=IP) :: nn

  type(chemical_db) :: chem_db

  character(len=1) :: yn

  continue

  ! READ IN THE CHEMICAL DATA FILE
  call chem_db%reset()
  call chem_db%populate_from_file(chemical_datafile)

  write(stdout,'(//A)') ' EFFLUENT CHARACTERISTICS:'
  second_pass = .false.
  NN = 2

  ! Select name of material released (NN=1)
50 continue
55 continue
  write(stdout,'(/A)') ' ******* MATERIAL RELEASED ******* '

  ! Display chemical names in two columns
  block
    integer(kind=IP) :: nend
    integer(kind=IP) :: I
    integer(kind=IP) :: indx
    nend = int(chem_db%nspecies / 2_IP, IP)
    do I = 1, nend
      indx = 2_IP * I - 1_IP
      write(stdout,'(2x,A,A,I2,10x,A,A,I2)')                            &
        chem_db%species(indx)%name, ' ==> ', indx,                      &
        chem_db%species(indx+1)%name, ' ==> ', indx+1
    end do
  end block
  if (mod(chem_db%nspecies,2_IP) == 1_IP) then
    write(stdout,'(2x,A,A,I2)') chem_db%species(chem_db%nspecies)%name, &
      ' ==>', chem_db%nspecies
  end if

  write(stdout,'(/A)',advance='NO') ' ENTER SELECTION NUMBER: '

  read(stdin,'(I4)',ERR=55) chem_id
  if (chem_id < 1 .OR. chem_id > chem_db%nspecies) then
    write(stdout,'(A,I2)') ' SELECTION MUST BE IN THE RANGE 1 TO ',     &
      chem_db%nspecies
    goto 55
  else
    write(stdout,'(11X,I4)') chem_id
  end if

!100   continue

  if (chem_id /= chem_db%nspecies) then
    ! Set effluent properties to selected chemical
    call effluent%from_chemical(chem_db%species(chem_id))

    ! Correct boiling point for atmospheric pressure
    call effluent%calculate_ref_and_boil_temp(environment%air_pressure)

    ! Estimate diffusion coeffient if not supplied
    if ((effluent%diff_coef <= ZERO) .AND. (release%is_liquid())) then
      write(stdout,'(A,A,/A)') ' Diffusion coefficient in data file',   &
        ' is zero. One will be computed',                               &
        ' from molecular weight, boiling point and density.'
      call effluent%estimate_diff_coeff(environment%air_pressure)
      write(stdout, '(/A,F8.3)') ' Computed diffusion coefficient = ',  &
        effluent%diff_coef
    end if

    goto 800
  else
    write(stdout,'(/A,A,/A)')' See Perry and Chilton, or Handbook of ', &
      'Chemistry and Physics',' for Physical Properties'
    write(stdout,' (/A,/A,/A)') ' ****** WARNING ***********',          &
      ' PROGRAM WILL NOT GIVE CORRECT RESULTS IF DATA ARE NOT ',        &
      ' ENTERED IN THE PROPER UNITS !!!!!!!!!!!!! '
105 continue
    write(stdout, '(/A,A)',advance='NO') ' Enter Name of Substance, ',  &
      '20 Character Maximum : '
    read(stdin, '(A)' ,ERR=105) effluent%name
    if (second_pass) goto 800
  end if

  ! Select molecular weight (NN=2)
200 continue
205 continue
  write(stdout,' (/A,A)',advance='NO')                                  &
    ' Enter Molecular Weight (grams/mole) of Substance ',               &
    ' (e.g., 70.9) : '
  read(stdin,'(F15.0)' ,ERR=205) effluent%molec_wt
  if (effluent%molec_wt <= ONE) then
    write(stdout,'(A)') ' Molecular Weight Must Be> 1.0 '
    goto 205
  else
    write(stdout,'(F15.1)') effluent%molec_wt
  end if
  if (second_pass) goto 800
  if (release%is_gas()) goto 800
  NN = 7

  ! Select boiling point at standard conditions (NN=3)
300 continue
305 continue
  write(stdout, '(/A,A)',advance='NO')                                  &
    ' Enter Boiling Point (C) of Liquid ',                              &
    ' at 760 mm Hg (e.g., -34.1) '
  read(stdin,'(F15.0)' ,ERR=305) effluent%std_boiling_point
  if ((effluent%std_boiling_point < -270.0_WP)                          &
    .OR. (effluent%std_boiling_point > 200.0_WP)) then
    write(stdout,'(A)') ' Boiling Point Range 1S -270C to +200C '
    goto 305
  else
    write(stdout,' (F15.1)') effluent%std_boiling_point
  end if

  ! Correct boiling point for atmospheric pressure
  call effluent%calculate_ref_and_boil_temp(environment%air_pressure)

  if (second_pass) goto 800

  ! Select heat capacity (NN=4)
400 continue
405 continue
  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Heat Capacity (j/gm-C) of Liquid Phase ' ,                  &
    ' (e.g., 0.946) : '
  read(stdin,'(F15.0)',ERR=405) effluent%cp
  if (effluent%cp <= ZERO) then
    write(stdout,'(A)') ' Heat Capacity Must be> 0.0 '
    goto 405
  else
    write(stdout,'(1PE15.2)') effluent%cp
  end if
  if (second_pass) goto 800

! Select heat of vaporization (NN=5)
500 continue
505 continue
  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Heat of Vaporization (j/gm) of Liquid Phase',               &
    ' (e. g., 288 .) : '
  read(stdin,'(F15.0)',ERR=505) effluent%hv
  if (effluent%hv <= ZERO) then
    write(stdout,'(A)') ' Heat of Vaporization Must be> 0.0 '
    goto 505
  else
    write(stdout,'(1PE15.2)') effluent%hv
  end if
  if (second_pass) goto 800

! Select density or specific gravity of liquid phase (NN=6)
600 continue
605 continue
  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Density or Specific Gravity of Liquid Phase ',              &
    '(e.g., 1.57) : '
  read(stdin,'(F15.0)' ,ERR=605) effluent%source_density
  if ((effluent%source_density < 0.1_WP)                                &
    .OR. (effluent%source_density > 4.0_WP)) then
    write(stdout,'(A)') ' Specific Gravity Range is 0.1 to 4.0 '
    goto 605
  else
    write(stdout,'(1PE15.2)') effluent%source_density
  end if
  if (second_pass) goto 800

! Select initial diffusion coefficient (NN=7)
700 continue
705 continue
    write(stdout,'(/A,A)',advance='NO')                                 &
    ' Enter Diffusion Coefficient (cm**2/sec) of Source',               &
    ' (e.g., .0792) : '
  read(stdin,'(F15.0)' ,ERR=705) effluent%initial_diff_coef
  effluent%diff_coef = effluent%initial_diff_coef
  if (effluent%diff_coef > 0.4_WP) then
    write(stdout,'(A)') 'Diffusion coefficient too large, ',            &
      'maximum is 0.4'
    goto 705
  else if (effluent%diff_coef <= ZERO) then
    write(stdout,'(A)') ' Diffusion coefficient will be computed'
    call effluent%estimate_diff_coeff(environment%air_pressure)
    write(stdout,'(F15.3)') effluent%diff_coef
  else
    write(stdout,'(F15.3)') effluent%diff_coef
  end if
  second_pass = .true.

! REVIEW INPUT DATA AND REVISE AS NECESSARY

800 continue
  write(stdout,'(///A)') ' EFFLUENT CHARACTERISTICS REVIEW:'

  write(stdout,'(A,A)')       '  1   Material Released           =   ', &
    effluent%name
  write(stdout,'(A,1PE10.2)') '  2   Molecular Weight (gm/mole)  = ',   &
    effluent%molec_wt
  if (release%is_gas()) goto 850
  write(stdout,'(A,1PE10.2)') '  3   Initial Boiling Point  (C)  = ',   &
    effluent%std_boiling_point
!  write(stdout,'(A,1PE10.2,A)') '  3   Initial Boiling Point  (C)  = ', &
!    std_boil_pt, ' (STP)'
!  write(stdout,'(A,1PE10.2)') '      Initial Boiling Point  (C)  = ',   &
!    boil_point
  write(stdout,'(A,1PE10.2)') '  4   Heat Capacity     (j/gm-C)  = ',   &
    effluent%cp
  write(stdout,'(A,1PE10.2)') '  5   Heat of Vapor.      (j/gm)  = ',   &
    effluent%hv
  write(stdout,'(A,1PE10.2)') '  6   Specific Gravity            = ',   &
    effluent%source_density
  write(stdout,'(A,1PE10.2)') '  7   Diffusion Coef. (cm**2/sec) = ',   &
    effluent%diff_coef
850 continue
  write(stdout,'(1X,A)') ' C   BEGIN CALCULATIONS '
  write(stdout,'(1X,A)') ' X   EXIT PROGRAM'
855 continue
  write(stdout ,856,advance='NO')
856 format(/' ENTER  C  TO CONTINUE IF DATA ARE CORRECT,',              &
    /' IF DATA ARE NOT CORRECT ENTER NUMBER OF ITEM TO BE CHANGED',     &
    /' OR, ENTER  X  TO STOP PROGRAM: ')
  read(stdin,'(a)' ,ERR=855) yn
  if (UC(yn) == 'X')                                                    &
    stop '  PROGRAM STOPPED AT EFFLUENT DATA ENTRY '
  if (UC(yn) == 'C') return

  if (yn == '1') then
    goto 50
  else if (yn == '2') then
    goto 200
  end if
  if (release%is_liquid()) then
    if (yn == '3') then
      goto 300
    else if (yn == '4') then
      goto 400
    else if (yn == '5') then
      goto 500
    else if (yn == '6') then
      goto 600
    else if (yn == '7') then
      goto 700
    end if
  end if
  write(stdout,'(A,I2,A)')                                              &
    '   ENTER NUMBER FROM 1 THRU ', NN, ' A C, OR AN X. '
  goto 800

  return
end subroutine INPUTEF

!> @brief Prompts User For Input Data
!!
!! Prompts user for input data into the WAKE model.
!! There are four types of input data:
!!   -# output control options
!!   -# release and receptor geometry
!!   -# source and effluent characteristics
!!   -# environmental data.
subroutine INPUT(title, RDATE, RTIME, ocfg, conc_cfg, output_cfg,       &
  environment, pool, effluent, intake, release, tank)
!***********************************************************************
!     INPUT                                           EXTRAN Version 1.2
!     Prompts User For Input Data
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 11/88
!     Updated: 10/90
!
!     Description: Prompts user for input data into the WAKE model.
!                  There are four types of input data:
!                  1) output control options
!                  2) release and receptor geometry
!                  3) source and effluent characteristics
!                  4) environmental data.
!
!     Relationship to other modules:
!
!         Makes calls to:  INPUTE, INPUTEF, INPUTO, INPUTS
!
!         Called from:     EXTRAN
!
!***********************************************************************
  use, intrinsic :: iso_fortran_env, only: stdin => input_unit,         &
    stdout => output_unit
  use extran_file, only: outfile_config
  use extran_param, only: WP, IP
  use extran_version, only: CODENAME, VERSION, BUILDDATE
  use m_effluent, only: effluent_properties
  use m_environ, only: environment_properties
  use m_pool, only: pool_condition
  use m_options, only: concentration_config, output_config
  use m_scenario, only: intake_properties, tank_properties, area,       &
    release_properties
  use m_constant, only: ZERO, ONE, PI, TEN, ugcnst => RGAS, STD_TEMP,   &
    STD_PRESSURE_MMHG, STD_AIR_DENSITY, HUNDRED, THOUSAND, FORMFEED

  implicit none

  ! character(len=1) :: UC

  real(kind=WP), parameter :: CM2_PER_M2 = HUNDRED * HUNDRED

  real (kind=WP), parameter :: pool_radius_limit = 9999.9_WP

  !> Run title
  character(len=70), intent(out) :: TITLE

  !> Run date
  character(len=10), intent(in) :: RDATE

  !> Run time
  character(len=8), intent(in) :: RTIME

  !> Output file configuration object
  type(outfile_config), intent(inout) :: ocfg

  !> Concentration unit configuration object
  type(concentration_config), intent(inout) :: conc_cfg

  !> Output (file and printing) configuration object
  type(output_config), intent(inout) :: output_cfg

  !> Environment property object
  type(environment_properties), intent(inout) :: environment

  !> Chemical pool object
  type(pool_condition), intent(inout) :: pool

  !> Chemical effluent object
  type(effluent_properties), intent(inout) :: effluent

  !> Control room intake object
  type(intake_properties), intent(out) :: intake

  !> Chemical release object
  type(release_properties), intent(out) :: release

  !> Chemical storage tank properties
  type(tank_properties), intent(out) :: tank

  real (kind=WP) :: log_sat_press
  ! real (kind=WP) :: mol_vol

  integer (kind=IP) :: iccover

  character(len=1) :: yn

  continue

  write(stdout,1) CODENAME, VERSION, BUILDDATE, CODENAME
1 format(/' Program Title: ', A, ' VERSION ', A, //                     &
    ' Developed For:    U.S. Nuclear Regulatory Commission'/            &
    '                   Office of Nuclear Regulatory Research'/         &
    '                   Division of Reactor Accident Analysis'//        &
    ' Date:             ', A, //                                        &
    ' NRC Contact(s):   C. Ferrell        Phone: (FTS) 492 3944'/       &
    ' Code Developer:   J. V. Ramsdell    Phone: (509) 376-8626'/       &
    '                                            (FTS) 444-8626'//      &
    ' Code Documentation: '/                                            &
    '                  ', A, ': A Computer Code For Estimating'/        &
    '                  Concentrations Of Toxic Substances At'/          &
    '                  Control Room Air Intakes'/                       &
    '                  NUREG/CR-5656 '////                              &
    ' The program was prepared for an agency of the United States',     &
    ' Government. Neither'/' the United States Government nor any',     &
    ' agency thereof, nor any of their'/' employees, makes any',        &
    ' warranty, expressed or implied, or assumes any legal'/            &
    ' liability or responsibilities for any third party''s use,',       &
    ' or the results of such'/' use, of any portion of this' ,          &
    ' program or represents that its use by such third'/' party',       &
    ' would not infringe privately owned rights. '//)
!      PAUSE ' PRESS ENTER TO CONTINUE '

!     GET TITLE FOR RUN

  write(stdout, '(/A)') ' ENTER TITLE FOR OUTPUT -- 70 CHARACTER MAXIMUM'
  read(stdin,'(A)') title

!     GET INPUT OPTIONS

  call INPUTO(conc_cfg, output_cfg)

! PROMPT USER FOR GEOMETRY OF INTAKE AND RELEASE HEIGHT

  write(stdout,'(//A)') ' SCENARIO:'
100 continue
205 format(/A,/A,/A,/A,/A,/A)
105 continue
  write(stdout, 205, advance='NO')                                      &
    ' Available Release Scenarios ',                                    &
    '     1 = Liquid Tank Burst ',                                      &
    '     2 = Liquid Tank Leak ',                                       &
    '     3 = Gas Tank Burst',                                          &
    '     4 = Gas Tank Leak',                                           &
    ' Enter Scenario Number; 1 Through 4 : '
  read(stdin, '(I2)', ERR=105) release%scenario
  if ((release%scenario < 1) .OR. (release%scenario > 4)) then
    write(stdout, '(/A)') ' Out of Range; Enter 1 Through 4 '
    goto 105
  else
    write(stdout,'(BN,I4)') release%scenario
  end if
206 format(///A)
106 continue
  write(stdout, 206, advance='NO') ' The Scenario Selected is: '
  if (release%scenario == 1) then
    write(stdout,'(30X,A)') ' Liquid Tank Burst '
  else if (release%scenario == 2) then
    write(stdout, '(30X,A)') ' Liquid Tank Leak'
  else if (release%scenario == 3) then
    write(stdout, '(30X,A)') ' Gas Tank Burst '
  else if (release%scenario == 4) then
    write(stdout, '(30X,A)') ' Gas Tank Leak'
  else
    write(stdout, '(//A,/A,/A//)')                                      &
      ' ***********************************',                           &
      '  Scenario Type Not Properly Defined',                           &
      ' ***********************************'
    goto 105
  end if
  if (conc_cfg%mci_per_m3 .AND. release%is_liquid()) then
    write(stdout,'(//A/A/A/A//)')                                       &
      ' **************************************************** ',         &
      '                    ... WARNING ...                   ' ,        &
      ' A Liquid Release in Ci will not give correct results ',         &
      ' *****************************************************'
  end if
  write(stdout, '(A)', advance='NO') '   IS SCENARIO CORRECT, Y or N? '
  read(stdin, '(A)', ERR=106)  yn
  if ((UC(yn) /= 'Y')) goto 100

  call INPUTS(conc_cfg, pool, intake, release, tank)

!....... PROMPT USER FOR ENVIRONMENTAL CONDITIONS

  call INPUTE(environment, release)

!....... PROMPT USER FOR EFFLUENT CHARACTERISTICS

  call INPUTEF(environment, release, effluent)

! OPEN OUTPUT FILES

  if (output_cfg%print_summary .OR. output_cfg%print_plot               &
    .OR. output_cfg%print_chronology) then
    ! open(UNIT=3, FILE='LPT1',STATUS='UNKNOWN')
    open(newunit=ocfg%uprint, FILE=ocfg%printer_file, STATUS='NEW')
  end if

  if (output_cfg%save_summary) then
    open(newunit=ocfg%usum, FILE=ocfg%summary_file, STATUS='NEW')
  end if

  if (output_cfg%save_chronology) then
    open(newunit=ocfg%ucron, FILE=ocfg%chronology_file, STATUS='NEW')
  end if

  if (output_cfg%save_balance) then
    open(newunit=ocfg%ubal, FILE=ocfg%balance_file, STATUS='NEW')
  end if

! LIST INPUT DATA IN PRIMARY OUTPUT

  if (output_cfg%print_summary .OR. output_cfg%print_plot               &
      .OR. output_cfg%print_chronology) then
    write(ocfg%uprint, 2) CODENAME, VERSION, BUILDDATE, CODENAME
2   format(/10X,'Program Title:   ', A, ' VERSION ', A, //              &
      10X,'Developed For:   U.S. Nuclear Regulatory Commission'/        &
      10X,'                 Office of Nuclear Regulatory Research'/     &
      10X,'                 Division of Reactor Accident Analysis'//    &
      10X,'Date:            ', A, //                                    &
      10X,'NRC Contact(s):  C. Ferrell     Phone: (FTS) 492 3944'/      &
      10X,'Code Developer:  J. V. Ramsdell Phone: (509) 376-8626'/      &
      10X,'                                       (FTS) 444-8626'//     &
      10X,'Code Documentation: ' /                                      &
      10X,'                 ', A, ': A Computer Code For Estimating'/   &
      10X,'                 Concentrations Of Toxic Substances At'/     &
      10X,'                 Control Room Air Intakes'/                  &
      10X,'                 NUREG/CR-5656'////                          &
      10X,'The program was prepared for an agency of the United',       &
      'States',/10X,'Government. Neither the United States ',           &
      'Government nor any',/10X,'agency thereof, nor any of their ',    &
      'employees, makes any',/10X,'warranty, expressed or implied, ',   &
      'or assumes any legal'/10X,'liability or responsibilities for ',  &
      'any third party''s use,'/10X,'or the results of such use, of ',  &
      'any portion of this program',/10X,'or represents that its use',  &
      ' by such third party would not',/10X,'infringe privately ',      &
      'owned rights. '//)
    write(ocfg%uprint, 3) title
3   format(/11X, A70)
    write(ocfg%uprint, 4) RDATE, RTIME
4   format(/10X,' RUN DATE = ', A10, '  RUN TIME = ', A8)
  end if
  if (output_cfg%print_summary) then
    write(ocfg%uprint, *) formfeed
    write(ocfg%uprint, 3) title
    write(ocfg%uprint, 4) RDATE, RTIME
    write(ocfg%uprint, 5)
5   format(/10X,' SCENARIO:')
    if (release%scenario == 1) then
      write(ocfg%uprint,6)
6     format(10X,' Release Type             =  Liquid Tank Burst')
    else if (release%scenario == 2) then
      write(ocfg%uprint, 7)
7     format(10X,' Release Type             =  Liquid Tank Leak ')
    else if (release%scenario == 3) then
      write(ocfg%uprint, 8)
8     format(10X,' Release Type             =  Gas Tank Burst ')
    else if (release%scenario == 4) then
      write(ocfg%uprint, 9)
9     format(10X,' Release Type             =  Gas Tank Leak ')
    else
      STOP ' !!!!!!  RELEASE TYPE ILL DEFINED !!!!!!! '
    end if
    if (conc_cfg%mci_per_m3) then
      write(ocfg%uprint, 10) tank%mass
10    format(10X,' Initial Activity    (Ci) = ',F10.0)
      if (release%from_leak()) then
        write(ocfg%uprint, 11) release%rate
11      format(10X,' Release Rate    (Ci/sec) = ',1PE16.2)
      end if
    else
      write(ocfg%uprint, 35) tank%mass
35    format(10X,' Initial Mass        (kg) = ', F10.0)
      if (release%from_leak()) then
        write(ocfg%uprint, 36) release%rate
36      format(10X,' Release Rate    (kg/sec) = ',1PE16.2)
      end if
    end if
    write(ocfg%uprint, 12) release%height
12  format(10X,' Release Height       (m) = ',F11.1)
    write(ocfg%uprint, 13) tank%temperature
13  format(10X,' Storage Temperature  (C) = ',F11.1)
    if (release%is_liquid()) then
      write(ocfg%uprint, 14) pool%max_radius
14    format(10X,' Maximum Pool Radius  (m) = ' , F11.1)
    end if
    write(ocfg%uprint, 15) intake%distance
15  format(10X,' Intake Distance      (m) = ',F10.0)
    write(ocfg%uprint, 16) intake%height
16  format(10X,' Intake Height        (m) = ',F11.1)
    write(ocfg%uprint, 17) area
17  format(10X,' Building Area     (m**2) = ',F10.0)
    if (release%scenario == 4) then
      write(ocfg%uprint, 18) release%vent_flow
18    format(10X,' Vent Flow       (m**3/s) = ',F12.2)
    end if
    write(ocfg%uprint, 19)
19  format(//10X,' ENVIRONMENTAL CONDITIONS:')
    write(ocfg%uprint, 20) environment%ubar
20  format(10X,' Wind Speed                (m/sec) = ' ,F10.1)
    write(ocfg%uprint, 21) environment%stability_class
21  format(10X,' Atmospheric Stability Class       = ',I8)
    write(ocfg%uprint, 22) environment%air_temperature
22  format(10X,' Air Temperature               (C) = ',F10.1)
    write(ocfg%uprint, 23) environment%air_pressure
23  format(10X,' Atmospheric Pressure      (mm Hg) = ',F10.1)
    if (release%is_liquid()) then
      write(ocfg%uprint, 24) environment%solar_radiation_flux
24    format(10X,' Solar Radiation      (watts/m**2) = ',F10.1)
      iccover = int(environment%cloud_cover_frac * TEN, IP)
      write(ocfg%uprint, 25) iccover
25    format(10X,' Cloud Cover              (tenths) = ',I8)
      write(ocfg%uprint, 26) environment%earth_temperature
26    format(10X,' Ground Temperature            (C) = ',F10.1)
    end if

    write(ocfg%uprint, 27)
27  format(//10X,' EFFLUENT CHARACTERISTICS:')
    write(ocfg%uprint, 28) effluent%name
28  format(10X,' Material Released                 = ',5X,A)
    write(ocfg%uprint, 29) effluent%molec_wt
29  format(10X,' Molecular Weight        (gm/mole) = ',F10.1)
    if (release%is_liquid()) then
      write(ocfg%uprint, 30) effluent%boiling_point
30    format(10X,' Initial Boiling Point         (C) = ',F10.1)
      write(ocfg%uprint, 31) effluent%cp
31    format(10X,' Heat Capacity            (j/gm-C) = ',F12.3)
      write(ocfg%uprint, 32) effluent%hv
32    format(10X,' Heat of Vapor.             (j/gm) = ',F10.1)
      write(ocfg%uprint, 33) effluent%source_density
33    format(10X,' Specific Gravity                  = ',F12.3)
      write(ocfg%uprint, 34) effluent%diff_coef
34    format(10X,' Diffusion Coef.       (cm**2/sec) = ',F12.3)
    end if
  end if
  if (output_cfg%save_summary) then
    write(ocfg%usum, 2) CODENAME, VERSION, BUILDDATE, CODENAME
    write(ocfg%usum, 3) title
    write(ocfg%usum, 4) RDATE, RTIME
    write(ocfg%usum, 5)
    if (release%scenario == 1_IP) then
      write(ocfg%usum, 6)
    else if (release%scenario == 2_IP) then
      write(ocfg%usum, 7)
    else if (release%scenario == 3_IP) then
      write(ocfg%usum, 8)
    else if (release%scenario == 4_IP) then
      write(ocfg%usum, 9)
    else
      ! write(stdout, '("ERROR: release%scenario is ", I2)') release%scenario
      STOP ' !!!!!!  RELEASE TYPE ILL DEFINED !!!!!!! '
    end if
    if (conc_cfg%mci_per_m3) then
      write(ocfg%usum,10) tank%mass
      if (release%from_leak()) then
        write(ocfg%usum,11) release%rate
      end if
    else
      write(ocfg%usum,35) tank%mass
      if (release%from_leak()) then
        write(ocfg%usum,36) release%rate
      end if
    end if
    write(ocfg%usum,12) release%height
    write(ocfg%usum,13) tank%temperature
    if (release%is_liquid()) then
      write(ocfg%usum,14) pool%max_radius
    end if
    write(ocfg%usum,15) intake%distance
    write(ocfg%usum,16) intake%height
    write(ocfg%usum,17) area
    if (release%scenario == 4) then
      write(ocfg%usum,18) release%vent_flow
    end if
    write(ocfg%usum,19)
    write(ocfg%usum,20) environment%ubar
    write(ocfg%usum,21) environment%stability_class
    write(ocfg%usum,22) environment%air_temperature
    write(ocfg%usum,23) environment%air_pressure
    if (release%is_liquid()) then
      write(ocfg%usum,24) environment%solar_radiation_flux
      iccover = int(environment%cloud_cover_frac * TEN, IP)
      write(ocfg%usum,25) iccover
      write(ocfg%usum,26) environment%earth_temperature
    end if
    write(ocfg%usum,27)
    write(ocfg%usum,28) effluent%name
    write(ocfg%usum,29) effluent%molec_wt
    if (release%is_liquid()) then
      write(ocfg%usum,30) effluent%boiling_point
      write(ocfg%usum,31) effluent%cp
      write(ocfg%usum,32) effluent%hv
      write(ocfg%usum,33) effluent%source_density
      write(ocfg%usum,34) effluent%diff_coef
    end if
  end if
! CORRECT FOR DEPARTURE FROM STANDARD CONDITIONS

  if (release%is_liquid()) then

! Compute air_density at air_temp and air_press

    call environment%calculate_air_density()

! Compute initial saturation vapor pressure

    if (effluent%boiling_point > tank%temperature) then
      call effluent%calculate_pconst()
      log_sat_press = effluent%pconst                                   &
        * (ONE - (effluent%boiling_point + STD_TEMP)                    &
        / (tank%temperature + STD_TEMP))
      effluent%sat_pressure =                                           &
        environment%air_pressure * exp(log_sat_press)
    else
      effluent%sat_pressure = environment%air_pressure
    end if
  end if

! COMPUTE FACTOR TO CONVERT g/m**3 TO ppm

  call effluent%calculate_ppm_conversion(environment)

! CONVERT FROM CGS TO MKS

  effluent%cp = effluent%cp * THOUSAND
  effluent%hv = effluent%hv * THOUSAND
  effluent%sat_pressure = effluent%sat_pressure / THOUSAND
  effluent%source_density = effluent%source_density * THOUSAND
  effluent%diff_coef = effluent%diff_coef / CM2_PER_M2
  environment%air_pressure = environment%air_pressure / THOUSAND

! SET MAXIMUM POOL RADIUS TO DEFAULT IF RADIUS NOT SUPPLIED BY USER
! AND COMPUTE MAXIMUM POOL AREA

  if (pool%max_radius == ZERO) then
    pool%max_radius = pool_radius_limit
  end if
  pool%max_area = PI * pool%max_radius**2
  return
end subroutine INPUT

!> @brief Allows User to Revise Environmental Data
!!
!! Prompts user for environmental data input depending on release type.
subroutine RINPUTE(environment, release, effluent)
!***********************************************************************
!     RINPUTE                                         EXTRAN Version 1.2
!     Allows User to Revise Environmental Data
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 7/90 from INPUT
!     Updated: 10/90
!
!     Description: Prompts user for environmental data input depending
!                  on release type.
!
!     Relationship to other modules:
!
!         Makes calls to:  NONE
!
!         Called from:     RINPUT
!
!***********************************************************************
  use, intrinsic :: iso_fortran_env, only: stdin => input_unit,         &
    stdout => output_unit
  use extran_param, only: WP, IP
  use m_effluent, only: effluent_properties
  use m_environ, only: environment_properties
  use m_constant, only: ZERO, TENTH, HALF, ONE, TEN, STD_TEMP,          &
    ugcnst => RGAS, STD_PRESSURE_MMHG
  use m_scenario, only: release_properties

  implicit none

  !  real(kind=WP), parameter :: ugcnst = 8.3144_WP

  !> Environment properties object
  type(environment_properties), intent(inout) :: environment

  !> Chemical release object
  type(release_properties), intent(in) :: release

  !> Chemical effluent object
  type(effluent_properties), intent(inout) :: effluent

  integer(kind=IP) :: NN
  integer(kind=IP) :: iccover

  character(len=1) :: yn

  continue

  write(stdout,'(//A)') ' REVISE ENVIRONMENTAL CONDITIONS:'
  if (release%is_liquid()) then
    NN = 7
  else
    NN = 4
  end if

10 continue

!....... REVIEW INPUT DATA AND REVISE AS NECESSARY

  write(stdout,'(//A)') ' ENVIRONMENTAL CONDITIONS REVIEW:'
  write(stdout,'(A,F10.1)')                                             &
    '  1   Wind Speed               (m/sec) = ',                        &
    environment%ubar
  write(stdout,'(A,I8)')                                                &
    '  2   Atmospheric Stability Class      = ',                        &
    environment%stability_class
  write(stdout,'(A,F10.1)')                                             &
    '  3   Air Temperature              (C) = ',                        &
    environment%air_temperature
  write(stdout,'(A,F10.1)')                                             &
    '  4   Atmospheric Pressure     (mm Hg) = ',                        &
    environment%air_pressure
  if (release%is_liquid()) then
    write(stdout,'(A,F10.1)')                                           &
    '  5   Solar Radiation     (watts/m**2) = ',                        &
    environment%solar_radiation_flux
    iccover = int(TEN * environment%cloud_cover_frac, IP)
    write(stdout,'(A,I8)')                                              &
    '  6   Cloud Cover             (tenths) = ', iccover
    write(stdout,'(A,F10.1)')                                           &
    '  7   Ground Temperature           (C) = ',                        &
    environment%earth_temperature
  end if

  write(stdout,'(1X,A)') ' C   START CALCULATIONS '
  write(stdout,'(1X,A)') ' X   EXIT PROGRAM '
  write(stdout ,21,advance='NO')
21 format(/'  ENTER  C  TO CONTINUE IF DATA ARE CORRECT,',              &
    /' IF DATA ARE NOT CORRECT ENTER NUMBER OF ITEM TO BE CHANGED',     &
    /' OR, ENTER  X  TO STOP PROGRAM: ')
  read(stdin,'(A)',ERR=10) yn
  if (UC(yn) == 'X')                                                  &
    stop '  PROGRAM STOPPED AT ENVIRONMENTAL DATA REVISION '
  if (UC(yn) == 'C') return

!..... DETERMINE ITEM TO BE REVISED

  if (yn == '1') then
    goto 100
  else if (yn == '2') then
    goto 200
  else if (yn == '3') then
    goto 300
  else if (yn == '4') then
    goto 400
  else if (yn == '5') then
    goto 500
  else if (yn == '6') then
    goto 600
  else if (yn == '7') then
    goto 700
  end if

  write(stdout,'(A,I2,A)')                                              &
    '  ENTER NUMBER FROM 1 THRU ', NN, ' A C, OR AN X. '
  goto 10

!...... DATA REVISION SECTION

100 continue
105 continue

  write(stdout,'(/A)',advance='NO')                                     &
    ' Enter Wind Speed (m/s) (e.g., 5.0) : '
  read(stdin,'(F15.0)' ,ERR=105) environment%ubar
  if ((environment%ubar < HALF) .OR. (environment%ubar > 20.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is (0.5 - 20.0)'
    goto 105
  else
    write(stdout,'(F15.1)') environment%ubar
  end if
  goto 10

200 continue
205 continue

  write(stdout,'(/A)') ' Atmospheric Stability Classes'
  write(stdout,'(A)') ' 1 = A, 2 = B, 3 = C, 4 = D, 5 = E, 6 = F, 7 = G'
  write(stdout,'(A)',advance='NO') ' Enter Stability Class Number : '
  read(stdin,'(I1)',ERR=205) environment%stability_class
  if ((environment%stability_class < 1)                                 &
    .OR. (environment%stability_class > 7)) then
    write(stdout,'(/A)') ' Out of Range; Range is (1 - 7)'
    goto 205
  else
    write(stdout,'(11X,I4)') environment%stability_class
  end if
  goto 10

300 continue
305 continue

  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Ambient Air Temperature (C)',                               &
    ' (e.g., 10.0) : '
  read(stdin,'(F15.0)' ,ERR=305) environment%air_temperature
  if ((environment%air_temperature < -40.0_WP)                          &
    .OR. (environment%air_temperature > 50.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is (-40.0 TO +50.0)'
    goto 305
  else
    write(stdout,'(F15.0)') environment%air_temperature
  end if
  goto 10

400 continue
405 continue

  write(stdout,' (/A,A)',advance='NO')                                  &
    ' Enter Atmospheric Pressure (mm Hg)',                              &
    ' (e.g., 760.0) : '
  read(stdin,'(F15.0)',ERR=405) environment%air_pressure
  if ((environment%air_pressure < 600.0_WP)                             &
    .OR. (environment%air_pressure > 800.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is (600.0 - 800.0)'
    goto 405
  else
    write(stdout,'(F15.1)') environment%air_pressure
  end if

  ! Correct boiling point for atmospheric pressure

  call effluent%calculate_ref_and_boil_temp(environment%air_pressure)

  ! Reset diffusion coefficient

  effluent%diff_coef = effluent%initial_diff_coef
  if ((effluent%diff_coef <= ZERO) .AND. (release%is_liquid())) then
    write(stdout,'(A)') ' Computing revised diffusion coefficient '
    call effluent%estimate_diff_coeff(environment%air_pressure)
  end if

  goto 10

500 continue
505 continue

  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Value for Solar Radiation ',                                &
    '(watts/m**2) (e.g. 500.0) : '
  read(stdin,' (F15.0)' ,ERR=505) environment%solar_radiation_flux
  if ((environment%solar_radiation_flux < ZERO)                         &
    .OR. (environment%solar_radiation_flux > 1200.0_WP)) then
    write(stdout,' (A,A)') ' Solar Radiation Out of Range, Range is ',  &
      '0 to 1200. '
    goto 505
  else
    write(stdout, '(F15.0)') environment%solar_radiation_flux
  end if
  goto 10

600 continue
605 continue

  write(stdout,'(/A)',advance='NO')                                     &
    ' Enter Cloud Cover in tenths (e.g. 4 ) '
  read(stdin,'(I4)',ERR=605) iccover
  environment%cloud_cover_frac = real(iccover, WP) * TENTH
  if ((iccover < 0) .OR. (iccover > 10)) then
    write(stdout,'(A,A)') ' Cloud Cover Out of Range, Range is ',       &
      '0 to 10 '
    goto 605
  else
    write(stdout,'(I4)') iccover
  end if
  goto 10

700 continue
705 continue

  write(stdout,'(/A,A)',advance='NO')                                   &
    ' Enter Temperature (C) of the Ground',                             &
    ' (e.g., Air Temp. + 10.0) : '
  read(stdin,'(F15.0)',ERR=705) environment%earth_temperature
  if ((environment%earth_temperature < -40.0_WP)                        &
    .OR. (environment%earth_temperature > 60.0_WP)) then
    write(stdout,'(/A)') ' Out of Range; Range is (-40.0 to +60.0)'
    goto 705
  else
    write(stdout,'(F15.0)') environment%earth_temperature
  end if
  goto 10

  return
end subroutine RINPUTE

!> @brief Prompts User For Input Data
!!
!! Prompts user for revisions to the environmental data for multiple
!! runs.
subroutine RINPUT(title, RDATE, RTIME, ocfg, conc_cfg, output_cfg,      &
  environment, pool, effluent, intake, release, tank)
!***********************************************************************
!     RINPUT                                          EXTRAN Version 1.2
!     Prompts User For Input Data
!
!     J.V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created: 7/90
!     Updated: 10/90
!
!     Description: Prompts user for revisions to the environmental
!                  data for multiple runs.
!
!     Relationship to other modules:
!
!         Makes calls to:  RINPUTE
!
!         Called from:     EXTRAN
!
!***********************************************************************
  use extran_file, only: outfile_config
  use extran_param, only: WP, IP
  use extran_version, only: CODENAME, VERSION, BUILDDATE
  use m_effluent, only: effluent_properties
  use m_environ, only: environment_properties
  use m_options, only: concentration_config, output_config
  use m_pool, only: pool_condition
  use m_scenario, only: intake_properties, tank_properties, area,       &
    release_properties
  use m_constant, only: ZERO, ONE, TEN, STD_TEMP, PI, ugcnst => RGAS,   &
    STD_PRESSURE_MMHG, STD_AIR_DENSITY, HUNDRED, THOUSAND, FORMFEED

  implicit none

  real(kind=WP), parameter :: CM2_PER_M2 = HUNDRED * HUNDRED

  !> Run title
  character(len=70), intent(in) :: TITLE

  !> Run date
  character(len=10), intent(in) :: RDATE

  !> Run time
  character(len=8), intent(in) :: RTIME

  !> Output file configuration object
  type(outfile_config), intent(inout) :: ocfg

  !> Concentration unit configuration object
  type(concentration_config), intent(in) :: conc_cfg

  !> Output (file and printing) configuration object
  type(output_config), intent(in) :: output_cfg

  !> Environment properties object
  type(environment_properties), intent(inout) :: environment

  !> Chemical pool object
  type(pool_condition), intent(in) :: pool

  !> Chemical effluent object
  type(effluent_properties), intent(inout) :: effluent

  !> Control room intake object
  type(intake_properties), intent(in) :: intake

  !> Chemical release object
  type(release_properties), intent(in) :: release

  !> Chemical storage tank object reference
  type(tank_properties), intent(in) :: tank

  integer(kind=IP) :: iccover

  real(kind=WP) :: log_sat_press

  continue

!     RESET ENVIRONMENTAL VARIABLES TO INITIAL cgs VALUES

  effluent%cp = effluent%cp / THOUSAND
  effluent%hv = effluent%hv / THOUSAND
  effluent%source_density = effluent%source_density / THOUSAND
  effluent%diff_coef = effluent%diff_coef * CM2_PER_M2
  environment%air_pressure = environment%air_pressure * THOUSAND

!     PROMPT USER FOR ENVIRONMENTAL CONDITIONS

  call RINPUTE(environment, release, effluent)

!     OPEN OUTPUT FILES

  if (output_cfg%print_summary .OR. output_cfg%print_plot               &
      .OR. output_cfg%print_chronology) then
    ! open(UNIT=3, FILE='LPT1',STATUS='UNKNOWN')
!    ocfg%uprint = 3
    open(newunit=ocfg%uprint, FILE=ocfg%printer_file, STATUS='NEW')
  end if

  if (output_cfg%save_summary) then
!    ocfg%usum = 10
    open(newunit=ocfg%usum, FILE=ocfg%summary_file, STATUS='NEW')
  end if

  if (output_cfg%save_chronology) then
!    ocfg%ucron = 11
    open(newunit=ocfg%ucron, FILE=ocfg%chronology_file, STATUS='NEW')
  end if

  if (output_cfg%save_balance) then
!    ocfg%ubal = 12
    open(newunit=ocfg%ubal, FILE=ocfg%balance_file, STATUS='NEW')
  end if

!....... LIST INPUT DATA IN PRIMARY OUTPUT

  if (output_cfg%print_summary .OR. output_cfg%print_plot               &
    .OR. output_cfg%print_chronology) then
    write(ocfg%uprint,*) FORMFEED
    write(ocfg%uprint,2) CODENAME, VERSION, BUILDDATE, CODENAME
2   format(/10X,'Program Title:   ', A, ' VERSION ', A, //              &
      10X,'Developed For:   U.S. Nuclear Regulatory Commission'/        &
      10X,'                 Office of Nuclear Regulatory Research'/     &
      10X,'                 Division of Reactor Accident Analysis'//    &
      10X,'Date:            ', A, //                                    &
      10X,'NRC Contact(s):  C. Ferrell      Phone: (FTS) 492 3944'/     &
      10X,'Code Developer:  J. V. Ramsdell  Phone: (509) 376-8626'/     &
      10X,'                                        (FTS) 444-8626'//    &
      10X,'Code Documentation: ' /                                      &
      10X,'                 ', A, ': A Computer Code For Estimating'/   &
      10X,'                 Concentrations Of Toxic Substances At'/     &
      10X,'                 Control Room Air Intakes'/                  &
      10X,'                 NUREG/CR-5656'////                          &
      10X,'The program was prepared for an agency of the United ',      &
      'States',/10X,'Government.  Neither the United States ',          &
      'Government nor any',/10x,'agency thereof, nor any of their ',    &
      'employees, makes any',/10x,'warranty, expressed or implied, ',   &
      'or assumes any legal'/10x,'liability or responsibilities for ',  &
      'any third party''s use,'/10x,'or the results of such use, of ',  &
      'any portion of this program',/10x,'or represents that its use',  &
      'by such third party would not',/10x,'infringe privately ',       &
      'owned rights.'// )
    write(ocfg%uprint,3) title
3   format(/11X,A70)
    write(ocfg%uprint,4) RDATE, RTIME
4   format(/10X,' RUN DATE = ',A10,'  RUN TIME = ',A8)
    write(ocfg%uprint,*) FORMFEED
  end if
  if (output_cfg%print_summary) then
    write(ocfg%uprint,3) title
    write(ocfg%uprint,4) RDATE, RTIME
    write(ocfg%uprint,5)
5   format(/10X,' SCENARIO:')
    if (release%scenario == 1) then
      write(ocfg%uprint,6)
6     format(10X,' Release Type             = Liquid Tank Burst ')
    else if (release%scenario == 2) then
      write(ocfg%uprint,7)
7     format(10X,' Release Type             = Liquid Tank Leak')
    else if (release%scenario == 3) then
      write(ocfg%uprint,8)
8     format (10X, 'Release Type            = Gas Tank Burst')
    else if (release%scenario == 4) then
      write(ocfg%uprint,9)
9     format(10X,' Release Type             = Gas Tank Leak')
    else
      stop ' !!!!!!  RELEASE TYPE ILL DEFINED !!!!!!! '
    end if

    if (conc_cfg%mci_per_m3) then
      write(ocfg%uprint,10) tank%mass
10    format(10X,' Initial Mass        (Ci) = ',F10.0)
      if (release%from_leak()) then
        write(ocfg%uprint,11) release%rate
11      format(10X,' Release Rate    (Ci/sec) = ',1PE16.2)
      end if
    else
      write(ocfg%uprint,35) tank%mass
35    format(10X,' Initial Mass        (kg) = ',F10.0)
      if (release%from_leak()) then
        write(ocfg%uprint,36) release%rate
36      format(10X,' Release Rate    (kg/sec) = ',1PE16.2)
      end if
    end if
    write(ocfg%uprint,12) release%height
12  format(10X,' Release Height       (m) = ',F11.1)
    write(ocfg%uprint,13) tank%temperature
13  format(10X,' Storage Temperature  (C) = ',F11.1)
    if (release%is_liquid()) then
      write(ocfg%uprint,14) pool%max_radius
14    format(10X,' Maximum Pool Radius  (m) = ',F11.1)
    end if
    write(ocfg%uprint,15) intake%distance
15  format(10X,' Intake Distance      (m) = ',F10.0)
    write(ocfg%uprint,16) intake%height
16  format(10X,' Intake Height        (m) = ',F11.1)
    write(ocfg%uprint,17) area
17  format(10X,' Building Area     (m**2) = ',F10.0)
    if (release%scenario == 4) then
      write(ocfg%uprint,18) release%vent_flow
18    format(10X,' Vent F1ow       (m**3/s) = ',F12.2)
    end if

    write(ocfg%uprint,19)
19  format(//10X,' ENVIRONMENTAL CONDITIONS:')
    write(ocfg%uprint,20) environment%ubar
20  format(10X,' Wind Speed                (m/sec) = ',F10.1)
    write(ocfg%uprint,21) environment%stability_class
21  format(10X,' Atmospheric Stability Class       = ', I8)
    write(ocfg%uprint,22) environment%air_temperature
22  format(10X,' Air Temperature               (C) = ',F10.1)
    write(ocfg%uprint,23) environment%air_pressure
23  format(10X,' Atmospheric Pressure      (mm Hg) = ',F10.1)
    if (release%is_liquid()) then
      write(ocfg%uprint,24) environment%solar_radiation_flux
24    format(10X,' Solar Radiation      (watts/m**2) = ',F10.1)
      iccover = int(environment%cloud_cover_frac * TEN, IP)
      write(ocfg%uprint,25) iccover
25    format(10X,' Cloud Cover              (tenths) = ',I8)
      write(ocfg%uprint,26) environment%earth_temperature
26    format(10X,' Ground Temperature            (C) = ',F10.1)
    end if

    write(ocfg%uprint,27)
27  format(//10X,' EFFLUENT CHARACTERISTICS:')
    write(ocfg%uprint,28) effluent%name
28  format(10X,' Material Released                 = ',5X,A)
    write(ocfg%uprint,29) effluent%molec_wt
29  format(10X,' Molecular Weight        (gm/mole) = ',F10.1)
    if (release%is_liquid()) then
      write(ocfg%uprint,30) effluent%boiling_point
30    format(10X,' Initial Boiling Point         (C) = ',F10.1)
      write(ocfg%uprint,31) effluent%cp
31    format(10X,' Heat Capacity            (j/gm-C) = ',F12.3)
      write(ocfg%uprint,32) effluent%hv
32    format(10X,' Heat of Vapor.             (j/gm) = ',F10.1)
      write(ocfg%uprint,33) effluent%source_density
33    format(10X,' Specific Gravity                  = ',F12.3)
      write(ocfg%uprint,34) effluent%diff_coef
34    format(10X,' Diffusion Coef.       (cm**2/sec) = ',F12.3)
    end if
  end if

  if (output_cfg%save_summary) then
    write(ocfg%usum,2) CODENAME, VERSION, BUILDDATE, CODENAME
    write(ocfg%usum,3) title
    write(ocfg%usum,4) RDATE, RTIME
    write(ocfg%usum,5)
    if (release%scenario == 1) then
      write(ocfg%usum,6)
    else if (release%scenario == 2) then
      write(ocfg%usum,7)
    else if (release%scenario == 3) then
      write(ocfg%usum,8)
    else if (release%scenario == 4) then
      write(ocfg%usum,9)
    else
      stop ' !!!!!!  RELEASE TYPE ILL DEFINED !!!!!!! '
    end if
    if (conc_cfg%mci_per_m3) then
      write(ocfg%usum,10) tank%mass
      if (release%from_leak()) then
        write(ocfg%usum,11) release%rate
      end if
    else
      write(ocfg%usum,35) tank%mass
      if (release%from_leak()) then
        write(ocfg%usum,36) release%rate
      end if
    end if
    write(ocfg%usum,12) release%height
    write(ocfg%usum,13) tank%temperature
    if (release%is_liquid()) write(ocfg%usum,14) pool%max_radius
    write(ocfg%usum,15) intake%distance
    write(ocfg%usum,16) intake%height
    write(ocfg%usum,17) area
    if (release%scenario == 4) then
      write(ocfg%usum,18) release%vent_flow
    end if
    write(ocfg%usum,19)
    write(ocfg%usum,20) environment%ubar
    write(ocfg%usum,21) environment%stability_class
    write(ocfg%usum,22) environment%air_temperature
    write(ocfg%usum,23) environment%air_pressure
    if (release%is_liquid()) then
      write(ocfg%usum,24) environment%solar_radiation_flux
      iccover = int(environment%cloud_cover_frac * TEN, IP)
      write(ocfg%usum,25) iccover
      write(ocfg%usum,26) environment%earth_temperature
    end if
    write(ocfg%usum,27)
    write(ocfg%usum,28) effluent%name
    write(ocfg%usum,29) effluent%molec_wt
    if (release%is_liquid()) then
      write(ocfg%usum,30) effluent%boiling_point
      write(ocfg%usum,31) effluent%cp
      write(ocfg%usum,32) effluent%hv
      write(ocfg%usum,33) effluent%source_density
      write(ocfg%usum,34) effluent%diff_coef
    end if
  end if

!     CORRECT FOR DEPARTURE FROM STANDARD CONDITIONS

  if (release%is_liquid()) then

!     Compute air_density at air_temp and air_press

    call environment%calculate_air_density()

!     Correct boiling point for atmospheric pressure

    call effluent%calculate_ref_and_boil_temp(environment%air_pressure)

!     Compute initial saturation vapor pressure

    if (effluent%boiling_point > tank%temperature) then
      call effluent%calculate_pconst()
      log_sat_press = effluent%pconst                                   &
        * (ONE - (effluent%boiling_point + STD_TEMP)                    &
                 / (tank%temperature + STD_TEMP))
      effluent%sat_pressure = environment%air_pressure                  &
        * exp(log_sat_press)
    else
      effluent%sat_pressure = environment%air_pressure
    end if
  end if

!     COMPUTE FACTOR TO CONVERT g/m**3 TO ppm

  call effluent%calculate_ppm_conversion(environment)

!     CONVERT FROM CGS TO MKS

  effluent%cp = effluent%cp * THOUSAND
  effluent%hv = effluent%hv * THOUSAND
  effluent%sat_pressure = effluent%sat_pressure / THOUSAND
  effluent%source_density = effluent%source_density * THOUSAND
  effluent%diff_coef = effluent%diff_coef / CM2_PER_M2
  environment%air_pressure = environment%air_pressure / THOUSAND

  return
end subroutine RINPUT

end module extran_input
