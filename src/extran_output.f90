!> @file extran_output.f90
!! @author J. V. Ramsdell
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains EXTRAN output routines

!> @brief Contains EXTRAN output routines
module extran_output
  implicit none

  private

  public :: ceplot

contains

!> @brief Plotting Subroutine
!!
!! Based on EVPLOT written 8/85 for Extreme Wind Analysis
subroutine CEPLOT(title, RDATE, RTIME, gname, dose_log, ocfg, conc_cfg)
!***********************************************************************
!
!     CEPLOT                                          EXTRAN Version 1.2
!     Plotting Subroutine
!
!     J. V. Ramsdell
!     Pacific Northwest Laboratory
!     PO Box 999
!     Richland, Washington 99352
!
!     Created:  11/88 from EVPLOT written 8/85 for Extreme Wind Analysis
!     Updated:  10/90
!
!     Relationship to other modules:
!
!         Makes calls to: NONE
!
!         Called from:    EXTRAN
!
!***********************************************************************
  use extran_file, only: outfile_config
  use extran_param, only: WP
  use m_options, only: concentration_config
  use m_print, only: dose_track
  use m_constant, only: ZERO, ONE, LINEFEED, FORMFEED, ESC, PRCOND,     &
    PRNORM

  implicit none

  ! SET PRINTER CONTROLS

  ! 6 LINES/INCH
  character(len=2), parameter :: L6 = ESC // '2'

  ! 8 LINES/INCH
  character(len=2), parameter :: L8 = ESC // '0'

  !> Run title
  character(len=70), intent(in) :: TITLE

  !> Run date
  character(len=10), intent(in) :: RDATE

  !> Run time
  character(len=8), intent(in) :: RTIME

  !> Name of chemical released
  character(len=20), intent(in) :: GNAME

  !> Concentrationa and exposure chronology object reference
  type(dose_track), intent(in) :: dose_log

  !> Output configuration object
  type(outfile_config), intent(in) :: ocfg

  !> Concentration units configuration object
  type(concentration_config), intent(in) :: conc_cfg

  integer :: I
  integer :: I1
  integer :: ICMIN
  integer :: IEMIN
  integer :: II
  integer :: ISTEP
  integer :: J
  integer :: JA
  integer :: JC
  integer :: JE
  integer :: K
  integer :: K1
  integer :: K2
  integer :: KSTEP
  integer :: N

  real(kind=WP) :: ALGAC
  real(kind=WP) :: ALOGC
  real(kind=WP) :: ALOGE
  real(kind=WP) :: CMAX
  real(kind=WP) :: EMAX

  integer :: CMAG
  integer :: EMAG
  integer :: IN
  integer :: IIP

  ! PLOT 'CANVAS', 132 COLUMNS BY 66 ROWS
  character(len=1), dimension(132, 66) :: GRAPH

  character(len=40) :: LGND1
  character(len=40) :: LGND2
  character(len=40) :: LGND3

  character(len=3) :: TEMP3
  character(len=4) :: TEMP4
  character(len=34) :: XAXIS
  character(len=6) :: YAXIS1
  character(len=13) :: YAXIS2
  character(len=8) :: YAXIS3
  character(len=29) :: DATTIM

  do J = 1, 66
    do I = 1, 132
      GRAPH(I,J) = ' '
    end do
  end do

  if (conc_cfg%ppm) then
    LGND1 = 'C = CONCENTRATION (ppm)                 '
    LGND2 = 'A = AVERAGE CONCENTRATION (ppm)         '
  else if (conc_cfg%mci_per_m3) then
    LGND1 = 'C = CONCENTRATION (mCi/m**3)            '
    LGND2 = 'A = AVERAGE CONCENTRATION (mCi/m**3)    '
  else
    LGND1 = 'C = CONCENTRATION (g/m**3)              '
    LGND2 = 'A = AVERAGE CONCENTRATION (g/m**3)      '
  end if
  if (conc_cfg%mci_per_m3) then
    LGND3 = 'E = TOTAL EXPOSURE [(mCi-sec)/m**3]     '
  else
    LGND3 = 'E = TOTAL EXPOSURE [(g-sec)/m**3]       '
  end if

! SET BORDERS FOR PLOT

! HORIZONTAL BORDERS
  do N = 22, 112
    K = mod(N-22, 10)
    if (K /= 0) then
      GRAPH(N,1)  = '.'
      GRAPH(N,61) = '.'
    else
      GRAPH(N,1)  = '+'
      GRAPH(N,2)  = '+'
      GRAPH(N,60) = '+'
      GRAPH(N,61) = '+'
    end if
  end do

! VERTICAL BORDERS
  do N = 1, 61
    K = mod(N-1, 15)
    if (K /= 0) then
      GRAPH(22,N)  = '-'
      GRAPH(112,N) = '-'
    else
      GRAPH(22,N)  = '+'
      GRAPH(23,N)  = '+'
      GRAPH(111,N) = '+'
      GRAPH(112,N) = '+'
    end if
  end do
  GRAPH(22,2)   = '+'
  GRAPH(22,60)  = '+'
  GRAPH(112,2)  = '+'
  GRAPH(112,60) = '+'

! ADD TIME SCALE
  if (dose_log%count > 91) then
    ISTEP = 2
  else
    ISTEP = 1
  end if
  K1 = 10 * ISTEP + 1
  K2 = 90 * ISTEP + 1
  KSTEP = 10 * ISTEP
  GRAPH(22,63) = '0'
  do K = K1, K2, KSTEP
    write(TEMP4(1:4), '(I4)') dose_log%datum(K)%TTIME
    if (ISTEP == 1) then
      I1 = K + 18
    else
      I1 = K / ISTEP + 19
    end if
    do II = 1,4
      GRAPH(I1+II,63) = TEMP4(II:II)
    end do
  end do

! DETERMINE RANGES FOR CONCENTRATION AND EXPOSURE SCALES
  CMAX = -99.0
  EMAX = -99.0
  do I = 1, dose_log%count
    CMAX = max(dose_log%datum(I)%conc,CMAX)
    EMAX = max(dose_log%datum(I)%expos,EMAX)
  end do
  if (CMAX < ONE) then
    ICMIN = int(log10(CMAX)) - 4
  else
    ICMIN = int(log10(CMAX) + ONE) - 4
  end if
  if (EMAX < ONE) then
    IEMIN = int(log10(EMAX)) - 4
  else
    IEMIN = int(log10(EMAX) + ONE) - 4
  end if

! PLACE SCALE VALUES IN CHARACTER STRINGS
  K = 4
  do J = 1,61,15
    CMAG = ICMIN + K
    EMAG = IEMIN + K
    write(TEMP3(1:3), '(I3)') CMAG
    do II = 1,3
      GRAPH(17+II,J) = TEMP3(II:II)
    end do
    write(TEMP3(1:3), '(I3)') EMAG
    do II = 1, 3
      GRAPH(113+II ,J) = TEMP3(II:II)
    end do
    K = K - 1
  end do

! COMPUTE NORMALIZED PLOTTING POSITIONS AND PLACE SYMBOLS IN GRAPH
  do I= 1, dose_log%count , ISTEP
    if (ISTEP == 1) then
      IIP = 21 + I
    else
      IIP = 21 + (I + 1) / 2
    end if
    if (dose_log%datum(I)%avconc > ZERO) then
      ALGAC = log10(dose_log%datum(I)%avconc)
      JA = 61 - int(15.0_WP * (ALGAC - ICMIN))
      if (JA >= 1 .AND. JA <= 61) GRAPH(IIP,JA) = 'A'
    end if
    if (dose_log%datum(I)%expos > ZERO) then
      ALOGE = log10(dose_log%datum(I)%expos)
      JE = 61 - int(15.0_WP * (ALOGE - IEMIN))
      if (JE >= 1 .AND. JE <= 61) GRAPH(IIP,JE) = 'E'
    end if
    if (dose_log%datum(I)%conc > ZERO) then
      ALOGC = log10(dose_log%datum(I)%conc)
      JC = 61 - int(15.0_WP * (ALOGC - ICMIN))
      if (JC >= 1 .AND. JC <= 61) GRAPH(IIP,JC) = 'C'
    end if
  end do

! ADD AXIS LABELS, TITLE AND LEGEND

  DATTIM = 'RUN ON            AT         '
  write(DATTIM(8:17),'(A10)') RDATE
  write(DATTIM(22:29),'(A8)') RTIME
  XAXIS = 'TIME AFTER ARRIVAL AT INTAKE (sec)'
  YAXIS1 = 'LOG OF'
  YAXIS2 = 'CONCENTRATION'
  YAXIS3 = 'EXPOSURE'
  IN = 1
  do II = 1,70
!   TITLE PLACED AT GRAPH(40-109,51)
    GRAPH(39+II,51) = TITLE(II:II)
    if (II <= 6) then
      GRAPH(7+II,30) = YAXIS1(II:II)
      GRAPH(119+II,30) = YAXIS1(II:II)
    end if
    if (II <= 8) GRAPH(119+II,32) = YAXIS3(II:II)
    if (II <= 13) GRAPH(4+II,32) = YAXIS2(II:II)
    if (II <= 20) then
!     GNAME PLACED AT GRAPH(40-59,54)
      GRAPH(39+II,54) = GNAME(II:II)
    end if
    if (II <= 29) GRAPH(39+II,52) = DATTIM(II:II)
    if (II <= 34) GRAPH(49+II,65) = XAXIS(II:II)
    if (II <= 40) then
!     LGND1 PLACED AT GRAPH(40-79,56)
      GRAPH(39+II,56) = LGND1(II:II)
!     LGND2 PLACED AT GRAPH(40-79,57)
      GRAPH(39+II,57) = LGND2(II:II)
!     LGND3 PLACED AT GRAPH(40-79,58)
      GRAPH(39+II,58) = LGND3(II:II)
    end if
  end do

! PREPARE FOR PLOTTING DATA

  write(ocfg%uprint,*) FORMFEED, L8, PRCOND
  write(ocfg%uprint,*) LINEFEED, LINEFEED, LINEFEED, LINEFEED,          &
             LINEFEED, LINEFEED, LINEFEED, LINEFEED

! PLOT GRAPH

  do J = 1,66
    write(ocfg%uprint,300) (GRAPH(I,J), I=1,132)
300 format(15X,132A1)
  end do

! RESTORE PRINTER

  write(ocfg%uprint,*) L6, PRNORM
  return
end subroutine CEPLOT

end module extran_output
