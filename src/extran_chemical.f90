!> @file extran_chemical.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains chemical property record and database classes

!> @brief Contains chemical property record and database classes
module extran_chemical
  use extran_param, only: WP, IP, NCHEMLIM, NCHEMPROPS
  use m_constant, only: ZERO
  implicit none

  private

  public :: create_chemical

  !> Chemical property database entry
  type, public :: chemical
    !> Species name
    character(len=20) :: name = '__DEFAULT__         '

    !> Molecular (formula) weight, \f$\si{\gram\per\mol}\f$
    real(kind=WP) :: molec_wt = ZERO

    !> Boiling point at standard conditions, \f$\si{\celsius}\f$
    real(kind=WP) :: std_boil_pt = ZERO

    !> Specific heat, \f$\si{\joule\per\gram\per\kelvin}\f$
    real(kind=WP) :: cp = ZERO

    !> Latent heat of vaporization, \f$\si{\joule\per\gram}\f$
    real(kind=WP) :: hv = ZERO

    !> Density, \f$\si{\gram\per\cubic\centi\meter}\f$
    real(kind=WP) :: density = ZERO

    !> Initial (reference) molecular diffusion coefficient,
    !! \f$\si{\square\centi\meter\per\second}\f$
    real(kind=WP) :: diff_coef = ZERO

  contains

    !> Set chemical properties to defaults (zero)
    procedure :: reset => chemical_reset

  end type chemical

  !> Chemical property database
  type, public :: chemical_db

    !> Total number of available species
    integer(kind=IP) :: nspecies

    !> Array of chemical property records
    type(chemical), dimension(NCHEMLIM) :: species

  contains

    !> Set chemical properties to defaults (zero)
    procedure :: reset => chemical_db_reset

    !> Create new chemical record from name and property list and add
    !! it to the database
    procedure :: add_entry => chemical_db_add_entry

    !> Create chemical property record from name and ordered list of
    !! properties and append to database
    procedure :: populate_from_file => chemical_db_populate_from_file

  end type chemical_db

contains

!> Set chemical properties to defaults (zero)
subroutine chemical_reset(this)
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(chemical), intent(inout) :: this

  continue

  this%name = '__DEFAULT__         '

  this%molec_wt    = ZERO
  this%std_boil_pt = ZERO
  this%cp          = ZERO
  this%hv          = ZERO
  this%density     = ZERO
  this%diff_coef   = ZERO

  return
end subroutine chemical_reset

!> Set chemical properties to defaults (zero)
subroutine chemical_db_reset(this)
  use extran_param, only: IP, NCHEMLIM
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(chemical_db), intent(inout) :: this

  integer :: i

  continue

  do i = 1, NCHEMLIM
    call this%species(i)%reset()
  end do

  this%nspecies = 0_IP

  return
end subroutine chemical_db_reset

!> Create chemical property record from name and ordered list of
!! properties and append to database
subroutine chemical_db_add_entry(this, name, props)
  use extran_param, only: IP, NCHEMPROPS, NCHEMLIM
  implicit none

  !> Object reference
  class(chemical_db), intent(inout) :: this

  !> Chemical species name
  character(len=20), intent(in) :: name

  !> Chemical property array
  real(kind=WP), dimension(NCHEMPROPS), intent(in) :: props

  continue

  if (this%nspecies < NCHEMLIM) then
    this%nspecies = this%nspecies + 1_IP
    this%species(this%nspecies) = create_chemical(name, props)
  else
    ! Fail silently if no more space to add chemical records
  end if

  return
end subroutine chemical_db_add_entry

!> Create chemical property record from name and ordered list of
!! properties and append to database
subroutine chemical_db_populate_from_file(this, filename)
  use, intrinsic :: iso_fortran_env, only: stdout => output_unit
  use extran_param, only: IP, NCHEMPROPS, NCHEMLIM
  use m_constant, only: ZERO
  implicit none

  !> Object reference
  class(chemical_db), intent(inout) :: this

  !> Chemical database name
  character(len=*), intent(in) :: filename

  ! File I/O unit
  integer :: uchem

  ! I/O status code
  integer :: ioerr

  ! I/O status message
  character(len=80) :: iomsg

  ! Number of chemical properties in database
  integer(kind=IP) :: nprops

  ! Chemical species name
  character(len=20) :: name

  ! Chemical property array
  real(kind=WP), dimension(NCHEMPROPS) :: props

  ! Error message
  character(len=80) :: errmsg

  integer(kind=IP) :: i

1 format(1X, A20, 6F8.0)

  continue

  open(newunit=uchem, file=filename, status='OLD',                      &
    iostat=ioerr, iomsg=iomsg)

  if (ioerr /= 0) then
    errmsg = 'ERROR: Cannot read from ' // trim(filename) // ': '       &
      // trim(iomsg)
    stop errmsg
  endif

  read(unit=uchem, fmt='(15X,I5)', iostat=ioerr, iomsg=iomsg) nprops
  if (ioerr /= 0) then
    errmsg = 'ERROR: Cannot read from ' // trim(filename) // ': '       &
      // trim(iomsg)
    stop errmsg
  endif

  if (nprops > NCHEMLIM) then
    write(stdout,'(//A)') '   ******* CHEMICAL FILE TO LARGE **********'
    stop
  end if

  do i = 1_IP, nprops
    read(unit=uchem, fmt=1, iostat=ioerr, iomsg=iomsg)                  &
      name, props(1:NCHEMPROPS)
    if (ioerr == 0) then
      call this%add_entry(name, props)
    elseif (ioerr < 0) then
      if (i < nprops) then
        ! Early termination
        errmsg = 'Warning: read fewer chemicals than expected from '    &
          // trim(filename)
        write(stdout,'(//A)') errmsg
      else
        ! Normal termination
      end if
      exit
    else
      errmsg = 'ERROR: failed to read from ' // trim(filename) // ': '  &
        // trim(iomsg)
      stop errmsg
    endif
  end do

  write(stdout,'(/A)') ' CHEMICAL DATA FILE READ '

  close(unit=uchem)

  return
end subroutine chemical_db_populate_from_file

!> Create chemical property record from name and ordered list of
!! properties
type(chemical) function create_chemical(name, props) result(chem)
  use extran_param, only: NCHEMPROPS
  implicit none

  !> Chemical species name
  character(len=20), intent(in) :: name

  !> Chemical property array
  real(kind=WP), dimension(NCHEMPROPS), intent(in) :: props

  continue

  chem%name = name

  chem%molec_wt    = props(1)
  chem%std_boil_pt = props(2)
  chem%cp          = props(3)
  chem%hv          = props(4)
  chem%density     = props(5)
  chem%diff_coef   = props(6)

  return
end function create_chemical

end module extran_chemical
