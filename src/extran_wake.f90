!> @file extran_param.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains building wake model parameters, objects, and functions

!> @brief Contains building wake model parameters, objects, and functions
module extran_wake
  use extran_param, only: WP, IP, NSTAB
  use m_constant, only: ZERO, TENTH, TEN
  implicit none

  private

  public :: get_wake_conditions

  !> Elevation above boundary, \f$\si{\meter}\f$.
  !! Taken as \f$\SI{10.0}{\meter}\f$
  real(kind=WP), parameter, private :: z = TEN

  !> Roughness height for turbulent boundary layer modeling,
  !! \f$\si{\meter}\f$. Taken as \f$\SI{0.1}{\meter}\f$
  real(kind=WP), parameter, private :: znot = TENTH

  !> \f$\left(\frac{z}{l}\right)\f$ parameters for each atmospheric
  !! stability class
  real(kind=WP), dimension(NSTAB), parameter, private :: ZOL =           &
    (/ -1.0_WP, -0.6_WP, -0.33_WP, -0.07_WP, 0.09_WP, 0.2_WP, 0.5_WP /)

  !> Von Karman constant for turbulent boundary layer flow with a
  !! no-slip condition. Dimensionless.
  real(kind=WP), parameter, private :: k_von_karman = 0.40_WP

  !> Object carrying building wake conditions - surface friction
  !! velocity and horizontal and vertical time scales.
  !! Calculates \f$\sigma_{y}\f$ and \f$\sigma_{z}\f$ for wake-induced
  !! turbulence
  type, public :: wake_condition
    !> Surface friction (shear) velocity, \f$\si{\meter\per\second}\f$
    real(kind=WP), public :: ustar = ZERO
    !> Horizontal time scale, \f$\si{\second}\f$
    real(kind=WP), public :: tsh = ZERO
    !> Vertical time scale, \f$\si{\second}\f$
    real(kind=WP), public :: tsv = ZERO
  contains
    !> Get lateral wake diffusion coefficient
    procedure, public :: get_sigma_y => wake_condition_get_sigma_y
    !> Get vertical wake diffusion coefficient
    procedure, public :: get_sigma_z => wake_condition_get_sigma_z
  end type wake_condition

contains

!> Returns wake condition data structure populated from given geometric
!! and atmospheric conditions. @cite Hanna1982
!!
!! Estimate planetary boundary layer shear (surface friction) velocity
!! \f$u_{*}\f$, given in \f$\si{\meter\per\second}\f$. Shear velocity
!! assumes a log wind profile in the surface layer of the atmosphere
!! (from \f$\SIrange{0}{100}{\meter}\f$ in elevation).
!!
!! \f$u_{*} = \frac{\kappa \bar{u}}{\ln{\frac{z}{z_{0}}}}\f$ where here
!! \f$\bar{u}\f$ is the mean wind speed at elevation \f$z\f$,
!! \f$\kappa = \num{0.4}\f$ (the Von Karman constant),
!! \f$z = \SI{1}{\meter}\f$, and \f$z_{0} = \SI{0.1}{\meter}\f$
type(wake_condition) function get_wake_conditions(area, environment)    &
  result(wake)
  use m_constant, only: TWO
  use m_environ, only: environment_properties
  implicit none

  !> Building cross-sectional area in direction of wind,
  !! in \f$\si{\square\meter}\f$
  !! Expected range is \f$\SIrange{0}{4000}{\square\meter}\f$
  real(kind=WP), intent(in) :: area

  !> Environment property object
  type(environment_properties), intent(in) :: environment

  continue

  ! Find surface friction velocity at 10 m given zo = 0.1 m
  wake%ustar = (k_von_karman * environment%ubar) / log(z / znot)

  wake%tsh = sqrt(area / wake%ustar**2)

  ! Find approximate z/l at 10 m given zo = 0.1 m and stability class
  wake%tsv = sqrt(area)                                                 &
    / ((TWO + ZOL(environment%stability_class)) * wake%ustar)

  return
end function get_wake_conditions

!> General internal function for wake diffusion coeffient calculations
elemental real(kind=WP) function get_wake_sigma(t, ts) result(sigma)
  use m_constant, only: HALF, ONE
  implicit none

  real(kind=WP), parameter :: C = HALF

  !> Time, \f$\si{\second}\f$
  real(kind=WP), intent(in) :: t

  !> Time scale, \f$\si{\second}\f$
  real(kind=WP), intent(in) :: ts

  continue

  sigma = sqrt(C * ts * ts * (exp(-t / ts) * (-t / ts - ONE) + ONE))

  return
end function get_wake_sigma

!> Calculate lateral wake diffusion coeffient. Wrapper around
!! get_wake_sigma() called with lateral time parameters.
elemental real(kind=WP) function wake_condition_get_sigma_y(this, th)   &
  result(wsigy)
  implicit none

  !> Object reference
  class(wake_condition), intent(in) :: this

  !> Characteristic time in lateral (\f$y\f$) direction,
  !! \f$\si{\second}\f$
  real(kind=WP), intent(in) :: th

  continue

  wsigy = get_wake_sigma(th, this%tsh)

  return
end function wake_condition_get_sigma_y

!> Calculate vertical wake diffusion coeffient. Wrapper around
!! get_wake_sigma() called with vertical time parameters.
elemental real(kind=WP) function wake_condition_get_sigma_z(this, tv)   &
  result(wsigz)
  implicit none

  !> Object reference
  class(wake_condition), intent(in) :: this

  !> Characteristic time in vertical (\f$z\f$) direction,
  !! \f$\si{\second}\f$
  real(kind=WP), intent(in) :: tv

  continue

  wsigz = get_wake_sigma(tv, this%tsv)

  return
end function wake_condition_get_sigma_z

end module extran_wake
